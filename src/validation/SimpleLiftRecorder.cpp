#include "SimpleLiftRecorder.h"

#include "cinder/gl/gl.h"

#include "core/utility/logging/logging.h"

SimpleLiftRecorder::SimpleLiftRecorder(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_samplePositions()
	, m_displacement(0.0f, 0.0f, 0.0f)
	, m_liftForces()
{
	m_samplePositions.push_back(0.0f);
	m_samplePositions.push_back(250.0f);
	m_samplePositions.push_back(750.0f);
	m_samplePositions.push_back(2000.0f);
	m_samplePositions.push_back(-100.0f);

	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));

	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		m_logger.logValue(agl);
		m_logger.logValue("pos");
	}

	m_logger.logValue("groundLevel");

	m_logger.nextLine();
}

SimpleLiftRecorder::~SimpleLiftRecorder()
{

}

void SimpleLiftRecorder::update(const ParticleContainer& particles, const float deltaTime)
{
	m_position.y = 0.0f;

	std::vector<float> slopes = getSlopes();
	slopes = adjustSlopes(slopes);

	slopes[0] *= 0.2f;
	slopes[1] *= 0.2f;
	if (slopes[2] < 0.0f)
	{
		slopes[2] *= 0.5f;
	}
	else
	{
		slopes[2] *= 0.0f;
	}
	if (slopes[0] > 0.0f && slopes[3] < 0.0f)
	{
		slopes[3] *= 0.0f;
	}
	else
	{
		slopes[3] *= 0.2f;
	}

	float lift = getBaseLift(slopes);

	m_liftForces.clear();

	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		float tmpLift = adjustLiftForAltitude(lift, m_terrainAltitude, agl);

		m_liftForces.push_back(tmpLift);

		m_logger.logValue(tmpLift);
		m_logger.logValue(m_terrainAltitude + agl);
	}
	
	m_logger.logValue(m_terrainAltitude);
	m_logger.nextLine();

	// ci::Vec3f travel = (m_velocity * deltaTime) + (m_wind * deltaTime);
	m_displacement = (m_velocity * deltaTime) + (m_wind * deltaTime);
	m_position += m_displacement;
	updateTargetAltitude();
}

void SimpleLiftRecorder::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.6f, 0.9f, 0.6f, 1.0f));

	int i = 0;
	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		ci::Vec3f pos = m_position + ci::Vec3f(0.0f, m_terrainAltitude + agl, 0.0f);
		ci::gl::drawSphere(meterToScreen(pos), 0.01f);
		if (m_liftForces.size() > 0)
		{
			ci::gl::drawLine(meterToScreen(pos), meterToScreen(pos + ci::Vec3f(0.0f, m_liftForces[i] * 1000.0f, 0.0f)));
		}
		++i;
	}
}

std::string SimpleLiftRecorder::getName() const
{
	return "SimpleLiftRecorder";
}

ci::Vec3f SimpleLiftRecorder::getVelocity() const
{
	return m_velocity;
}

void SimpleLiftRecorder::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SimpleLiftRecorder::getWind() const
{
	return m_wind;
}

void SimpleLiftRecorder::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

ci::Vec3f SimpleLiftRecorder::getDisplacement() const
{
	return m_displacement;
}

float SimpleLiftRecorder::adjustLiftForAltitude(const float baseLift, const float terrainLevel, const float agl) const
{
	float boundary1 = 40.0f;
	float boundary2 = 130.0f;

	float lift = baseLift;

	float aglFactor = 1.0f;

	if (agl < boundary1)
	{
		aglFactor = 0.5f + 0.5f * agl / boundary1;
	}
	else if (agl < boundary2)
	{
		aglFactor = 1.0f;
	}
	else
	{
		aglFactor = std::exp(-1.0f * (2.0f + 2.0f * terrainLevel / 4000.0f) * (agl - boundary2) / std::max(terrainLevel, 200.0f));
	}

	lift *= aglFactor;

	return lift;
}

float SimpleLiftRecorder::getBaseLift(const std::vector<float>& slopes) const
{
	// std::vector<float> slopes = getSlopes();

	std::vector<float> liftFactors;
	liftFactors.push_back(0.0f);
	liftFactors.push_back(slopes[1]);
	liftFactors[0] = (slopes[1] + slopes[0]) * std::abs(liftFactors[1]);
	liftFactors.push_back(slopes[2] / 1.5f);
	liftFactors.push_back(slopes[3] / 4.0f);

	float baseLift = liftFactors[0] + liftFactors[1] + liftFactors[2] + liftFactors[3];

	return baseLift * m_wind.length();
}

std::vector<float> SimpleLiftRecorder::adjustSlopes(const std::vector<float>& slopes) const
{
	std::vector<float> adjustedSlopes;

	for (unsigned int i = 0; i < slopes.size(); i++)
	{
		float slope = slopes[i];
		if (slope != 0.0f)
		{
			adjustedSlopes.push_back(std::sin(std::atan(5.0f * std::pow(std::abs(slope), 1.7f))) * (slope / std::abs(slope)));
		}
		else
		{
			adjustedSlopes.push_back(0.0f);
		}
	}

	return adjustedSlopes;
}

std::vector<float> SimpleLiftRecorder::getSlopes() const
{
	std::vector<ci::Vec3f> samplePoints = sampleTerrain();

	std::vector<float> slopes;

	slopes.push_back((samplePoints[0].y - samplePoints[1].y) / m_samplePositions[1]);
	slopes.push_back((samplePoints[1].y - samplePoints[2].y) / m_samplePositions[2]);
	slopes.push_back((samplePoints[2].y - samplePoints[3].y) / m_samplePositions[3]);
	slopes.push_back((samplePoints[4].y - samplePoints[0].y) / -m_samplePositions[4]);

	return slopes;
}

std::vector<ci::Vec3f> SimpleLiftRecorder::sampleTerrain() const
{
	std::vector<ci::Vec3f> samplePoints;

	if (m_terrain != NULL)
	{
		ci::Vec3f windDir = m_wind.normalized();

		for (unsigned int i = 0; i < m_samplePositions.size(); i++)
		{
			ci::Vec3f sample = m_position - windDir * m_samplePositions[i];
			float altitude = 0.0f;
			if (m_terrain->getAltitudeAtPosition(sample, altitude))
			{
				sample.y = altitude;
			}
			else if (i > 0)
			{
				sample.y = samplePoints[i - 1].y;
			}

			samplePoints.push_back(sample);
		}
	}

	return samplePoints;
}