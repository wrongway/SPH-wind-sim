#include "SimpleTurbulenceDrone.h"

#include <stdlib.h>

#include "core/utility/logging/logging.h"

#include "cinder/gl/gl.h"

SimpleTurbulenceDrone::SimpleTurbulenceDrone(int id)
	: IValidator(id)
	, m_magnitude(0.0f)
	, m_magnitudeAcceleration(0.0f)
	, m_accelerationDirection(0.0f, 0.0f, 0.0f)
	, m_direction(0.0f, 0.0f, 0.0f)
	, m_turbulence(0.0f, 0.0f, 0.0f)
	, m_turbulenceRate(1.0f)
	, m_step(0)
	, m_averageTurbulence(0.0f, 0.0f, 0.0f)
	, m_accumulatedTurbulence(0.0f, 0.0f, 0.0f)
{
	std::srand(std::time(0));
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

SimpleTurbulenceDrone::~SimpleTurbulenceDrone()
{

}

void SimpleTurbulenceDrone::update(const ParticleContainer& particles, const float deltaTime)
{
	calculateTurbulence(deltaTime);

	m_wayPoints.push_back(m_position);

	// LOG_WARNING_STREAM(<< m_turbulence << " - " << m_wind << " - " << m_velocity);

	ci::Vec3f travel = (m_velocity * deltaTime) + (m_wind * deltaTime) + m_turbulence;
	m_position += travel;

	/*LOG_WARNING_STREAM(<< deltaTime);
	LOG_WARNING_STREAM(<< travel);*/

	updateTargetAltitude();

	float turbulenceMetric = getTurbulenceMetric();

	m_logger.logValue(turbulenceMetric);
	m_logger.nextLine();
}

void SimpleTurbulenceDrone::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.2f, 0.8f, 0.8f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}

	ci::gl::drawLine(meterToScreen(m_position), meterToScreen(m_position + m_turbulence));

	ci::gl::drawSphere(meterToScreen(m_position), 0.01f);
}

std::string SimpleTurbulenceDrone::getName() const
{
	return "SimpleTurbulenceDrone";
}

ci::Vec3f SimpleTurbulenceDrone::getVelocity() const
{
	return m_velocity;
}

void SimpleTurbulenceDrone::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SimpleTurbulenceDrone::getWind() const
{
	return m_wind;
}

void SimpleTurbulenceDrone::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;

	m_turbulenceRate = m_wind.length() * 0.05f;
}

void SimpleTurbulenceDrone::calculateTurbulence(const float deltaTime)
{
	float turbulenceGain = 0.1f;
	float rate = 100.0f;
	float turbulenceRate = m_turbulenceRate;

	if (turbulenceRate <= 0.0f)
	{
		turbulenceGain = 0.0f;
	}

	float wingSpan = 10.0f;
	float tailArm = 10.0f;

	ci::Vec3f accelerationDirection(0.0f, 0.0f, 0.0f);
	accelerationDirection.x = 1.0f - 2.0f * (float(std::rand()) / float(RAND_MAX));
	accelerationDirection.y = 1.0f - 2.0f * (float(std::rand()) / float(RAND_MAX));
	accelerationDirection.z = 1.0f - 2.0f * (float(std::rand()) / float(RAND_MAX));

	accelerationDirection.normalize();

	float accelerationMagnitude = 1.0f - 2.0f * (float(std::rand()) / float(RAND_MAX)) - m_magnitude;
	accelerationMagnitude = ((accelerationMagnitude - m_magnitude) / (1.0f + std::fabs(m_magnitude)));
	m_magnitudeAcceleration += accelerationMagnitude * rate * turbulenceRate * deltaTime;
	m_magnitude += m_magnitudeAcceleration * rate * turbulenceRate * deltaTime;
	m_magnitude = std::fabs(m_magnitude);

	accelerationDirection.x = squareSigned(accelerationDirection.x);
	accelerationDirection.y = squareSigned(accelerationDirection.y);
	accelerationDirection.z = squareSigned(squareSigned(accelerationDirection.z));

	m_accelerationDirection += accelerationDirection * rate * turbulenceRate * deltaTime;
	m_accelerationDirection.normalize();
	m_direction += m_accelerationDirection * rate * deltaTime;

	m_direction.normalize();

	ci::Vec3f turbulenceGradient = turbulenceGain * m_magnitudeAcceleration * m_direction;
	
	m_turbulence.x = turbulenceGradient.x / tailArm;
	m_turbulence.y = turbulenceGradient.y / tailArm;
	// m_turbulence.z = turbulenceGradient.z / wingSpan;

	// prevent endless acceleration
	if (std::abs(m_magnitudeAcceleration) > m_wind.length())
	{
		m_magnitudeAcceleration *= 0.5f;
	}

	// m_turbulence.y = turbulenceGradient.y;
}

float SimpleTurbulenceDrone::squareSigned(const float value)
{
	if (value < 0.0f)
	{
		return value * value * -1.0f;
	}
	else
	{
		return value * value;
	}
}

float SimpleTurbulenceDrone::getTurbulenceMetric()
{
	m_step++;

	m_accumulatedTurbulence += m_turbulence;

	if (m_step % 30 == 0)
	{
		m_averageTurbulence = m_accumulatedTurbulence;

		m_accumulatedTurbulence = ci::Vec3f(0.0f, 0.0f, 0.0f);

		m_averageTurbulence.x *= m_averageTurbulence.x;
		m_averageTurbulence.y *= m_averageTurbulence.y;
		m_averageTurbulence.z *= m_averageTurbulence.z;
		m_averageTurbulence /= 30.0f;
	}

	return 0.5f * (m_averageTurbulence.x + m_averageTurbulence.y + m_averageTurbulence.z);
}
