#ifndef SIMPLE_TURBULENCE_DRONE_H
#define SIMPLE_TURBULENCE_DRONE_H

#include "IValidator.h"

class SimpleTurbulenceDrone : public IValidator
{
public:
	SimpleTurbulenceDrone(int id);
	virtual ~SimpleTurbulenceDrone();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	void calculateTurbulence(const float deltaTime);

	float squareSigned(const float value);

	float getTurbulenceMetric();

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	std::vector<ci::Vec3f> m_wayPoints;

	// for turbulence calculation
	float m_magnitude;
	float m_magnitudeAcceleration;
	ci::Vec3f m_accelerationDirection;
	ci::Vec3f m_direction;

	ci::Vec3f m_turbulence;

	float m_turbulenceRate;

	int m_step;
	ci::Vec3f m_averageTurbulence;
	ci::Vec3f m_accumulatedTurbulence;
};

#endif // SIMPLE_TURBULENCE_DRONE_H