#include "SPHLiftRecorder.h"

#include "cinder/gl/gl.h"

#include "core/utility/logging/logging.h"

SPHLiftRecorder::SPHLiftRecorder(int id)
: IValidator(id)
, m_velocity(0.0f, 0.0f, 0.0f)
, m_wind(0.0f, 0.0f, 0.0f)
, m_displacement(0.0f, 0.0f, 0.0f)
, m_previousCellVelocitys()
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));

	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		m_logger.logValue(agl);
		m_logger.logValue("pos");

		m_previousCellVelocitys.push_back(ci::Vec3f(0.0f, 0.0f, 0.0f));
	}

	m_logger.logValue("groundLevel");

	m_logger.nextLine();
}

SPHLiftRecorder::~SPHLiftRecorder()
{

}

void SPHLiftRecorder::update(const ParticleContainer& particles, const float deltaTime)
{
	m_position.y = 0.0f;

	RegularGrid3D<Particle> grid = setupParticleGrid(particles);

	ci::Vec3f oldPos = m_position;

	unsigned int i = 0;
	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		std::vector<std::vector<Particle>> sphParticles = grid.getCells(m_position + ci::Vec3f(0.0f, m_terrainAltitude + agl, 0.0f), 0);

		ci::Vec3f cellVelocity(0.0f, 0.0f, 0.0f);
		float count = 0.0f;

		for (unsigned int j = 0; j < sphParticles.size(); j++)
		{
			for (unsigned int k = 0; k < sphParticles[j].size(); k++)
			{
				float simFactor = sphParticles[j][k].getSimulationFactor();

				ci::Vec3f vel = sphParticles[j][k].getVelocity();

				cellVelocity += (vel * simFactor + m_wind * (1.0f - simFactor));

				count += 1.0f;
			}
		}

		if (count > 0.0f)
		{
			cellVelocity /= count;
		}

		// LOG_WARNING_STREAM(<< cellVelocity.y << " - " << m_previousCellVelocity.y);

		ci::Vec3f tmpVelocity = cellVelocity;
		cellVelocity = (cellVelocity + m_previousCellVelocitys[i])* 0.5f;
		m_previousCellVelocitys[i] = tmpVelocity;

		if (cellVelocity.length() <= 0.0000001f)
		{
			cellVelocity = m_wind;
		}

		// LOG_WARNING_STREAM(<< cellVelocity.y);

		m_logger.logValue(cellVelocity.y);
		m_logger.logValue(m_terrainAltitude + agl);

		++i;
	}

	m_logger.logValue(m_terrainAltitude);

	m_displacement = (m_velocity * deltaTime) + (m_wind * deltaTime);
	m_position += m_displacement; //(m_velocity * deltaTime) + (m_wind * deltaTime);
	updateTargetAltitude();

	m_logger.nextLine();
}

void SPHLiftRecorder::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.9f, 0.6f, 0.6f, 1.0f));

	for (float agl = 0; agl <= 900.0f; agl += 50.0f)
	{
		ci::gl::drawSphere(meterToScreen(m_position + ci::Vec3f(0.0f, m_terrainAltitude + agl, 0.0f)), 0.01f);
	}
}

std::string SPHLiftRecorder::getName() const
{
	return "SPHLiftRecorder";
}

ci::Vec3f SPHLiftRecorder::getVelocity() const
{
	return m_velocity;
}

void SPHLiftRecorder::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SPHLiftRecorder::getWind() const
{
	return m_wind;
}

void SPHLiftRecorder::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

ci::Vec3f SPHLiftRecorder::getDisplacement() const
{
	return m_displacement;
}

RegularGrid3D<Particle> SPHLiftRecorder::setupParticleGrid(const ParticleContainer& particles)
{
	ci::Vec3f minCorner = particles.getBoundingBoxMinCorner();
	ci::Vec3f maxCorner = particles.getBoundingBoxMaxCorner();

	ci::Vec3f center = (minCorner + maxCorner) * 0.5f;

	float sideLength = 0.0f;
	float length = (maxCorner - minCorner).x;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).y;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).z;
	if (length > sideLength)
	{
		sideLength = length;
	}

	RegularGrid3D<Particle> grid;
	grid.setCenter(center);
	grid.setGridSideLength(sideLength);
	grid.setCellsPerDimension(particles.getNormParticlesPerDimension());

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f pos = particles[i].getPosition();
		grid.addValue(pos, particles[i]);
	}

	return grid;
}
