#include "SimpleWindValidator.h"

#include "cinder/gl/gl.h"

SimpleWindValidator::SimpleWindValidator(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_wayPoints()
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

SimpleWindValidator::~SimpleWindValidator()
{

}

void SimpleWindValidator::update(const ParticleContainer& particles, const float deltaTime)
{
	m_wayPoints.push_back(m_position);

	m_position += (m_velocity * deltaTime) + (m_wind * deltaTime);

	updateTargetAltitude();
}

void SimpleWindValidator::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.2f, 0.8f, 0.8f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}
}

std::string SimpleWindValidator::getName() const
{
	return "SimpleWindValidator";
}

ci::Vec3f SimpleWindValidator::getVelocity() const
{
	return m_velocity;
}

void SimpleWindValidator::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SimpleWindValidator::getWind() const
{
	return m_wind;
}

void SimpleWindValidator::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}