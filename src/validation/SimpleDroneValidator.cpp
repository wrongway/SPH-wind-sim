#include "SimpleDroneValidator.h"

#include "core/utility/logging/logging.h"

#include "cinder/gl/gl.h"

SimpleDroneValidator::SimpleDroneValidator(int id)
: IValidator(id)
, m_velocity(0.0f, 0.0f, 0.0f)
, m_wind(0.0f, 0.0f, 0.0f)
, m_displacement(0.0f, 0.0f, 0.0f)
, m_wayPoints()
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

SimpleDroneValidator::~SimpleDroneValidator()
{

}

void SimpleDroneValidator::update(const ParticleContainer& particles, const float deltaTime)
{
	if (m_startPosSet == false)
	{
		m_startPosition = m_position;
		m_startPosSet = true;
	}

	m_wayPoints.push_back(m_position);

	ci::Vec3f travel = (m_velocity * deltaTime) + (m_wind * deltaTime);
	m_position += travel;

	//LOG_WARNING_STREAM(<< travel);

	ci::Vec3f distanceTravelled = m_startPosition - m_position;
	ci::Vec2f distanceTravelled2d(distanceTravelled.x, distanceTravelled.z);

	m_distanceTravelled += distanceTravelled.length();

	m_logger.logValue(distanceTravelled2d.length());
	m_logger.logValue(m_distanceTravelled);
	m_logger.logValue(m_position.y);
	
	updateTargetAltitude();

	m_logger.logValue(m_terrainAltitude);
	m_logger.nextLine();
}

void SimpleDroneValidator::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.6f, 0.9f, 0.6f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}

	ci::gl::drawSphere(meterToScreen(m_position), 0.01f);
}

std::string SimpleDroneValidator::getName() const
{
	return "SimpleDroneValidator";
}

ci::Vec3f SimpleDroneValidator::getVelocity() const
{
	return m_velocity;
}

void SimpleDroneValidator::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SimpleDroneValidator::getWind() const
{
	return m_wind;
}

void SimpleDroneValidator::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

ci::Vec3f SimpleDroneValidator::getDisplacement() const
{
	return m_displacement;
}