#include "Streak.h"

#include "core/utility/logging/logging.h"

#include "core/UnitUtility.h"

Streak::Streak()
{
	m_shader.load("data/shader/streakShader.vert", "data/shader/streakShader.frag");
	m_shader.bindAttribute(0, "vPosition");
	m_shader.bindAttribute(1, "vNormal");
	m_shader.linkShaders();
}

Streak::~Streak()
{

}

void Streak::render(const ci::Camera& camera)
{
	std::vector<ci::Vec3f> vertices;
	std::vector<ci::Vec3f> normals;
	std::vector<unsigned int> indices;

	for (unsigned int i = 1; i < m_waypoints.size(); i++)
	{
		ci::Vec3f p0 = meterToScreen(m_waypoints[i - 1]);
		ci::Vec3f p1 = meterToScreen(m_waypoints[i]);

		ci::Vec3f forward = p0 - p1;
		ci::Vec3f sideward = forward.cross(ci::Vec3f(0.0f, 1.0f, 0.0f));
		sideward.normalize();
		sideward *= m_width;

		
		ci::Vec3f p2 = p0 + sideward * 0.5f;
		ci::Vec3f p3 = p1 + sideward * 0.5f;
		p0 -= sideward * 0.5f;
		p1 -= sideward * 0.5f;

		vertices.push_back(p0);
		vertices.push_back(p2);

		/*vertices.push_back(p2);
		vertices.push_back(p3);*/

		normals.push_back(ci::Vec3f(0.0f, 1.0f, 0.0f));
		normals.push_back(ci::Vec3f(0.0f, 1.0f, 0.0f));
		/*normals.push_back(ci::Vec3f(0.0f, 1.0f, 0.0f));
		normals.push_back(ci::Vec3f(0.0f, 1.0f, 0.0f));*/

		

		if (vertices.size() > 3)
		{
			unsigned int idx = vertices.size() - 1;

			indices.push_back(idx - 2);
			indices.push_back(idx - 3);
			indices.push_back(idx - 1);

			
			indices.push_back(idx - 1);
			indices.push_back(idx);
			indices.push_back(idx - 2);
		}
	}

	MeshBufferObject mesh;
	mesh.generateBuffers();
	mesh.bindVertices(vertices);
	mesh.bindIndices(indices);
	mesh.bindNormals(normals);

	ci::Matrix44f mat_modelview = camera.getModelViewMatrix();
	ci::Matrix44f mat_model_view_proj = camera.getProjectionMatrix() * mat_modelview;

	ci::gl::pushMatrices();
	ci::gl::enableDepthWrite();
	ci::gl::enableDepthRead();
	ci::gl::enableAlphaBlending();

	ci::gl::disable(GL_CULL_FACE);

	ci::gl::color(ci::ColorAf(0.9f, 0.6f, 0.6f, 0.5f));

	m_shader.bind();
	m_shader.setUniform("modelviewMatrix", mat_modelview, false);
	m_shader.setUniform("modelviewProjectionMatrix", mat_model_view_proj, false);
	m_shader.setUniform("vCameraPos", camera.getEyePoint());

	m_shader.setUniform("fLightAmbient", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fLightDiffuse", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fLightSpecular", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fShinyness", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));

	m_shader.setUniform("fMaterialSpecular", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("vLightPosition", ci::Vec3f(0.0f, 10.0f, 10.0f));

		mesh.bindVertexArray();
		mesh.draw();

	m_shader.unbind();

	ci::gl::enable(GL_CULL_FACE);

	ci::gl::disableAlphaBlending();
	ci::gl::popMatrices();
}

void Streak::setWidth(const float width)
{
	m_width = width;
}

float Streak::getWidth() const
{
	return m_width;
}

void Streak::pushWaypoint(const ci::Vec3f& waypoint)
{
	m_waypoints.push_back(waypoint);
}

void Streak::clearWaypoints()
{
	m_waypoints.clear();
}