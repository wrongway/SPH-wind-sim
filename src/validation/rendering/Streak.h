#ifndef STREAK_H
#define STREAK_H

#include <vector>

#include "cinder/Camera.h"

#include "cinder/Vector.h"

#include "core/graphics/MeshBufferObject.h"
#include "core/graphics/Shader.h"

class Streak
{
public:
	Streak();
	~Streak();

	void render(const ci::Camera& camera);

	void setWidth(const float width);
	float getWidth() const;

	void pushWaypoint(const ci::Vec3f& waypoint);
	void clearWaypoints();

private:
	float m_width;

	std::vector<ci::Vec3f> m_waypoints;

	Shader m_shader;
};

#endif // STREAK_H