#ifndef SPH_WIND_VALIDATOR_H
#define SPH_WIND_VALIDATOR_H

#include "core/algorithm/RegularGrid3d.h"

#include "IValidator.h"

class SPHWindValidator : public IValidator
{
public:
	SPHWindValidator(int id);
	virtual ~SPHWindValidator();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	RegularGrid3D<Particle> setupParticleGrid(const ParticleContainer& particles);

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	std::vector<ci::Vec3f> m_wayPoints;
};

#endif // SPH_WIND_VALIDATOR_H