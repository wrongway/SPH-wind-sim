#ifndef SIMPLE_LIFT_DRONE_H
#define SIMPLE_LIFT_DRONE_H

#include "IValidator.h"

class SimpleLiftDrone : public IValidator
{
public:
	SimpleLiftDrone(int id);
	virtual ~SimpleLiftDrone();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	float adjustLiftForAltitude(const float baseLift) const;
	float getBaseLift() const;
	std::vector<float> adjustSlopes(const std::vector<float>& slopes) const;
	std::vector<float> getSlopes() const;
	std::vector<ci::Vec3f> sampleTerrain() const;

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	std::vector<ci::Vec3f> m_wayPoints;

	std::vector<float> m_samplePositions;

	ci::Vec3f m_lift;
};

#endif // SIMPLE_LIFT_DRONE_H