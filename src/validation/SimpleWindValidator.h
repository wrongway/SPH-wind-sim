#ifndef SIMPLE_WIND_VALIDATOR_H
#define SIMPLE_WIND_VALIDATOR_H

#include "IValidator.h"

class SimpleWindValidator : public IValidator
{
public:
	SimpleWindValidator(int id);
	virtual ~SimpleWindValidator();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	std::vector<ci::Vec3f> m_wayPoints;
};

#endif // SIMPLE_WIND_VALIDATOR_H