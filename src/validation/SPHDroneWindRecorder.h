#ifndef SPH_DRONE_WIND_RECORDER_H
#define SPH_DRONE_WIND_RECORDER_H

#include "core/algorithm/RegularGrid3d.h"

#include "IValidator.h"

class SPHDroneWindRecorder : public IValidator
{
public:
	SPHDroneWindRecorder(int id);
	virtual ~SPHDroneWindRecorder();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	ci::Vec3f getDisplacement() const;

private:
	RegularGrid3D<Particle> setupParticleGrid(const ParticleContainer& particles);

	float getCorrectionFactor(const float windSpeed, const float agl, bool negativ) const;

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;
	ci::Vec3f m_sphWindVelocity;

	ci::Vec3f m_displacement;

	std::vector<ci::Vec3f> m_wayPoints;

	int m_step;

	ci::Vec3f m_previousCellVelocity;
};

#endif // SPH_DRONE_WIND_RECORDER_H