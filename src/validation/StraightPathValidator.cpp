#include "StraightPathValidator.h"

#include "cinder/gl/gl.h"

StraightPathValidator::StraightPathValidator(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wayPoints()
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

StraightPathValidator::~StraightPathValidator()
{

}

void StraightPathValidator::update(const ParticleContainer& particles, const float deltaTime)
{
	m_wayPoints.push_back(m_position);

	m_position += m_velocity * deltaTime;

	updateTargetAltitude();
}

void StraightPathValidator::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(1.0f, 1.0f, 1.0f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}
}

std::string StraightPathValidator::getName() const
{
	return "StraightPathValidator";
}

ci::Vec3f StraightPathValidator::getVelocity() const
{
	return m_velocity;
}

void StraightPathValidator::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}