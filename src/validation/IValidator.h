#ifndef I_VALIDATOR_H
#define I_VALIDATOR_H

#include <string>

#include "core/UnitUtility.h"

#include "core/utility/logging/CSVLogger.h"

#include "cinder/Camera.h"

#include "simulation/bodies/IPolygonBody.h"

#include "simulation/particle/Particle.h"
#include "simulation/particle/ParticleContainer.h"

class IValidator
{
public:
	IValidator(int id);
	virtual ~IValidator(){}

	virtual void update(const ParticleContainer& particles, const float deltaTime) = 0;
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera) = 0;

	virtual std::string getName() const = 0;

	void move(const ci::Vec3f& displacement);

	ci::Vec3f getPosition() const;
	void setPosition(const ci::Vec3f& position);

	void setTerrain(IPolygonBody* terrain);

	void setTargetAltitude(const float targetAltitude);

	bool getIsDominant() const;
	void setIsDominant(const bool isDominant);

	int getId() const;
	void setId(const int id);

protected:
	void updateTargetAltitude();

	ci::Vec3f m_position;

	IPolygonBody* m_terrain;

	float m_targetAltitude;

	CSVLogger m_logger;

	ci::Vec3f m_startPosition;
	bool m_startPosSet;

	float m_terrainAltitude;
	float m_distanceTravelled;

	bool m_isDominant;
	int m_validatorId;
};

#endif // I_VALIDATOR_H