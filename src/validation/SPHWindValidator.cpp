#include "SPHWindValidator.h"

#include "cinder/gl/gl.h"

SPHWindValidator::SPHWindValidator(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_wayPoints()
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

SPHWindValidator::~SPHWindValidator()
{

}

void SPHWindValidator::update(const ParticleContainer& particles, const float deltaTime)
{
	m_wayPoints.push_back(m_position);

	RegularGrid3D<Particle> grid = setupParticleGrid(particles);

	std::vector<std::vector<Particle>> sphParticles = grid.getCells(m_position, 0);

	ci::Vec3f cellVelocity(0.0f, 0.0f, 0.0f);
	float count = 0.0f;

	for (unsigned int j = 0; j < sphParticles.size(); j++)
	{
		for (unsigned int k = 0; k < sphParticles[j].size(); k++)
		{
			float simFactor = sphParticles[j][k].getSimulationFactor();

			ci::Vec3f vel = sphParticles[j][k].getVelocity();

			cellVelocity += (vel * simFactor + m_wind * (1.0f - simFactor));

			count += 1.0f;
		}
	}

	if (count > 0.0f)
	{
		cellVelocity /= count;
	}

	m_position += (m_velocity * deltaTime) + (cellVelocity * deltaTime);

	updateTargetAltitude();
}

void SPHWindValidator::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.8f, 0.2f, 0.9f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}
}

std::string SPHWindValidator::getName() const
{
	return "SPHWindValidator";
}

ci::Vec3f SPHWindValidator::getVelocity() const
{
	return m_velocity;
}

void SPHWindValidator::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SPHWindValidator::getWind() const
{
	return m_wind;
}

void SPHWindValidator::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

RegularGrid3D<Particle> SPHWindValidator::setupParticleGrid(const ParticleContainer& particles)
{
	ci::Vec3f minCorner = particles.getBoundingBoxMinCorner();
	ci::Vec3f maxCorner = particles.getBoundingBoxMaxCorner();

	ci::Vec3f center = (minCorner + maxCorner) * 0.5f;

	float sideLength = 0.0f;
	float length = (maxCorner - minCorner).x;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).y;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).z;
	if (length > sideLength)
	{
		sideLength = length;
	}

	RegularGrid3D<Particle> grid;
	grid.setCenter(center);
	grid.setGridSideLength(sideLength);
	grid.setCellsPerDimension(particles.getNormParticlesPerDimension());

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f pos = particles[i].getPosition();
		grid.addValue(pos, particles[i]);
	}

	return grid;
}