#include "IValidator.h"

IValidator::IValidator(int id)
	: m_position(0.0f, 0.0f, 0.0f)
	, m_terrain(NULL)
	, m_targetAltitude(0.0f)
	, m_logger()
	, m_startPosition(0.0f, 0.0f, 0.0f)
	, m_startPosSet(false)
	, m_terrainAltitude(0.0f)
	, m_distanceTravelled(0.0f)
	, m_isDominant(false)
	, m_validatorId(id)
{
}

void IValidator::move(const ci::Vec3f& displacement)
{
	m_position += displacement;

	if (m_terrain != NULL)
	{
		float altitude = 0.0f;

		if (m_terrain->getAltitudeAtPosition(m_position, altitude))
		{
			m_position.y = altitude + m_targetAltitude;
		}
	}
}

ci::Vec3f IValidator::getPosition() const
{
	return m_position;
}

void IValidator::setPosition(const ci::Vec3f& position)
{
	m_position = position;
}

void IValidator::setTerrain(IPolygonBody* terrain)
{
	m_terrain = terrain;
}

void IValidator::setTargetAltitude(const float targetAltitude)
{
	m_targetAltitude = targetAltitude;
}

bool IValidator::getIsDominant() const
{
	return m_isDominant;
}

void IValidator::setIsDominant(const bool isDominant)
{
	m_isDominant = isDominant;
}

int IValidator::getId() const
{
	return m_validatorId;
}

void IValidator::setId(const int id)
{
	m_validatorId = id;
}

void IValidator::updateTargetAltitude()
{
	if (m_terrain != NULL)
	{
		float altitude = 0.0f;

		if (m_terrain->getAltitudeAtPosition(m_position, altitude))
		{
			m_terrainAltitude = altitude;
			m_targetAltitude = m_position.y - altitude;
		}
	}
}