#include "VerticalPressureRecorder.h"

VerticalPressureRecorder::VerticalPressureRecorder(int id)
	: IValidator(id)
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));

	m_logger.logValue("minDensity");
	m_logger.logValue("maxDensity");
	m_logger.logValue("averageDensity");
	m_logger.logValue("stdDeviation");
	m_logger.nextLine();
}

VerticalPressureRecorder::~VerticalPressureRecorder()
{

}

void VerticalPressureRecorder::update(const ParticleContainer& particles, const float deltaTime)
{
	RegularGrid3D<Particle> grid = setupParticleGrid(particles);

	std::vector<std::vector<Particle>> slices = grid.getHorizontalSlices();

	for (unsigned int i = 0; i < slices.size(); i++)
	{
		float minDensity = FLT_MAX;
		float maxDensity = FLT_MIN;
		float averageDensity = 0.0f;

		for (unsigned int j = 0; j < slices[i].size(); j++)
		{
			float currentDensity = slices[i][j].getSPHDensity();

			if (currentDensity < minDensity)
			{
				minDensity = currentDensity;
			}
			if (currentDensity > maxDensity)
			{
				maxDensity = currentDensity;
			}

			averageDensity += (currentDensity / (float)slices[i].size());
		}

		float standardDeviation = 0.0f;
		for (unsigned int j = 0; j < slices[i].size(); j++)
		{
			float currentDensity = slices[i][j].getSPHDensity();

			standardDeviation += std::pow(currentDensity - averageDensity, 2.0f) / slices[i].size();
		}

		m_logger.logValue(minDensity);
		m_logger.logValue(maxDensity);
		m_logger.logValue(averageDensity);
		m_logger.logValue(standardDeviation);
	}

	m_logger.nextLine();
}

void VerticalPressureRecorder::render(const ParticleContainer& particles, const ci::Camera& camera)
{

}

std::string VerticalPressureRecorder::getName() const
{
	return "VerticalPressureRecorder";
}

RegularGrid3D<Particle> VerticalPressureRecorder::setupParticleGrid(const ParticleContainer& particles)
{
	ci::Vec3f minCorner = particles.getSphBoundingBoxMinCorner();
	ci::Vec3f maxCorner = particles.getSphBoundingBoxMaxCorner();

	ci::Vec3f center = (minCorner + maxCorner) * 0.5f;

	float sideLength = 0.0f;
	float length = (maxCorner - minCorner).x;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).y;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).z;
	if (length > sideLength)
	{
		sideLength = length;
	}

	RegularGrid3D<Particle> grid;
	grid.setCenter(center);
	grid.setGridSideLength(sideLength);
	grid.setCellsPerDimension(particles.getNormParticlesPerDimension());

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f pos = particles[i].getPosition();
		grid.addValue(pos, particles[i]);
	}

	return grid;
}