#ifndef SPH_LIFT_RECORDER_H
#define SPH_LIFT_RECORDER_H

#include "core/algorithm/RegularGrid3d.h"

#include "IValidator.h"

class SPHLiftRecorder : public IValidator
{
public:
	SPHLiftRecorder(int id);
	virtual ~SPHLiftRecorder();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	ci::Vec3f getDisplacement() const;

private:
	RegularGrid3D<Particle> setupParticleGrid(const ParticleContainer& particles);

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	ci::Vec3f m_displacement;

	std::vector<ci::Vec3f> m_previousCellVelocitys;
};

#endif // SPH_LIFT_RECORDER_H