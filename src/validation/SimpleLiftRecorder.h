#ifndef SIMPLE_LIFT_RECORDER_H
#define SIMPLE_LIFT_RECORDER_H

#include "core/algorithm/RegularGrid3d.h"

#include "IValidator.h"

class SimpleLiftRecorder : public IValidator
{
public:
	SimpleLiftRecorder(int id);
	virtual ~SimpleLiftRecorder();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	ci::Vec3f getDisplacement() const;

private:
	float adjustLiftForAltitude(const float baseLift, const float terrainLevel, const float agl) const;
	float getBaseLift(const std::vector<float>& slopes) const;
	std::vector<float> adjustSlopes(const std::vector<float>& slopes) const;
	std::vector<float> getSlopes() const;
	std::vector<ci::Vec3f> sampleTerrain() const;

	ci::Vec3f m_velocity;
	ci::Vec3f m_wind;

	std::vector<float> m_samplePositions;

	ci::Vec3f m_displacement;

	std::vector<float> m_liftForces;
};

#endif // SIMPLE_LIFT_RECORDER_H