#include "SPHDroneWindRecorder.h"

#include "cinder/gl/gl.h"

#include "core/utility/logging/logging.h"

SPHDroneWindRecorder::SPHDroneWindRecorder(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_displacement(0.0f, 0.0f, 0.0f)
	, m_sphWindVelocity(0.0f, 0.0f, 0.0f)
	, m_wayPoints()
	, m_step(0)
	, m_previousCellVelocity(0.0f, 0.0f, 0.0f)
{
	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));

	m_logger.logValue("pos x");
	m_logger.logValue("pos y");
	m_logger.logValue("pos z");

	m_logger.logValue("wind x");
	m_logger.logValue("wind y");
	m_logger.logValue("wind z");

	m_logger.logValue("dist");

	m_logger.nextLine();
}

SPHDroneWindRecorder::~SPHDroneWindRecorder()
{

}

void SPHDroneWindRecorder::update(const ParticleContainer& particles, const float deltaTime)
{
	if (m_startPosSet == false)
	{
		m_startPosition = m_position;
		m_startPosSet = true;
	}

	m_wayPoints.push_back(m_position);

	RegularGrid3D<Particle> grid = setupParticleGrid(particles);

	ci::Vec3f oldPos = m_position;

	std::vector<std::vector<Particle>> sphParticles = grid.getCells(m_position, 0);

	ci::Vec3f cellVelocity(0.0f, 0.0f, 0.0f);
	float count = 0.0f;

	for (unsigned int j = 0; j < sphParticles.size(); j++)
	{
		for (unsigned int k = 0; k < sphParticles[j].size(); k++)
		{
			float simFactor = sphParticles[j][k].getSimulationFactor();

			ci::Vec3f vel = sphParticles[j][k].getVelocity();

			cellVelocity += (vel * simFactor + m_wind * (1.0f - simFactor));

			count += 1.0f;
		}
	}

	if (count > 0.0f)
	{
		cellVelocity /= count;
	}

	// even out super high frequency osci...oszil...vibration
	ci::Vec3f tmpVelocity = cellVelocity;
	cellVelocity = (cellVelocity + m_previousCellVelocity)* 0.5f;
	m_previousCellVelocity = tmpVelocity;

	if (cellVelocity.length() <= 0.0000001f)
	{
		cellVelocity = m_wind;
	}

	bool downWind = cellVelocity.y < 0.0f;
	float correctionFactor = getCorrectionFactor(cellVelocity.length(), m_position.y - m_terrainAltitude, downWind);


	cellVelocity.y /= std::max(1.0f, correctionFactor);

	ci::Vec3f travel = (m_velocity * deltaTime); // + (m_wind * deltaTime);
	m_position += travel;


	m_displacement = m_position - oldPos;

	m_sphWindVelocity = cellVelocity;

	ci::Vec3f distanceTravelled2d = m_position - m_startPosition;
	distanceTravelled2d.y = 0.0f;

	m_distanceTravelled += m_displacement.length();

	m_logger.logValue(m_position.x);
	m_logger.logValue(m_position.y);
	m_logger.logValue(m_position.z);

	m_logger.logValue(cellVelocity.x);
	m_logger.logValue(cellVelocity.y);
	m_logger.logValue(cellVelocity.z);

	m_logger.logValue(m_distanceTravelled);

	m_logger.nextLine();
}

void SPHDroneWindRecorder::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.9f, 0.6f, 0.6f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}

	ci::gl::drawSphere(meterToScreen(m_position), 0.01f);

	ci::gl::color(ci::ColorAf(0.5f, 0.8f, 0.0f, 1.0f));
	ci::gl::drawLine(meterToScreen(m_position), meterToScreen(m_position + m_velocity));
	ci::gl::color(ci::ColorAf(0.0f, 0.3f, 0.9f, 1.0f));
	ci::gl::drawLine(meterToScreen(m_position + m_velocity), meterToScreen(m_position + m_velocity + m_sphWindVelocity));
	ci::gl::color(ci::ColorAf(0.0f, 0.9f, 0.2f, 1.0f));
	ci::gl::drawLine(meterToScreen(m_position), meterToScreen(m_position + m_velocity + m_sphWindVelocity));
}

std::string SPHDroneWindRecorder::getName() const
{
	return "SPHDroneWindRecorder";
}

ci::Vec3f SPHDroneWindRecorder::getVelocity() const
{
	return m_velocity;
}

void SPHDroneWindRecorder::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SPHDroneWindRecorder::getWind() const
{
	return m_wind;
}

void SPHDroneWindRecorder::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

ci::Vec3f SPHDroneWindRecorder::getDisplacement() const
{
	return m_displacement;
}

RegularGrid3D<Particle> SPHDroneWindRecorder::setupParticleGrid(const ParticleContainer& particles)
{
	ci::Vec3f minCorner = particles.getBoundingBoxMinCorner();
	ci::Vec3f maxCorner = particles.getBoundingBoxMaxCorner();

	ci::Vec3f center = (minCorner + maxCorner) * 0.5f;

	float sideLength = 0.0f;
	float length = (maxCorner - minCorner).x;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).y;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).z;
	if (length > sideLength)
	{
		sideLength = length;
	}

	RegularGrid3D<Particle> grid;
	grid.setCenter(center);
	grid.setGridSideLength(sideLength);
	grid.setCellsPerDimension(particles.getNormParticlesPerDimension());

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f pos = particles[i].getPosition();
		grid.addValue(pos, particles[i]);
	}

	return grid;
}

float SPHDroneWindRecorder::getCorrectionFactor(const float windSpeed, const float agl, bool negativ) const
{
	float aglFactor = 1.0f;

	if (negativ == false)
	{
		aglFactor = 12.24134f
			+ (agl * -0.04341144f)
			+ (std::pow(agl, 2.0f) * 0.0001896047f)
			+ (std::pow(agl, 3.0f) * -0.000000250851f)
			+ (std::pow(agl, 4.0f) * 0.000000000126271f);
	}
	else
	{
		aglFactor = 12.76619f
			+ (agl * -0.05343759f)
			+ (std::pow(agl, 2.0f) * 0.0001450935f)
			+ (std::pow(agl, 3.0f) * -0.0000001263247f)
			+ (std::pow(agl, 4.0f) * 0.00000000005227062f);
	}

	float windFactor = ((1.0f / windSpeed) * 2.0472f) + 0.3032f;

	return aglFactor *windFactor;
}
