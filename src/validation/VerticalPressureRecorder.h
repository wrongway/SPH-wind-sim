#ifndef VERTICAL_PRESSURE_RECORDER_H
#define VERTICAL_PRESSURE_RECORDER_H

#include "core/algorithm/RegularGrid3d.h"

#include "IValidator.h"

class VerticalPressureRecorder : public IValidator
{
public:
	VerticalPressureRecorder(int id);
	virtual ~VerticalPressureRecorder();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

private:
	RegularGrid3D<Particle> setupParticleGrid(const ParticleContainer& particles);
};

#endif // VERTICAL_PRESSURE_RECORDER_H