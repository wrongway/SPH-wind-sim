#include "SimpleLiftDrone.h"

#include <algorithm>
#include <math.h>

#include "cinder/gl/gl.h"

SimpleLiftDrone::SimpleLiftDrone(int id)
	: IValidator(id)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_wayPoints()
	, m_samplePositions()
	, m_lift(0.0f, 0.0f, 0.0f)
{
	m_samplePositions.push_back(0.0f);
	m_samplePositions.push_back(250.0f);
	m_samplePositions.push_back(750.0f);
	m_samplePositions.push_back(2000.0f);
	m_samplePositions.push_back(-100.0f);

	m_logger = CSVLogger(getName() + std::to_string(m_validatorId));
}

SimpleLiftDrone::~SimpleLiftDrone()
{

}

void SimpleLiftDrone::update(const ParticleContainer& particles, const float deltaTime)
{
	if (m_startPosSet == false)
	{
		m_startPosition = m_position;
		m_startPosSet = true;
	}

	float lift = 0.0f;

	// std::cout << m_position.y << " - " << m_terrainAltitude << std::endl;

	if (m_position.y > m_terrainAltitude)
	{
		lift = getBaseLift();
		lift = adjustLiftForAltitude(lift);

		if (isnan(lift) || !isfinite(lift))
		{
			std::vector<float> slopes = getSlopes();
			adjustSlopes(slopes);
			float foo = getBaseLift();
			foo = adjustLiftForAltitude(foo);
		}
	}
	
	m_wayPoints.push_back(m_position);

	ci::Vec3f wind = m_wind;
	wind.y = lift;
	m_lift = ci::Vec3f(0.0f, lift, 0.0f);
	ci::Vec3f travel = (m_velocity * deltaTime) + (wind * deltaTime);
	m_position += travel;

	ci::Vec3f distanceTravelled = m_startPosition - m_position;
	ci::Vec2f distanceTravelled2d(distanceTravelled.x, distanceTravelled.z);

	m_distanceTravelled += distanceTravelled.length();

	m_logger.logValue(distanceTravelled2d.length());
	m_logger.logValue(m_distanceTravelled);
	m_logger.logValue(m_position.y);
	m_logger.logValue(travel.y);

	updateTargetAltitude();

	m_logger.logValue(m_terrainAltitude);
	m_logger.nextLine();
}

void SimpleLiftDrone::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(0.6f, 0.9f, 0.6f, 1.0f));

	for (unsigned int i = 1; i < m_wayPoints.size(); i++)
	{
		ci::gl::drawLine(meterToScreen(m_wayPoints[i - 1]), meterToScreen(m_wayPoints[i]));
	}

	ci::gl::drawSphere(meterToScreen(m_position), 0.01f);
	ci::gl::drawLine(meterToScreen(m_position), meterToScreen(m_position + m_lift));
}

std::string SimpleLiftDrone::getName() const
{
	return "SimpleLiftDrone";
}

ci::Vec3f SimpleLiftDrone::getVelocity() const
{
	return m_velocity;
}

void SimpleLiftDrone::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f SimpleLiftDrone::getWind() const
{
	return m_wind;
}

void SimpleLiftDrone::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

float SimpleLiftDrone::adjustLiftForAltitude(const float baseLift) const
{
	float boundary1 = 40.0f;
	float boundary2 = 130.0f;

	float lift = baseLift;

	if (m_terrain != NULL)
	{
		float altitude = 0.0f;
		if (m_terrain->getAltitudeAtPosition(m_position, altitude))
		{
			float aglAltitude = m_position.y - altitude;
			float aglFactor = 1.0f;

			if (aglAltitude < boundary1)
			{
				aglFactor = 0.5f + 0.5f * aglAltitude / boundary1;
			}
			else if (aglAltitude < boundary2)
			{
				aglFactor = 1.0f;
			}
			else
			{
				aglFactor = std::exp(-1.0f * (2.0f + 2.0f * altitude / 4000.0f) * (aglAltitude - boundary2) / std::max(altitude, 200.0f));
			}

			lift *= aglFactor;
		}
	}

	return lift;
}

float SimpleLiftDrone::getBaseLift() const
{
	std::vector<float> slopes = getSlopes();
	slopes = adjustSlopes(slopes);

	slopes[0] *= 0.2f;
	slopes[1] *= 0.2f;
	if (slopes[2] < 0.0f)
	{
		slopes[2] *= 0.5f;
	}
	else
	{
		slopes[2] *= 0.0f;
	}
	if (slopes[0] > 0.0f && slopes[3] < 0.0f)
	{
		slopes[3] *= 0.0f;
	}
	else
	{
		slopes[3] *= 0.2f;
	}

	std::vector<float> liftFactors;
	liftFactors.push_back(0.0f);
	liftFactors.push_back(slopes[1]);
	liftFactors[0] = (slopes[1] + slopes[0]) * std::abs(liftFactors[1]);
	liftFactors.push_back(slopes[2] / 1.5f);
	liftFactors.push_back(slopes[3] / 4.0f);

	float baseLift = liftFactors[0] + liftFactors[1] + liftFactors[2] + liftFactors[3];

	return baseLift * m_wind.length();
}

std::vector<float> SimpleLiftDrone::adjustSlopes(const std::vector<float>& slopes) const
{
	std::vector<float> adjustedSlopes;

	for (unsigned int i = 0; i < slopes.size(); i++)
	{
		float slope = slopes[i];
		if (slope != 0.0f)
		{
			adjustedSlopes.push_back(std::sin(std::atan(5.0f * std::pow(std::abs(slope), 1.7f))) * (slope / std::abs(slope)));
		}
		else
		{
			adjustedSlopes.push_back(0.0f);
		}
	}

	return adjustedSlopes;
}

std::vector<float> SimpleLiftDrone::getSlopes() const
{
	std::vector<ci::Vec3f> samplePoints = sampleTerrain();

	std::vector<float> slopes;

	slopes.push_back((samplePoints[0].y - samplePoints[1].y) / m_samplePositions[1]);
	slopes.push_back((samplePoints[1].y - samplePoints[2].y) / m_samplePositions[2]);
	slopes.push_back((samplePoints[2].y - samplePoints[3].y) / m_samplePositions[3]);
	slopes.push_back((samplePoints[4].y - samplePoints[0].y) / -m_samplePositions[4]);

	return slopes;
}

std::vector<ci::Vec3f> SimpleLiftDrone::sampleTerrain() const
{
	std::vector<ci::Vec3f> samplePoints;

	if (m_terrain != NULL)
	{
		ci::Vec3f windDir = m_wind.normalized();

		for (unsigned int i = 0; i < m_samplePositions.size(); i++)
		{
			ci::Vec3f sample = m_position - windDir * m_samplePositions[i];
			float altitude = 0.0f;
			if (m_terrain->getAltitudeAtPosition(sample, altitude))
			{
				sample.y = altitude;
			}
			else if (i > 0)
			{
				sample.y = samplePoints[i - 1].y;
			}

			samplePoints.push_back(sample);
		}
	}

	return samplePoints;
}