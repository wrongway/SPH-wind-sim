#ifndef STRAIGHT_PATH_VALIDATOR_H
#define STRAIGHT_PATH_VALIDATOR_H

#include "IValidator.h"

class StraightPathValidator : public IValidator
{
public:
	StraightPathValidator(int id);
	virtual ~StraightPathValidator();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	ci::Vec3f getVelocity() const;
	void setVelocity(const ci::Vec3f& velocity);

private:
	ci::Vec3f m_velocity;

	std::vector<ci::Vec3f> m_wayPoints;
};

#endif // STRAIGHT_PATH_VALIDATOR_H