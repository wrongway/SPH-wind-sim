#include "SPHParticleRenderer.h"

#include "cinder/gl/gl.h"

SPHParticleRenderer::SPHParticleRenderer()
{

}

SPHParticleRenderer::~SPHParticleRenderer()
{

}

void SPHParticleRenderer::update(const ParticleContainer& particles, const float deltaTime)
{

}

void SPHParticleRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	std::pair<float, float> minMaxDensity = calculateMinMaxDensity(particles);
	renderParticles(particles, minMaxDensity);
}

std::string SPHParticleRenderer::getName() const
{
	return "SPHParticleRenderer";
}

void SPHParticleRenderer::move(const ci::Vec3f& displacement)
{

}

void SPHParticleRenderer::renderParticles(const ParticleContainer& particles, const std::pair<float, float>& minMaxDensity)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/*ci::gl::disableDepthWrite();
	ci::gl::disableDepthRead();*/

	unsigned int j = 0;

	for (unsigned int i = 0; i < particles.size(); i += 1)
	{
		float restDensity = particles[i].getDensity();
		float density = particles[i].getSPHDensity();
		float g = (density - minMaxDensity.first) / (minMaxDensity.second - minMaxDensity.first);
		float b = 0.9f;

		float foo = 1.0f; // 0.2f; //  std::abs(density - restDensity) / 0.2f;

		/*if (density > restDensity)
		{
			ci::gl::color(ci::ColorAf(g, 0.0f, b, 0.1f));
		}
		else
		{
			ci::gl::color(ci::ColorAf(0.0f, g, b, 0.1f));
		}*/

		ci::gl::color(ci::ColorAf(b, 1.0f - g, 0.0f, foo));
		// ci::gl::color(ci::ColorAf(0.0f, g, b, 0.1f));

		if (particles[i].getSimulationFactor() > 0.0f && particles[i].getIsGhost() == false)
		{
			j++;

			/*if (j != 180)
				continue;

			std::vector<unsigned int> neighbours = particles[i].getNeighbours();*/

			// ci::gl::color(ci::ColorAf(0.0f, 0.4f, 0.8f, 1.0f));

			float rad = meterToScreen(particles[i].getRadius());
			ci::Vec3f pos = meterToScreen(particles[i].getPosition());
			ci::Vec3f vel = meterToScreen(particles[i].getVelocity());

			ci::gl::drawSphere(pos, rad);

			/*ci::gl::color(ci::ColorAf(0.0f, g, b, 0.3f));

			ci::gl::drawSphere(pos, rad * 0.2f);
			ci::gl::color(ci::ColorAf(0.0f, g, b, 0.8f));
			ci::gl::drawLine(pos, pos + vel);*/

			/*for (unsigned int k = 0; k < neighbours.size(); k++)
			{
				pos = meterToScreen(particles[neighbours[k]].getPosition());
				vel = meterToScreen(particles[neighbours[k]].getVelocity());

				ci::gl::drawSphere(pos, rad);
			}*/
		}

		ci::gl::color(ci::ColorAf(0.0f, g, b, 0.3f));

	}

	/*ci::gl::enableDepthWrite();
	ci::gl::enableDepthRead();*/

	glDisable(GL_ALPHA);
}

std::pair<float, float> SPHParticleRenderer::calculateMinMaxDensity(const ParticleContainer& particles)
{
	float minDensity = FLT_MAX;
	float maxDensity = FLT_MIN;

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getIsGhost())
		{
			continue;
		}

		float currentDensity = particles[i].getSPHDensity();

		if (currentDensity < minDensity)
		{
			minDensity = currentDensity;
		}
		if (currentDensity > maxDensity)
		{
			maxDensity = currentDensity;
		}
	}

	return std::make_pair(minDensity, maxDensity);
}