#include "ModularParticleRenderer.h"

#include "cinder/gl/gl.h"

ModularParticleRenderer::ModularParticleRenderer()
{

}

ModularParticleRenderer::~ModularParticleRenderer()
{

}

void ModularParticleRenderer::update(const ParticleContainer& particles, const float deltaTime)
{

}

void ModularParticleRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	renderSPHParticles(particles);
	renderInflowParticles(particles);
	renderBoundaryParticles(particles);
}

std::string ModularParticleRenderer::getName() const
{
	return "ModularParticleRenderer";
}

void ModularParticleRenderer::move(const ci::Vec3f& displacement)
{

}

void ModularParticleRenderer::renderSPHParticles(const ParticleContainer& particles)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ci::gl::color(ci::ColorAf(0.0f, 0.8f, 0.3f, 1.0f));
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getSimulationFactor() > 0.0f && particles[i].getIsGhost() == false)
		{
			ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
		}
	}

	glDisable(GL_ALPHA);
}

void ModularParticleRenderer::renderInflowParticles(const ParticleContainer& particles)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ci::gl::color(ci::ColorAf(0.3f, 0.8f, 0.0f, 1.0f));
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getSimulationFactor() <= 0.0f && particles[i].getIsGhost() == false)
		{
			ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
		}
	}

	glDisable(GL_ALPHA);
}

void ModularParticleRenderer::renderBoundaryParticles(const ParticleContainer& particles)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ci::gl::color(ci::ColorAf(0.3f, 0.8f, 0.0f, 1.0f));
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getIsGhost() == true)
		{
			ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
		}
	}

	glDisable(GL_ALPHA);
}