#include "StreamlineRenderer.h"

#include "cinder/gl/gl.h"

StreamlineRenderer::StreamlineRenderer()
	: m_setupComplete(false)
	, m_smokeManager()
{

}

StreamlineRenderer::~StreamlineRenderer()
{

}

void StreamlineRenderer::update(const ParticleContainer& particles, const float deltaTime)
{
	if (m_setupComplete == false)
	{
		setup(particles);
		m_setupComplete = true;
	}

	m_smokeManager.update(particles, deltaTime);
}

void StreamlineRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	std::vector<std::vector<ci::Vec3f>> vertices = m_smokeManager.getVerticesByStreak();

	ci::gl::color(ci::ColorAf(0.8f, 0.8f, 0.8f, 0.8f));
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		for (unsigned int j = 0; j < vertices[i].size()-1; j++)
		{
			ci::Vec3f pos0 = vertices[i][j];
			ci::Vec3f pos1 = vertices[i][j+1];

			ci::gl::drawLine(pos0, pos1);

			ci::gl::drawSphere(pos0, 0.01f);
			ci::gl::drawSphere(pos1, 0.01f);
		}
	}

	std::vector<ci::Vec3f> emitters = m_smokeManager.getEmitterPositions();

	ci::gl::color(ci::ColorAf(0.8f, 0.8f, 0.8f, 1.0f));
	for (unsigned int i = 0; i < emitters.size(); i++)
	{
		ci::gl::drawSphere(emitters[i], 0.015f);
	}
}

std::string StreamlineRenderer::getName() const
{
	return "StreamlineRenderer";
}

void StreamlineRenderer::move(const ci::Vec3f& displacement)
{

}

ci::Vec3f StreamlineRenderer::getWind() const
{
	return m_smokeManager.getWind();
}

void StreamlineRenderer::setWind(const ci::Vec3f& wind)
{
	m_smokeManager.setWind(wind);
}

void StreamlineRenderer::setTerrain(IPolygonBody* terrain)
{
	m_smokeManager.setTerrain(terrain);
}

void StreamlineRenderer::setup(const ParticleContainer& particles)
{
	m_smokeManager.setStreakHeight(1.0f);
	m_smokeManager.setStreakSpacing(3.0f);
	m_smokeManager.setMinCorner(meterToScreen(particles.getNormMinCorner()));
	m_smokeManager.setMaxCorner(meterToScreen(particles.getNormMaxCorner()));
	float particleSpacing = meterToScreen((particles.getNormMaxCorner() - particles.getNormMinCorner()).x / (float)particles.getNormParticlesPerDimension());
	m_smokeManager.setParticleSpacing(ci::Vec3f(particleSpacing, particleSpacing, particleSpacing));
	m_smokeManager.setup(particles);
}