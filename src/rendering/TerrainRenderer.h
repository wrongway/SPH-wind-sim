#ifndef TERRAIN_RENDERER_H
#define TERRAIN_RENDERER_H

#include <string>

#include "cinder/Camera.h"

#include "core/graphics/MeshBufferObject.h"
#include "core/graphics/Shader.h"

class Terrain;

class TerrainRenderer
{
public:
	TerrainRenderer();
	~TerrainRenderer();

	void setup(const std::string& vertexPath, const std::string& fragmentPath);
	void render(const Terrain* terrain, const ci::Camera& camera, bool wireframe = false);

	virtual std::string getName() const;

private:
	void setupTerrainMesh(const Terrain* terrain);

	Shader m_phongShader;

	MeshBufferObject m_terrainMesh;

	bool m_meshGenerated;
};

#endif // TERRAIN_RENDERER_H