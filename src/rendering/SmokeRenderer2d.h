#ifndef SMOKE_RENDERER_2D_H
#define SMOKE_RENDERER_2D_H

#include "IRenderer.h"

#include "smokeRendererUtility/SmokeManager.h"

#include "core/graphics/MeshBufferObject.h"
#include "core/graphics/Shader.h"

#include "simulation/bodies/IPolygonBody.h"

class SmokeRenderer2d : public IRenderer
{
public:
	SmokeRenderer2d();
	virtual~SmokeRenderer2d();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

	void setStreakWidth(const unsigned int streakWidth);
	void setStreakSpacing(const unsigned int streakSpacing);
	void setParticleYCount(const unsigned int particleYCount);
	void setParticleXCount(const unsigned int particleXCount);
	void setShader(const std::string& vertexShader, const std::string& fragmentShader);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	void setTerrain(IPolygonBody* terrain);

private:
	void setup(const ParticleContainer& particles);
	void setupShader();
	void calculateValues(std::vector<ci::Vec3f>& vertices, std::vector<ci::Vec3f>& normals, std::vector<float>& areas, std::vector<float>& shapeFactors, float& averageArea) const;

	bool m_setupComplete;

	unsigned int m_streakWidth;
	unsigned int m_streakSpacing;
	unsigned int m_particleYCount;
	unsigned int m_particleXCount;

	SmokeManager m_smokeManager;

	std::string m_vertexShaderPath;
	std::string m_fragmentShaderPath;
	Shader m_shader;
};

#endif // SMOKE_RENDERER_2D_H