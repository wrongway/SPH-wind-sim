#include "SmokeRenderer3d.h"

#include <map>

#include "cinder/gl/gl.h"
#include "core/math/Triangle.h"

SmokeRenderer3d::SmokeRenderer3d()
	: m_setupComplete(false)
	, m_streakWidth(1)
	, m_streakSpacing(1)
	, m_particleYCount(0)
	, m_particleXCount(0)
	, m_smokeManager()
	, m_vertexShaderPath("")
	, m_fragmentShaderPath("")
	, m_shader()
	, m_displacement(0.0f, 0.0f, 0.0f)
{

}

SmokeRenderer3d::~SmokeRenderer3d()
{

}

void SmokeRenderer3d::update(const ParticleContainer& particles, const float deltaTime)
{
	if (m_setupComplete == false)
	{
		setup(particles);
		setupShader();
		m_setupComplete = true;
	}

	m_smokeManager.update(particles, deltaTime);
}

void SmokeRenderer3d::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	ci::gl::color(ci::ColorAf(1.0f, 1.0f, 1.0f, 1.0f));

	MeshBufferObject mesh;

	std::vector<ci::Vec3f> vertices;
	std::vector<ci::Vec3f> normals;
	std::vector<unsigned int> indices;
	std::vector<float> areas;
	std::vector<float> formFactors;
	float averageArea = 1.0f;

	calculateValues(vertices, normals, areas, formFactors, averageArea);

	if (vertices.size() <= 0)
	{
		return;
	}

	indices = m_smokeManager.getIndices();

	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		ci::gl::drawSphere(vertices[i], 0.00f); // prevents render error, somehow... fucking hell
	}

	if (indices.size() <= 0)
	{
		return;
	}

	std::vector<ci::Vec2f> texCoords;
	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		texCoords.push_back(ci::Vec2f(0.0f, 0.0f));
	}

	if (vertices.size() <= 0)
	{
		return;
	}

	mesh.generateBuffers();
	mesh.bindVertices(vertices);
	mesh.bindIndices(indices);
	mesh.bindNormals(normals);
	mesh.bindMiscValues(areas);
	mesh.bindMiscValues2(formFactors);

	ci::Matrix44f mat_modelview = camera.getModelViewMatrix();
	ci::Matrix44f mat_model_view_proj = camera.getProjectionMatrix() * mat_modelview;

	ci::Vec3f lightPos(0.0f, 1.0f, 0.0f);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// ci::gl::enableWireframe();

	ci::gl::pushMatrices();
	ci::gl::enableDepthWrite();
	ci::gl::enableDepthRead();

	m_shader.bind();
	m_shader.setUniform("modelviewMatrix", mat_modelview, false);
	m_shader.setUniform("modelviewProjectionMatrix", mat_model_view_proj, false);
	m_shader.setUniform("vCameraPos", camera.getEyePoint());

	m_shader.setUniform("fLightAmbient", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fLightDiffuse", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fLightSpecular", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("fShinyness", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));

	m_shader.setUniform("fMaterialSpecular", ci::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
	m_shader.setUniform("vLightPosition", ci::Vec3f(0.0f, 10.0f, 10.0f));

	m_shader.setUniform("fK", averageArea * 0.3f);

	mesh.bindVertexArray();
	mesh.draw();
	// glBindVertexArray(0);

	m_shader.unbind();

	ci::gl::popMatrices();

	// ci::gl::disableWireframe();

	glDisable(GL_ALPHA);
	glEnable(GL_DEPTH_TEST);
}

std::string SmokeRenderer3d::getName() const
{
	return "SmokeRenderer3d";
}

void SmokeRenderer3d::move(const ci::Vec3f& displacement)
{
	m_displacement += displacement;
	m_smokeManager.move(displacement);
}

void SmokeRenderer3d::setStreakSpacing(const unsigned int streakSpacing)
{
	m_streakSpacing = streakSpacing;
}

void SmokeRenderer3d::setParticleYCount(const unsigned int particleYCount)
{
	m_particleYCount = particleYCount;
}

void SmokeRenderer3d::setParticleXCount(const unsigned int particleXCount)
{
	m_particleXCount = particleXCount;
}

void SmokeRenderer3d::setShader(const std::string& vertexShader, const std::string& fragmentShader)
{
	m_vertexShaderPath = vertexShader;
	m_fragmentShaderPath = fragmentShader;
}

ci::Vec3f SmokeRenderer3d::getWind() const
{
	return m_smokeManager.getWind();
}

void SmokeRenderer3d::setWind(const ci::Vec3f& wind)
{
	m_smokeManager.setWind(wind);
}

void SmokeRenderer3d::setTerrain(IPolygonBody* terrain)
{
	m_smokeManager.setTerrain(terrain);
}

void SmokeRenderer3d::setup(const ParticleContainer& particles)
{
	m_smokeManager.setStreakHeight(m_streakWidth);
	m_smokeManager.setStreakSpacing(m_streakSpacing);

	m_smokeManager.setMinCorner(meterToScreen(particles.getNormSPHDomainMinCorner() + m_displacement));
	m_smokeManager.setMaxCorner(meterToScreen(particles.getNormSPHDomainMaxCorner() + m_displacement));
	float particleSpacing = meterToScreen((particles.getNormSPHDomainMaxCorner() - particles.getNormSPHDomainMinCorner()).x / (float)particles.getNormParticlesPerDimension());

	m_smokeManager.setParticleSpacing(ci::Vec3f(particleSpacing, particleSpacing, particleSpacing));
	m_smokeManager.move(m_displacement);
	m_smokeManager.setup(particles);
}

void SmokeRenderer3d::setupShader()
{
	if (m_vertexShaderPath.size() > 0 && m_fragmentShaderPath.size() > 0)
	{
		m_shader.load(m_vertexShaderPath, m_fragmentShaderPath);
		m_shader.bindAttribute(0, "vPosition");
		m_shader.bindAttribute(1, "vNormal");
		m_shader.bindAttribute(2, "vTexcoord");
		m_shader.bindAttribute(4, "vArea");
		m_shader.bindAttribute(5, "vFormFactor");
		m_shader.linkShaders();
	}
}

void SmokeRenderer3d::calculateValues(std::vector<ci::Vec3f>& vertices, std::vector<ci::Vec3f>& normals, std::vector<float>& areas, std::vector<float>& shapeFactors, float& averageArea) const
{
	normals.clear();
	areas.clear();

	std::map<unsigned int, ci::Vec3f> normalsMap;
	std::map<unsigned int, float> areasMap;
	std::map<unsigned int, float> areasInfluenceMap;
	std::map<unsigned int, ci::Vec3f> verticesMap;
	std::map<unsigned int, float> shapeFactorMap;

	float sqrt3 = std::sqrt(3.0f);
	float s = 2.0f;

	vertices = m_smokeManager.getVertices();
	std::vector<unsigned int> indices = m_smokeManager.getIndices();

	if (indices.size() <= 0)
	{
		return;
	}

	for (unsigned int i = 0; i < indices.size(); i += 3)
	{
		unsigned int idxA = indices[i];
		unsigned int idxB = indices[i + 1];
		unsigned int idxC = indices[i + 2];

		ci::Vec3f point0 = vertices[idxA];
		ci::Vec3f point1 = vertices[idxB];
		ci::Vec3f point2 = vertices[idxC];

		ci::Vec3f vec01 = point1 - point0;
		ci::Vec3f vec02 = point2 - point0;

		ci::Vec3f normal = vec01.cross(vec02);

		normalsMap[idxA] = normalsMap[idxA] + normal;
		normalsMap[idxB] = normalsMap[idxB] + normal;
		normalsMap[idxC] = normalsMap[idxC] + normal;

		ci::Vec3f a = point0;
		ci::Vec3f b = point1;
		ci::Vec3f c = point2;

		float d = (c - b).length();
		float e = (a - c).length();
		float f = (b - a).length();

		float area = Triangle::getArea3d(a, b, c);

		float areaFactor = std::pow((4.0f * area) / (sqrt3 * std::max(d*e, std::max(e*f, f*d))), s);
		shapeFactorMap[idxA] = shapeFactorMap[idxA] + areaFactor;
		shapeFactorMap[idxB] = shapeFactorMap[idxB] + areaFactor;
		shapeFactorMap[idxC] = shapeFactorMap[idxC] + areaFactor;

		areasMap[idxA] = areasMap[idxA] + area;
		areasMap[idxB] = areasMap[idxB] + area;
		areasMap[idxC] = areasMap[idxC] + area;

		areasInfluenceMap[idxA] = areasInfluenceMap[idxA] + 1.0f;
		areasInfluenceMap[idxB] = areasInfluenceMap[idxB] + 1.0f;
		areasInfluenceMap[idxC] = areasInfluenceMap[idxC] + 1.0f;
	}

	std::map<unsigned int, ci::Vec3f>::iterator it = normalsMap.begin();
	for (it; it != normalsMap.end(); it++)
	{
		normals.push_back((*it).second.normalized());
	}

	it = verticesMap.begin();
	for (it; it != verticesMap.end(); it++)
	{
		vertices.push_back((*it).second);
	}

	float maxArea = 0.0f;
	float minArea = 9999999.9f;
	std::map<unsigned int, float>::iterator it2 = areasMap.begin();
	std::map<unsigned int, float>::iterator it3 = areasInfluenceMap.begin();
	std::map<unsigned int, float>::iterator it4 = shapeFactorMap.begin();
	for (it2; it2 != areasMap.end(); it2++)
	{
		float area = (*it2).second / (*it3).second;
		float shapeFactor = (*it4).second / (*it3).second;
		areas.push_back(area);
		shapeFactors.push_back(shapeFactor);
		if (area > maxArea)
		{
			maxArea = area;
		}
		if (area < minArea)
		{
			minArea = area;
		}

		averageArea += area;

		it3++;
		it4++;
	}

	averageArea /= areasMap.size();
}