#include "SimpleParticleRenderer.h"

#include "cinder/gl/gl.h"

SimpleParticleRenderer::SimpleParticleRenderer()
{

}

SimpleParticleRenderer::~SimpleParticleRenderer()
{

}

void SimpleParticleRenderer::update(const ParticleContainer& particles, const float deltaTime)
{

}

void SimpleParticleRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	renderParticles(particles);
}

std::string SimpleParticleRenderer::getName() const
{
	return "SimpleParticleRenderer";
}

void SimpleParticleRenderer::move(const ci::Vec3f& displacement)
{

}

void SimpleParticleRenderer::renderParticles(const ParticleContainer& particles)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ci::gl::color(ci::ColorAf(0.0f, 0.8f, 0.3f, 1.0f));
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
	}

	glDisable(GL_ALPHA);
}
