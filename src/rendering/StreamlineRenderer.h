#ifndef STREAMLINE_RENDERER_H
#define STREAMLINE_RENDERER_H

#include "IRenderer.h"

#include "smokeRendererUtility/SmokeManager.h"

#include "simulation/bodies/IPolygonBody.h"

class StreamlineRenderer : public IRenderer
{
public:
	StreamlineRenderer();
	virtual ~StreamlineRenderer();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	void setTerrain(IPolygonBody* terrain);

private:
	void setup(const ParticleContainer& particles);

	bool m_setupComplete;
	SmokeManager m_smokeManager;
};

#endif // STREAMLINE_RENDERER_H