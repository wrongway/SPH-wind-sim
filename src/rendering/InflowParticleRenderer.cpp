#include "InflowParticleRenderer.h"

#include "cinder/gl/gl.h"

InflowParticleRenderer::InflowParticleRenderer()
{

}

InflowParticleRenderer::~InflowParticleRenderer()
{

}

void InflowParticleRenderer::update(const ParticleContainer& particles, const float deltaTime)
{

}

void InflowParticleRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	std::pair<float, float> minMaxDensity = calculateMinMaxDensity(particles);
	renderParticles(particles, minMaxDensity);
}

std::string InflowParticleRenderer::getName() const
{
	return "InflowParticleRenderer";
}

void InflowParticleRenderer::move(const ci::Vec3f& displacement)
{

}

void InflowParticleRenderer::renderParticles(const ParticleContainer& particles, const std::pair<float, float>& minMaxDensity)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// ci::gl::color(ci::ColorAf(0.3f, 0.8f, 0.0f, 1.0f));
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float restDensity = particles[i].getDensity();
		float density = particles[i].getSPHDensity();
		float g = (density - minMaxDensity.first) / (minMaxDensity.second - minMaxDensity.first);
		float b = 0.3f;

		/*if (density > restDensity)
		{
			ci::gl::color(ci::ColorAf(g, 0.0f, b, 1.0f));
		}
		else
		{
			ci::gl::color(ci::ColorAf(0.0f, g, b, 1.0f));
		}*/

		ci::gl::color(ci::ColorAf(0.0f, g, b, 1.0f));

		if (particles[i].getSimulationFactor() <= 0.0f && particles[i].getIsGhost() == false)
		{
			ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
		}
	}

	glDisable(GL_ALPHA);
}

std::pair<float, float> InflowParticleRenderer::calculateMinMaxDensity(const ParticleContainer& particles)
{
	float minDensity = FLT_MAX;
	float maxDensity = FLT_MIN;

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getIsGhost())
		{
			continue;
		}

		float currentDensity = particles[i].getSPHDensity();

		if (currentDensity < minDensity)
		{
			minDensity = currentDensity;
		}
		if (currentDensity > maxDensity)
		{
			maxDensity = currentDensity;
		}
	}

	return std::make_pair(minDensity, maxDensity);
}