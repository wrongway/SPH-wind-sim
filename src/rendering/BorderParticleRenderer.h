#ifndef BORDER_PARTICLE_RENDERER_H
#define BORDER_PARTICLE_RENDERER_H

#include "IRenderer.h"

class BorderParticleRenderer : public IRenderer
{
public:
	BorderParticleRenderer();
	virtual ~BorderParticleRenderer();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

private:
	void renderParticles(const ParticleContainer& particles, const std::pair<float, float>& minMaxDensity);
	std::pair<float, float> calculateMinMaxDensity(const ParticleContainer& particles);
};

#endif // BORDER_PARTICLE_RENDERER_H