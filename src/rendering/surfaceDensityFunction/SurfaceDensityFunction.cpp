#include "SurfaceDensityFunction.h"

SurfaceDensityFunction::SurfaceDensityFunction()
	: m_kernelSupportRadius(0.0f)
	, m_restDensity(0.0f)
	, m_densityKernel(NULL)
	, m_particles(NULL)
{

}

SurfaceDensityFunction::~SurfaceDensityFunction()
{

}

float SurfaceDensityFunction::getValue(ci::Vec3f& position) const
{
	float density = 0.0f;

	if (m_densityKernel != NULL || m_particles == NULL)
	{
		for (unsigned int i = 0; i < m_particles->size(); i++)
		{
			ci::Vec3f seperation = (*m_particles)[i].getPosition() - position;
			float distance = seperation.length();

			if (distance < m_kernelSupportRadius)
			{
				density += (m_densityKernel->kernel(distance, m_kernelSupportRadius) - m_restDensity);
			}
		}
	}
	
	return density;
}

void SurfaceDensityFunction::setKernelSupportRadius(const float kernelSupportRadius)
{
	m_kernelSupportRadius = kernelSupportRadius;
}

void SurfaceDensityFunction::setRestDensity(const float restDensity)
{
	m_restDensity = restDensity;
}

void SurfaceDensityFunction::setDensityKernel(const std::shared_ptr<IKernelFunction>& kernelFunction)
{
	m_densityKernel = kernelFunction;
}

void SurfaceDensityFunction::setParticles(std::vector<Particle>* particles)
{
	m_particles = particles;
}