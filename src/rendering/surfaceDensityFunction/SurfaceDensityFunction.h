#ifndef SURFACE_DENSITY_FUNCTION
#define SURFACE_DENSITY_FUNCTION

#include "terrain/densityFunction/IDensityFunction.h"

#include "simulation/kernels/IKernelFunction.h"
#include "simulation/particle/Particle.h"

class SurfaceDensityFunction : public IDensityFunction
{
public:
	SurfaceDensityFunction();
	virtual ~SurfaceDensityFunction();

	virtual float getValue(ci::Vec3f& position) const;

	void setKernelSupportRadius(const float kernelSupportRadius);
	void setRestDensity(const float restDensity);
	void setDensityKernel(const std::shared_ptr<IKernelFunction>& kernelFunction);
	void setParticles(std::vector<Particle>* particles);

private:
	float m_kernelSupportRadius;
	float m_restDensity;

	std::shared_ptr<IKernelFunction> m_densityKernel;
	std::vector<Particle>* m_particles;
};

#endif // SURFACE_DENSITY_FUNCTION