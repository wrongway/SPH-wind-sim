#ifndef SIMPLE_PARTICLE_RENDERER_H
#define SIMPLE_PARTICLE_RENDERER_H

#include "IRenderer.h"

class SimpleParticleRenderer : public IRenderer
{
public:
	SimpleParticleRenderer();
	virtual ~SimpleParticleRenderer();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

private:
	void renderParticles(const ParticleContainer& particles);
};

#endif // SIMPLE_PARTICLE_RENDERER_H