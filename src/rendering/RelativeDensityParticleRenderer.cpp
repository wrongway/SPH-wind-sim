#include "RelativeDensityParticleRenderer.h"

#include <cfloat>

#include "cinder/gl/gl.h"

RelativeDensityParticleRenderer::RelativeDensityParticleRenderer()
{

}

RelativeDensityParticleRenderer::~RelativeDensityParticleRenderer()
{

}

void RelativeDensityParticleRenderer::update(const ParticleContainer& particles, const float deltaTime)
{

}

void RelativeDensityParticleRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	std::pair<float, float> minMaxDensity = calculateMinMaxDensity(particles);
	renderParticles(particles, minMaxDensity);
}

std::string RelativeDensityParticleRenderer::getName() const
{
	return "RelativeDensityParticleRenderer";
}

void RelativeDensityParticleRenderer::move(const ci::Vec3f& displacement)
{

}

void RelativeDensityParticleRenderer::renderParticles(const ParticleContainer& particles, const std::pair<float, float>& minMaxDensity)
{
	glEnable(GL_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float density = particles[i].getSPHDensity();
		float g = (density - minMaxDensity.first) / (minMaxDensity.second - minMaxDensity.first);
		ci::gl::color(ci::ColorAf(0.0f, g, 0.3f, 1.0f));

		ci::gl::drawSphere(meterToScreen(particles[i].getPosition()), meterToScreen(particles[i].getRadius()));
	}

	glDisable(GL_ALPHA);
}

std::pair<float, float> RelativeDensityParticleRenderer::calculateMinMaxDensity(const ParticleContainer& particles)
{
	float minDensity = FLT_MAX;
	float maxDensity = FLT_MIN;

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getIsGhost())
		{
			continue;
		}

		float currentDensity = particles[i].getSPHDensity();

		if (currentDensity < minDensity)
		{
			minDensity = currentDensity;
		}
		if (currentDensity > maxDensity)
		{
			maxDensity = currentDensity;
		}
	}

	return std::make_pair(minDensity, maxDensity);
}