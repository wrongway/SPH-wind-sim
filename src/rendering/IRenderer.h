#ifndef I_RENDERER_H
#define I_RENDERER_H

#include <vector>

#include "core/UnitUtility.h"

#include "cinder/Camera.h"

#include "simulation/particle/Particle.h"
#include "simulation/particle/ParticleContainer.h"

class IRenderer
{
public:
	IRenderer(){}
	virtual~IRenderer(){}

	virtual void update(const ParticleContainer& particles, const float deltaTime) = 0;
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera) = 0;

	virtual std::string getName() const = 0;

	virtual void move(const ci::Vec3f& displacement) = 0;
};

#endif // I_RENDERER_H