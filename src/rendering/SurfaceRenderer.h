#ifndef SURFACE_RENDERER_H
#define SURFACE_RENDERER_H

#include "cinder/MayaCamUI.h"

#include "core/graphics/MeshBufferObject.h"
#include "core/graphics/Shader.h"

#include "surfaceDensityFunction/SurfaceDensityFunction.h"

#include "terrain/terrain/TerrainGenerator.h"

#include "IRenderer.h"

class SurfaceRenderer : public IRenderer
{
public:
	SurfaceRenderer();
	virtual~SurfaceRenderer();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

	void loadShader(const std::string& vertexPath, const std::string& fragmentPath);

	void setDomainSize(const float domainSize);
	void setCellSize(const float cellSize);

	void setKernelSupportRadius(const float supportRadius);
	void setRestDensity(const float restDensity);

private:
	void calculateSurface(ParticleContainer& particles);

	SurfaceDensityFunction m_densityFunction;
	float m_domainSize;
	float m_cellSize;

	ci::Vec3f m_lightPosition;

	MeshBufferObject m_meshBufferObject;
	Shader m_phongShader;
	Terrain* m_terrain;

	bool m_hasMeshToRender;
};

#endif // SURFACE_RENDERER_H