#ifndef SMOKE_STREAK_H
#define SMOKE_STREAK_H

#include <memory>
#include <vector>

#include "cinder/Vector.h"

#include "SmokeParticle.h"

#include "simulation/bodies/IPolygonBody.h"

class SmokeStreak
{
public:
	SmokeStreak();
	~SmokeStreak();

	ci::Vec3f getEmitterPosition() const;
	void setEmitterPosition(const ci::Vec3f& position);

	unsigned int getParticleHeight() const;
	void setParticleHeight(const unsigned int particleHeight);

	float getParticleSpacing() const;
	void setParticleSpacing(const float particleSpacing);

	float getTargetAltitude() const;
	void setTargetAltitude(const float altitude);

	struct Triangle
	{
	public:
		Triangle(unsigned int idxA, unsigned int idxB, unsigned int idxC)
			: m_idxA(idxA)
			, m_idxB(idxB)
			, m_idxC(idxC)
		{}

		unsigned int m_idxA;
		unsigned int m_idxB;
		unsigned int m_idxC;
	};

	void update();

	unsigned int getParticleCount() const;
	std::vector<std::shared_ptr<SmokeParticle>> getParticles() const;
	std::shared_ptr<SmokeParticle> getParticle(const unsigned int index) const;

	void pushParticleToKill(const unsigned int index);

	void setTerrain(IPolygonBody* terrain);

	std::vector<Triangle> getTriangles() const;

	bool getIsHorizontal() const;
	void setIsHorizontal(const bool isHorizontal);

private:
	void emitColumn();
	void updateTriangles();
	void killParticles();

	ci::Vec3f m_emitterPosition;
	unsigned int m_particleHeight;
	float m_particleSpacing;

	std::vector<std::shared_ptr<SmokeParticle>> m_particles;
	std::vector<Triangle> m_triangles;

	std::vector<unsigned int> m_particlesToKill;

	IPolygonBody* m_terrain;

	float m_targetAltitude;

	bool m_isHorizontal;
};

#endif // SMOKE_STREAK_H