#ifndef SMOKE_PARTICLE_H
#define SMOKE_PARTICLE_H

#include <memory>

#include "cinder/Vector.h"

class SmokeParticle
{
public:
	SmokeParticle();
	~SmokeParticle();

	ci::Vec3f getPosition() const;
	void setPosition(const ci::Vec3f& position);

	std::shared_ptr<SmokeParticle> getLeftNeighbour() const;
	void setLeftNeighbour(const std::weak_ptr<SmokeParticle>& leftNeighbour);

	std::shared_ptr<SmokeParticle> getRightNeighbour() const;
	void setRightNeighbour(const std::weak_ptr<SmokeParticle>& rightNeighbour);

	std::shared_ptr<SmokeParticle> getUpperNeighbour() const;
	void setUpperNeighbour(const std::weak_ptr<SmokeParticle>& upperNeighbour);

	std::shared_ptr<SmokeParticle> getLowerNeighbour() const;
	void setLowerNeighbour(const std::weak_ptr<SmokeParticle>& lowerNeighbour);

	void increaseEvolution();
	unsigned int getEvolution() const;
	void resetEvolution();

	float getTargetAltitude() const;
	void setTargetAltitude(const float targetAltitude);

private:
	ci::Vec3f m_position;

	std::weak_ptr<SmokeParticle> m_leftNeighbour;
	std::weak_ptr<SmokeParticle> m_rightNeighbour;
	std::weak_ptr<SmokeParticle> m_upperNeighbour;
	std::weak_ptr<SmokeParticle> m_lowerNeighbour;

	unsigned int m_evolution;

	float m_targetAltitude;
};

#endif // SMOKE_PARTICLE_H