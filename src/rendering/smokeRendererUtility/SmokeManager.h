#ifndef SMOKE_MANAGER_H
#define SMOKE_MANAGER_H

#include "core/algorithm/RegularGrid3d.h"

#include "simulation/particle/Particle.h"
#include "simulation/particle/ParticleContainer.h"

#include "SmokeStreak.h"

#include "simulation/bodies/IPolygonBody.h"

class SmokeManager
{
public:
	SmokeManager();
	~SmokeManager();

	void setup(const ParticleContainer& particles);

	void update(const ParticleContainer& particles, const float deltaTime);

	void move(const ci::Vec3f& displacement);

	ci::Vec3f getMinCorner() const;
	void setMinCorner(const ci::Vec3f& minCorner);

	ci::Vec3f getMaxCorner() const;
	void setMaxCorner(const ci::Vec3f& maxCorner);

	ci::Vec3f getParticleSpacing() const;
	void setParticleSpacing(const ci::Vec3f& particleSpacing);

	float getStreakHeight() const;
	void setStreakHeight(const float height);

	float getStreakSpacing() const;
	void setStreakSpacing(const float spacing);

	std::vector<ci::Vec3f> getVertices() const;
	std::vector<unsigned int> getIndices() const;

	std::vector<ci::Vec3f> getEmitterPositions() const;

	std::vector<std::vector<ci::Vec3f>> getVerticesByStreak() const;

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

	void setTerrain(IPolygonBody* terrain);

private:
	SmokeStreak setupStreak(const float startAltitude, const float startZ);

	void updateStreaks(const RegularGrid3D<Particle>& grid, const float deltaTime);
	void updateStreak(SmokeStreak& streak, const RegularGrid3D<Particle>& grid, const float deltaTime);

	void updateParticleAltitudeOverTerrain();

	float getCorrectionFactor(const float windSpeed, const float agl, bool negativ) const;

	ci::Vec3f m_minCorner;
	ci::Vec3f m_maxCorner;

	ci::Vec3f m_particleSpacing;
	float m_streakHeight;
	float m_streakSpacing;

	float m_particleDiameter;

	std::vector<SmokeStreak> m_smokeStreaks;

	ci::Vec3f m_wind;

	IPolygonBody* m_terrain;
};

#endif // SMOKE_MANAGER_H