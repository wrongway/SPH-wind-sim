#include "SmokeParticle.h"

SmokeParticle::SmokeParticle()
	: m_position(0.0f, 0.0f, 0.0f)
	, m_leftNeighbour()
	, m_rightNeighbour()
	, m_upperNeighbour()
	, m_lowerNeighbour()
	, m_evolution(0)
{

}

SmokeParticle::~SmokeParticle()
{

}

ci::Vec3f SmokeParticle::getPosition() const
{
	return m_position;
}

void SmokeParticle::setPosition(const ci::Vec3f& position)
{
	m_position = position;
}

std::shared_ptr<SmokeParticle> SmokeParticle::getLeftNeighbour() const
{
	return m_leftNeighbour.lock();
}

void SmokeParticle::setLeftNeighbour(const std::weak_ptr<SmokeParticle>& leftNeighbour)
{
	m_leftNeighbour = leftNeighbour;
}

std::shared_ptr<SmokeParticle> SmokeParticle::getRightNeighbour() const
{
	return m_rightNeighbour.lock();
}

void SmokeParticle::setRightNeighbour(const std::weak_ptr<SmokeParticle>& rightNeighbour)
{
	m_rightNeighbour = rightNeighbour;
}

std::shared_ptr<SmokeParticle> SmokeParticle::getUpperNeighbour() const
{
	return m_upperNeighbour.lock();
}

void SmokeParticle::setUpperNeighbour(const std::weak_ptr<SmokeParticle>& upperNeighbour)
{
	m_upperNeighbour = upperNeighbour;
}

std::shared_ptr<SmokeParticle> SmokeParticle::getLowerNeighbour() const
{
	return m_lowerNeighbour.lock();
}

void SmokeParticle::setLowerNeighbour(const std::weak_ptr<SmokeParticle>& lowerNeighbour)
{
	m_lowerNeighbour = lowerNeighbour;
}

void SmokeParticle::increaseEvolution()
{
	m_evolution++;
}

unsigned int SmokeParticle::getEvolution() const
{
	return m_evolution;
}

void SmokeParticle::resetEvolution()
{
	m_evolution = 0;
}

float SmokeParticle::getTargetAltitude() const
{
	return m_targetAltitude;
}

void SmokeParticle::setTargetAltitude(const float targetAltitude)
{
	m_targetAltitude = targetAltitude;
}