#include "SmokeManager.h"

#include "core/UnitUtility.h"

#include "core/utility/logging/logging.h"

SmokeManager::SmokeManager()
	: m_minCorner(0.0f, 0.0f, 0.0f)
	, m_maxCorner(0.0f, 0.0f, 0.0f)
	, m_particleDiameter(0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_terrain(NULL)
{

}

SmokeManager::~SmokeManager()
{

}

void SmokeManager::setup(const ParticleContainer& particles)
{
	if (particles.size() <= 0)
	{
		return;
	}

	float height = m_maxCorner.y - m_minCorner.y;
	float width = m_maxCorner.z - m_minCorner.z;
	m_particleDiameter = meterToScreen(particles[0].getDiameter());
	float streakDelta = (m_streakHeight + m_streakSpacing) * m_particleDiameter;
	unsigned int streakCount = height / streakDelta;
	unsigned int streakCountZ = width / streakDelta;
	streakCount++;

	streakCount = 1;

	float minZ = m_minCorner.z;
	float y = (m_minCorner.y + m_maxCorner.y) * 0.5f;

	for (unsigned int i = 0; i < streakCount; i++)
	{
		SmokeStreak streak = setupStreak(/*m_particleDiameter + i * streakDelta*/ height * 0.15f, minZ);
		if (streakCountZ > 0)
		{
			streak.setIsHorizontal(true);
			streak.setParticleHeight(width / m_particleDiameter);
		}
		m_smokeStreaks.push_back(streak);
	}
}

void SmokeManager::update(const ParticleContainer& particles, const float deltaTime)
{
	ci::Vec3f minCorner = meterToScreen(particles.getBoundingBoxMinCorner());
	ci::Vec3f maxCorner = meterToScreen(particles.getBoundingBoxMaxCorner());

	ci::Vec3f center = (minCorner + maxCorner) * 0.5f;
	// ci::Vec2f center2d(center.x, center.y);

	float sideLength = 0.0f;
	float length = (maxCorner - minCorner).x;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).y;
	if (length > sideLength)
	{
		sideLength = length;
	}
	length = (maxCorner - minCorner).z;
	if (length > sideLength)
	{
		sideLength = length;
	}

	float streakLength = m_maxCorner.x - m_minCorner.x;

	unsigned int particleXCount = streakLength / m_particleSpacing.x;

	RegularGrid3D<Particle> grid;
	grid.setCenter(center);
	grid.setGridSideLength(sideLength);
	grid.setCellsPerDimension(particleXCount);

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f pos = meterToScreen(particles[i].getPosition());
		grid.addValue(pos, particles[i]);
	}

	updateStreaks(grid, deltaTime);
}

void SmokeManager::move(const ci::Vec3f& displacement)
{
	m_minCorner += displacement;
	m_maxCorner += displacement;

	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		ci::Vec3f newPos = m_smokeStreaks[i].getEmitterPosition() + displacement;

		if (m_terrain != NULL)
		{
			float altitude = 0.0f;
			if (m_terrain->getAltitudeAtPosition(screenToMeter(newPos), altitude))
			{
				newPos.y = meterToScreen(altitude) + m_smokeStreaks[i].getTargetAltitude();
			}
		}

		m_smokeStreaks[i].setEmitterPosition(newPos);
	}
}

ci::Vec3f SmokeManager::getMinCorner() const
{
	return m_minCorner;
}

void SmokeManager::setMinCorner(const ci::Vec3f& minCorner)
{
	m_minCorner = minCorner;
}

ci::Vec3f SmokeManager::getMaxCorner() const
{
	return m_maxCorner;
}

void SmokeManager::setMaxCorner(const ci::Vec3f& maxCorner)
{
	m_maxCorner = maxCorner;
}

ci::Vec3f SmokeManager::getParticleSpacing() const
{
	return m_particleSpacing;
}

void SmokeManager::setParticleSpacing(const ci::Vec3f& particleSpacing)
{
	m_particleSpacing = particleSpacing;
}

float SmokeManager::getStreakHeight() const
{
	return m_streakHeight;
}

void SmokeManager::setStreakHeight(const float height)
{
	m_streakHeight = height;
}

float SmokeManager::getStreakSpacing() const
{
	return m_streakSpacing;
}

void SmokeManager::setStreakSpacing(const float spacing)
{
	m_streakSpacing = spacing;
}

std::vector<ci::Vec3f> SmokeManager::getVertices() const
{
	std::vector<ci::Vec3f> vertices;

	for (unsigned int i = 0;  i < m_smokeStreaks.size(); i++)
	{
		std::vector<std::shared_ptr<SmokeParticle>> particles = m_smokeStreaks[i].getParticles();

		for (unsigned int j = 0; j < particles.size(); j++)
		{
			vertices.push_back(particles[j]->getPosition());
		}
	}

	return vertices;
}

std::vector<unsigned int> SmokeManager::getIndices() const
{
	std::vector<unsigned int> indices;

	unsigned int indexOffset = 0;

	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		unsigned int maxIndex = 0;

		std::vector<SmokeStreak::Triangle> triangles = m_smokeStreaks[i].getTriangles();

		for (unsigned int j = 0; j < triangles.size(); j++)
		{
			unsigned int idxA = triangles[j].m_idxA + indexOffset;
			unsigned int idxB = triangles[j].m_idxB + indexOffset;
			unsigned int idxC = triangles[j].m_idxC + indexOffset;

			if (m_smokeStreaks[i].getParticle(triangles[j].m_idxA)->getEvolution() != m_smokeStreaks[i].getParticle(triangles[j].m_idxB)->getEvolution()
				|| m_smokeStreaks[i].getParticle(triangles[j].m_idxA)->getEvolution() != m_smokeStreaks[i].getParticle(triangles[j].m_idxC)->getEvolution())
			{
				continue;
			}

			indices.push_back(idxA);
			indices.push_back(idxB);
			indices.push_back(idxC);

			if (triangles[j].m_idxA > maxIndex)
			{
				maxIndex = triangles[j].m_idxA;
			}
			if (triangles[j].m_idxB > maxIndex)
			{
				maxIndex = triangles[j].m_idxB;
			}
			if (triangles[j].m_idxC > maxIndex)
			{
				maxIndex = triangles[j].m_idxC;
			}
		}

		indexOffset += maxIndex+1;
	}

	return indices;
}

std::vector<ci::Vec3f> SmokeManager::getEmitterPositions() const
{
	std::vector<ci::Vec3f> positions;

	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		positions.push_back(m_smokeStreaks[i].getEmitterPosition());
	}

	return positions;
}

std::vector<std::vector<ci::Vec3f>> SmokeManager::getVerticesByStreak() const
{
	std::vector<std::vector<ci::Vec3f>> verticesPerStreak;

	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		std::vector<ci::Vec3f> vertices;

		std::vector<std::shared_ptr<SmokeParticle>> particles = m_smokeStreaks[i].getParticles();

		for (unsigned int j = 0; j < particles.size(); j++)
		{
			vertices.push_back(particles[j]->getPosition());
		}

		verticesPerStreak.push_back(vertices);
	}

	return verticesPerStreak;
}

ci::Vec3f SmokeManager::getWind() const
{
	return m_wind;
}

void SmokeManager::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

void SmokeManager::setTerrain(IPolygonBody* terrain)
{
	m_terrain = terrain;

	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		m_smokeStreaks[i].setTerrain(m_terrain);
	}
}

SmokeStreak SmokeManager::setupStreak(const float startAltitude, const float startZ)
{
	float emitterX = m_maxCorner.x;
	float emitterY = m_minCorner.y + startAltitude;
	float emitterZ = startZ; // (m_minCorner.z + m_maxCorner.z) * 0.5f;

	ci::Vec3f position(emitterX, emitterY, emitterZ);

	float altitude = 0.0f;
	if (m_terrain != NULL)
	{
		m_terrain->getAltitudeAtPosition(screenToMeter(position), altitude);
		position.y = meterToScreen(altitude) + startAltitude;
	}

	SmokeStreak streak;

	streak.setTargetAltitude(startAltitude);

	float streakHeight = m_streakHeight * m_particleDiameter;

	streak.setEmitterPosition(position);
	streak.setParticleHeight(m_streakHeight);
	streak.setParticleSpacing(m_particleDiameter);
	streak.setTerrain(m_terrain);

	return streak;
}

void SmokeManager::updateStreaks(const RegularGrid3D<Particle>& grid, const float deltaTime)
{
	for (unsigned int i = 0; i < m_smokeStreaks.size(); i++)
	{
		updateStreak(m_smokeStreaks[i], grid, deltaTime);
	}
}

void SmokeManager::updateStreak(SmokeStreak& streak, const RegularGrid3D<Particle>& grid, const float deltaTime)
{
	streak.update();

	for (unsigned int i = 0; i < streak.getParticleCount(); i++)
	{
		std::shared_ptr<SmokeParticle> particle = streak.getParticle(i);
		ci::Vec3f pos = particle->getPosition();

		ci::Vec3f gridCenter = grid.getCenter();
		// ci::Vec3f pos3d(pos.x, pos.y, pos.z);

		std::vector<std::vector<Particle>> sphParticles = grid.getCells(pos, 0);

		ci::Vec3f cellVelocity(0.0f, 0.0f, 0.0f);
		float count = 0.0f;

		for (unsigned int j = 0; j < sphParticles.size(); j++)
		{
			for (unsigned int k = 0; k < sphParticles[j].size(); k++)
			{
				ci::Vec3f vel = meterToScreen(sphParticles[j][k].getVelocity());
				float simFactor = sphParticles[j][k].getSimulationFactor();
				/*if (simFactor <= 0.0f)
				{
					cellVelocity += m_wind;
				}
				else
				{
					cellVelocity += vel;
				}*/

				cellVelocity += (vel * simFactor + m_wind * (1.0f - simFactor));
				
				count += 1.0f;
			}
		}

		if (count > 0.0f)
		{
			cellVelocity /= count;

			float altitude = 0.0f;
			ci::Vec3f meterPos = screenToMeter(pos);
			float correctionFactor = 1.0f;
			if (m_terrain != NULL && m_terrain->getAltitudeAtPosition(meterPos, altitude))
			{
				correctionFactor = getCorrectionFactor(screenToMeter(cellVelocity).length(), meterPos.y - altitude, cellVelocity.y < 0.0f);
			}

			cellVelocity.y /= correctionFactor;

			//LOG_WARNING_STREAM(<< "move by cell velocity");

			pos += cellVelocity * deltaTime;
		}
		else
		{
			//LOG_WARNING_STREAM(<< "move by wind");

			pos += m_wind * deltaTime;
		}

		particle->setPosition(pos);
	}
}

void SmokeManager::updateParticleAltitudeOverTerrain()
{

}

float SmokeManager::getCorrectionFactor(const float windSpeed, const float agl, bool negativ) const
{
	float aglFactor = 1.0f;

	if (negativ == false)
	{
		aglFactor = 12.24134f
			+ (agl * -0.04341144f)
			+ (std::pow(agl, 2.0f) * 0.0001896047f)
			+ (std::pow(agl, 3.0f) * -0.000000250851f)
			+ (std::pow(agl, 4.0f) * 0.000000000126271f);
	}
	else
	{
		aglFactor = 12.76619f
			+ (agl * -0.05343759f)
			+ (std::pow(agl, 2.0f) * 0.0001450935f)
			+ (std::pow(agl, 3.0f) * -0.0000001263247f)
			+ (std::pow(agl, 4.0f) * 0.00000000005227062f);
	}

	float windFactor = ((1.0f / windSpeed) * 2.0472f) + 0.3032f;

	return aglFactor * windFactor * 0.25f;
}