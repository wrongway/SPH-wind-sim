#include "SmokeStreak.h"

#include "core/utility/logging/logging.h"

SmokeStreak::SmokeStreak()
	: m_emitterPosition()
	, m_particleHeight()
	, m_particleSpacing()
	, m_particles()
	, m_triangles()
	, m_particlesToKill()
	, m_terrain(NULL)
	, m_targetAltitude(0.0f)
	, m_isHorizontal(false)
{

}

SmokeStreak::~SmokeStreak()
{

}

ci::Vec3f SmokeStreak::getEmitterPosition() const
{
	return m_emitterPosition;
}

void SmokeStreak::setEmitterPosition(const ci::Vec3f& position)
{
	m_emitterPosition = position;
}

unsigned int SmokeStreak::getParticleHeight() const
{
	return m_particleHeight;
}

void SmokeStreak::setParticleHeight(const unsigned int particleHeight)
{
	m_particleHeight = particleHeight;
}

float SmokeStreak::getParticleSpacing() const
{
	return m_particleSpacing;
}

void SmokeStreak::setParticleSpacing(const float particleSpacing)
{
	m_particleSpacing = particleSpacing;
}

float SmokeStreak::getTargetAltitude() const
{
	return m_targetAltitude;
}

void SmokeStreak::setTargetAltitude(const float altitude)
{
	m_targetAltitude = altitude;
}

void SmokeStreak::update()
{
	killParticles();

	unsigned int particleCount = m_particles.size();

	unsigned int emit = 0;
	if (particleCount > 0 && particleCount >= m_particleHeight - 1)
	{
		unsigned int startIndex = (particleCount) - m_particleHeight;

		for (unsigned int i = startIndex; i < particleCount; i++)
		{
			if (std::abs(m_emitterPosition.z - m_particles[i]->getPosition().z) > m_particleSpacing)
			{
				emit++;
			}
		}

		if (emit == m_particleHeight)
		{
			emitColumn();

			updateTriangles();
		}
	}
	else
	{
		emitColumn();
	}
}

unsigned int SmokeStreak::getParticleCount() const
{
	return m_particles.size();
}

std::vector<std::shared_ptr<SmokeParticle>> SmokeStreak::getParticles() const
{
	return m_particles;
}

std::shared_ptr<SmokeParticle> SmokeStreak::getParticle(const unsigned int index) const
{
	if (index < m_particles.size())
	{
		return m_particles[index];
	}
	else
	{
		LOG_ERROR_STREAM(<< "Index out of range, is " << index << ", max is " << m_particles.size()-1);
		return std::make_shared<SmokeParticle>();
	}
}

void SmokeStreak::pushParticleToKill(const unsigned int index)
{
	m_particlesToKill.push_back(index);
}

void SmokeStreak::setTerrain(IPolygonBody* terrain)
{
	m_terrain = terrain;
}

std::vector<SmokeStreak::Triangle> SmokeStreak::getTriangles() const
{
	return m_triangles;
}

bool SmokeStreak::getIsHorizontal() const
{
	return m_isHorizontal;
}

void SmokeStreak::setIsHorizontal(const bool isHorizontal)
{
	m_isHorizontal = isHorizontal;
}

void SmokeStreak::emitColumn()
{
	LOG_WARNING_STREAM(<< "emit");

	ci::Vec3f position = m_emitterPosition;

	for (unsigned int i = 0; i < m_particleHeight; i++)
	{
		std::shared_ptr<SmokeParticle> newParticle = std::make_shared<SmokeParticle>();

		newParticle->setPosition(position);

		m_particles.push_back(newParticle);

		if (m_isHorizontal)
		{
			position.x -= m_particleSpacing;
		}
		else
		{
			position.y += m_particleSpacing;
		}
	}
}

void SmokeStreak::updateTriangles()
{
	if (m_particles.size() >= 2 * m_particleHeight && m_particles.size() > 0)
	{
		unsigned int count = m_particles.size()-1;

		for (unsigned int i = 0; i < m_particleHeight - 1; i++)
		{
			unsigned int idxA = count - i;
			unsigned int idxB = count - (i + 1);
			unsigned int idxC = count - (i + m_particleHeight);
			unsigned int idxD = count - (i + 1 + m_particleHeight);

			SmokeStreak::Triangle triangle0(idxA, idxB, idxC);
			SmokeStreak::Triangle triangle1(idxB, idxD, idxC);

			if (m_isHorizontal)
			{
				triangle0 = SmokeStreak::Triangle(idxA, idxC, idxB);
				triangle1 = SmokeStreak::Triangle(idxB, idxC, idxD);
			}

			m_triangles.push_back(triangle0);
			m_triangles.push_back(triangle1);
		}
	}
}

void SmokeStreak::killParticles()
{
	std::vector<std::vector<std::shared_ptr<SmokeParticle>>::iterator> toErase;

	std::vector<std::shared_ptr<SmokeParticle>>::iterator it = m_particles.begin();
	for (unsigned int i = 0; i < m_particlesToKill.size(); i++)
	{
		toErase.push_back(it+m_particlesToKill[i]);
	}

	for (unsigned int i = 0; i < toErase.size(); i++)
	{
		m_particles.erase(toErase[i]);
	}

	m_particlesToKill.clear();
}