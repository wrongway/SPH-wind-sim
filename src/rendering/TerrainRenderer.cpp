#include "TerrainRenderer.h"

#include "terrain/terrain/Terrain.h"

TerrainRenderer::TerrainRenderer()
	: m_meshGenerated(false)
{

}

TerrainRenderer::~TerrainRenderer()
{

}

void TerrainRenderer::setup(const std::string& vertexPath, const std::string& fragmentPath)
{
	m_phongShader.load(vertexPath, fragmentPath);
	m_phongShader.bindAttribute(0, "vPosition");
	m_phongShader.bindAttribute(1, "vNormal");
	m_phongShader.bindAttribute(2, "vTexcoord");
	m_phongShader.linkShaders();
}

void TerrainRenderer::render(const Terrain* terrain, const ci::Camera& camera, bool wireframe)
{
	if (m_meshGenerated == false)
	{
		setupTerrainMesh(terrain);
	}

	ci::Matrix44f mat_modelview = camera.getModelViewMatrix();
	ci::Matrix44f mat_model_view_proj = camera.getProjectionMatrix() * mat_modelview;

	ci::Vec3f lightPos(0.0f, 1.0f, 0.0f);

	ci::gl::pushMatrices();
		ci::gl::enableDepthWrite();
		ci::gl::enableDepthRead();
		if (wireframe)
		{
			ci::gl::enableWireframe();
		}

		m_phongShader.bind();
			m_phongShader.setUniform("modelviewMatrix", mat_modelview, false);
			m_phongShader.setUniform("modelviewProjectionMatrix", mat_model_view_proj, false);
			m_phongShader.setUniform("vCameraPos", camera.getEyePoint());

			m_phongShader.setUniform("fLightAmbient", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fLightDiffuse", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fLightSpecular", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fShinyness", 20.0f);

			m_phongShader.setUniform("fMaterialSpecular", ci::Vec4f(0.1f, 0.3f, 0.12f, 1.0f));
			m_phongShader.setUniform("vLightPosition", lightPos);

			m_terrainMesh.bindVertexArray();
			m_terrainMesh.draw();

		m_phongShader.unbind();
		
		if (wireframe)
		{
			ci::gl::disableWireframe();
		}
	ci::gl::popMatrices();
}

std::string TerrainRenderer::getName() const
{
	return "TerrainRenderer";
}

void TerrainRenderer::setupTerrainMesh(const Terrain* terrain)
{
	m_terrainMesh.generateBuffers();
	m_terrainMesh.bindVertices(terrain->getVertices());
	m_terrainMesh.bindIndices(terrain->getIndices());
	m_terrainMesh.bindNormals(terrain->getNormals());
	m_terrainMesh.bindTexCoords(terrain->getTextureCoordinates());

	m_meshGenerated = true;
}