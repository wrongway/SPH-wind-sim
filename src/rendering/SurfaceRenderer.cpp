#include "SurfaceRenderer.h"

#include "simulation/kernels/DensityKernel2D.h"
#include "simulation/kernels/DensityKernel3D.h"

#include "terrain/densityFunction/DensityFunctionSinCos.h"

SurfaceRenderer::SurfaceRenderer()
	: m_densityFunction()
	, m_domainSize(1.0f)
	, m_cellSize(0.1f)
	, m_lightPosition(0.0f, 1.0f, 0.0f)
	, m_terrain(NULL)
	, m_hasMeshToRender(false)
{
	m_densityFunction.setDensityKernel(std::make_shared<DensityKernel2D>());
}

SurfaceRenderer::~SurfaceRenderer()
{

}

void SurfaceRenderer::update(const ParticleContainer& particles, const float deltaTime)
{
	calculateSurface(const_cast<ParticleContainer&>(particles));
}

void SurfaceRenderer::render(const ParticleContainer& particles, const ci::Camera& camera)
{
	if (m_hasMeshToRender == false)
	{
		return;
	}

	ci::Matrix44f mat_modelview = camera.getModelViewMatrix();
	ci::Matrix44f mat_model_view_proj = camera.getProjectionMatrix() * mat_modelview;

	ci::gl::pushMatrices();
		ci::gl::enableDepthWrite();
		ci::gl::enableDepthRead();

		m_phongShader.bind();
			m_phongShader.setUniform("modelviewMatrix", mat_modelview, false);
			m_phongShader.setUniform("modelviewProjectionMatrix", mat_model_view_proj, false);
			m_phongShader.setUniform("vCameraPos", camera.getEyePoint());

			m_phongShader.setUniform("fLightAmbient", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fLightDiffuse", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fLightSpecular", ci::Vec4f(0.8f, 0.9f, 1.0f, 1.0f));
			m_phongShader.setUniform("fShinyness", 20.0f);

			m_phongShader.setUniform("fMaterialSpecular", ci::Vec4f(0.1f, 0.3f, 0.12f, 1.0f));
			m_phongShader.setUniform("vLightPosition", m_lightPosition);

			m_meshBufferObject.bindVertexArray();
			m_meshBufferObject.draw();
		m_phongShader.unbind();

	ci::gl::popMatrices();
}

std::string SurfaceRenderer::getName() const
{
	return "SurfaceRenderer";
}

void SurfaceRenderer::move(const ci::Vec3f& displacement)
{

}

void SurfaceRenderer::loadShader(const std::string& vertexPath, const std::string& fragmentPath)
{
	m_phongShader = Shader();

	m_phongShader.load(vertexPath, fragmentPath);
	m_phongShader.bindAttribute(0, "vPosition");
	m_phongShader.bindAttribute(1, "vNormal");
	m_phongShader.bindAttribute(2, "vTexcoord");
	m_phongShader.linkShaders();
}

void SurfaceRenderer::setDomainSize(const float domainSize)
{
	m_domainSize = domainSize;
}

void SurfaceRenderer::setCellSize(const float cellSize)
{
	m_cellSize = cellSize;
}

void SurfaceRenderer::setKernelSupportRadius(const float supportRadius)
{
	m_densityFunction.setKernelSupportRadius(supportRadius);
}

void SurfaceRenderer::setRestDensity(const float restDensity)
{
	m_densityFunction.setRestDensity(restDensity);
}

void SurfaceRenderer::calculateSurface(ParticleContainer& particles)
{
	if (m_terrain != NULL)
	{
		delete m_terrain;
	}

	m_densityFunction.setParticles(&particles.getParticles());

	TerrainGenerator generator;
	m_terrain = generator.generateTerrain(m_densityFunction, 1.0f, 0.1f);

	if (m_terrain->getVertices().size() > 0)
	{
		m_hasMeshToRender = true;

		m_meshBufferObject = MeshBufferObject();
		m_meshBufferObject.generateBuffers();
		m_meshBufferObject.bindVertices(m_terrain->getVertices());
		m_meshBufferObject.bindIndices(m_terrain->getIndices());
		m_meshBufferObject.bindNormals(m_terrain->getNormals());
		m_meshBufferObject.bindTexCoords(m_terrain->getTextureCoordinates());
	}
	else
	{
		m_hasMeshToRender = false;
	}
}