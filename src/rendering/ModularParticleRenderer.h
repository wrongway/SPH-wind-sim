#ifndef MODULAR_PARTICLE_RENDERER_H
#define MODULAR_PARTICLE_RENDERER_H

#include "IRenderer.h"

class ModularParticleRenderer : public IRenderer
{
public:
	ModularParticleRenderer();
	virtual ~ModularParticleRenderer();

	virtual void update(const ParticleContainer& particles, const float deltaTime);
	virtual void render(const ParticleContainer& particles, const ci::Camera& camera);

	virtual std::string getName() const;

	virtual void move(const ci::Vec3f& displacement);

private:
	void renderSPHParticles(const ParticleContainer& particles);
	void renderInflowParticles(const ParticleContainer& particles);
	void renderBoundaryParticles(const ParticleContainer& particles);
};

#endif // MODULAR_PARTICLE_RENDERER_H