#include "particle.h"

#include "core/defines.h"

#include "simulation/ids/ParticleIdManager.h"

Particle::Particle()
	: m_id(0)
	, m_is3D(true)
	, m_isGhost(false)
	, m_position(0.0f, 0.0f, 0.0f)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_force(0.0f, 0.0f, 0.0f)
	, m_density(0.0f)
	, m_radius(0.0f)
	, m_mass(0.0f)
	, m_viscosity(0.0f)
	, m_massModifier(1.0f)
	, m_targetRelativeAltitude(0.0f)
	, m_simulationFactor(0.0f)
	, m_evolution(0)
	, m_neighbours()
{

}

Particle::Particle(const id id)
	: m_id(id)
	, m_is3D(true)
	, m_isGhost(false)
	, m_position(0.0f, 0.0f, 0.0f)
	, m_velocity(0.0f, 0.0f, 0.0f)
	, m_force(0.0f, 0.0f, 0.0f)
	, m_density(0.0f)
	, m_radius(0.0f)
	, m_mass(0.0f)
	, m_viscosity(0.0f)
	, m_massModifier(1.0f)
	, m_targetRelativeAltitude(0.0f)
	, m_simulationFactor(0.0f)
	, m_evolution(0)
	, m_neighbours()
{

}

Particle::~Particle()
{

}

Particle Particle::createParticle(const ci::Vec3f& position, const float density, const float radius, const float massModifier, const float simulationFactor, const float viscosity)
{
	Particle particle(ParticleIdManager::getInstance()->getNextId());

	particle.setPosition(position);
	particle.setDensity(density);
	particle.setRadius(radius);
	particle.setMassModifier(massModifier);
	particle.setSimulationFactor(simulationFactor);
	particle.setViscosity(viscosity);

	return particle;
}

id Particle::getId() const
{
	return m_id;
}

void Particle::setIs3D(const bool is3D)
{
	m_is3D = is3D;
	updateMass();
}

bool Particle::getIs3D() const
{
	return m_is3D;
}

void Particle::setIsGhost(const bool isGhost)
{
	m_isGhost = isGhost;
}

bool Particle::getIsGhost() const
{
	return m_isGhost;
}

void Particle::setPosition(const ci::Vec3f& position)
{
	m_position = position;
}

ci::Vec3f Particle::getPosition() const
{
	return m_position;
}

ci::Vec3f Particle::getInterpolationPoint() const
{
	if (m_isGhost)
	{
		return ci::Vec3f(m_position.x, m_position.y + (-2.0f * m_targetRelativeAltitude), m_position.z);
	}
	else
	{
		return m_position;
	}
}

void Particle::setVelocity(const ci::Vec3f& velocity)
{
	m_velocity = velocity;
}

ci::Vec3f Particle::getVelocity() const
{
	return m_velocity;
}

void Particle::setForce(const ci::Vec3f& force)
{
	m_force = force;
}

ci::Vec3f Particle::getForce() const
{
	return m_force;
}

void Particle::setDensity(const float density)
{
	m_density = density;

	updateMass();
}

float Particle::getDensity() const
{
	return m_density;
}

void Particle::setRadius(const float radius)
{
	m_radius = radius;

	updateMass();
}

float Particle::getRadius() const
{
	return m_radius;
}

float Particle::getDiameter() const
{
	return 2.0f * m_radius;
}

float Particle::getDiameterSquared() const
{
	return (2.0f * m_radius) * (2.0f * m_radius);
}

float Particle::getMass() const
{
	return m_mass * m_massModifier;
}

void Particle::setViscosity(const float viscosity)
{
	m_viscosity = viscosity;
}

float Particle::getViscosity() const
{
	return m_viscosity;
}

void Particle::setMassModifier(const float massModifier)
{
	m_massModifier = massModifier;
}

float Particle::getMassModifier() const
{
	return m_massModifier;
}

void Particle::setTargetRelativeAltitude(const float relativeAltitude)
{
	m_targetRelativeAltitude = relativeAltitude;
}

float Particle::getTargetRelativeAltitude() const
{
	return m_targetRelativeAltitude;
}

void Particle::setSPHDensity(const float sphDensity)
{
	m_sphDensity = sphDensity;
}

float Particle::getSPHDensity() const
{
	return m_sphDensity;
}

void Particle::setSimulationFactor(const float factor)
{
	m_simulationFactor = factor;
}

float Particle::getSimulationFactor() const
{
	return m_simulationFactor;
}

float Particle::getVolume() const
{
	float volume = 0.0f;

	if (m_is3D)
	{
		volume = (4.0f / 3.0f) * m_radius * m_radius * m_radius * 3.141592654f;
	}
	else
	{
		volume = m_radius * m_radius * 3.141592654f;
	}

	return volume;
}

void Particle::increaseEvolution()
{
	m_evolution++;
}

void Particle::resetEvolution()
{
	m_evolution = 0;
}

unsigned int Particle::getEvolution() const
{
	return m_evolution;
}

std::vector<unsigned int> Particle::getNeighbours() const
{
	return m_neighbours;
}

void Particle::pushNeighbour(const unsigned int neighbour)
{
	m_neighbours.push_back(neighbour);
}

void Particle::clearNeighbours()
{
	m_neighbours.clear();
}

void Particle::updateMass()
{
	m_mass = getVolume() * m_density;
}