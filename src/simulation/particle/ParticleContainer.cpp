#include "ParticleContainer.h"

#include "core/utility/logging/logging.h"

ParticleContainer::ParticleContainer()
	: m_particles()
	, m_boundingBoxMinCorner(0.0f, 0.0f, 0.0f)
	, m_boundingBoxMaxCorner(0.0f, 0.0f, 0.0f)
	, m_normParticlesPerDimension(0)
	, m_normMinCorner(0.0f, 0.0f, 0.0f)
	, m_normMaxCorner(0.0f, 0.0f, 0.0f)
	, m_minDensity(0.0f)
	, m_maxDensity(0.0f)
	, m_averageDensity(0.0f)
	, m_standardDeviationDensity(0.0f)
	, m_normSPHDomainMinCorner(0.0f, 0.0f, 0.0f)
	, m_normSPHDomainMaxCorner(0.0f, 0.0f, 0.0f)
{

}

ParticleContainer::~ParticleContainer()
{

}

void ParticleContainer::push_back(const Particle& particle)
{
	m_particles.push_back(particle);
}

Particle& ParticleContainer::getParticle(const unsigned int idx)
{
	if (idx >= m_particles.size())
	{
		LOG_ERROR_STREAM(<< "Index out of range, index is " << idx << ", maximum is: " << m_particles.size());

		return Particle();
	}

	return m_particles[idx];
}

Particle ParticleContainer::getParticle(const unsigned int idx) const
{
	if (idx >= m_particles.size())
	{
		LOG_ERROR_STREAM(<< "Index out of range, index is " << idx << ", maximum is: " << m_particles.size());

		return Particle();
	}

	return m_particles[idx];
}

Particle& ParticleContainer::operator[](const unsigned int idx)
{
	return getParticle(idx);
}

Particle ParticleContainer::operator[](const unsigned int idx) const
{
	return getParticle(idx);
}

unsigned int ParticleContainer::size() const
{
	return m_particles.size();
}

void ParticleContainer::clear()
{
	m_particles.clear();
}

void ParticleContainer::setBoundingBoxMinCorner(const ci::Vec3f& corner)
{
	m_boundingBoxMinCorner = corner;
}

ci::Vec3f ParticleContainer::getBoundingBoxMinCorner() const
{
	return m_boundingBoxMinCorner;
}

void ParticleContainer::setBoundingBoxMaxCorner(const ci::Vec3f& corner)
{
	m_boundingBoxMaxCorner = corner;
}

ci::Vec3f ParticleContainer::getBoundingBoxMaxCorner() const
{
	return m_boundingBoxMaxCorner;
}

void ParticleContainer::setNormParticlesPerDimension(const unsigned int particlesPerDimension)
{
	m_normParticlesPerDimension = particlesPerDimension;
}

unsigned int ParticleContainer::getNormParticlesPerDimension() const
{
	return m_normParticlesPerDimension;
}

void ParticleContainer::setSphBoundingBoxMinCorner(const ci::Vec3f& corner)
{
	m_sphBoundingBoxMinCorner = corner;
}

ci::Vec3f ParticleContainer::getSphBoundingBoxMinCorner() const
{
	return m_sphBoundingBoxMinCorner;
}

void ParticleContainer::setSphBoundingBoxMaxCorner(const ci::Vec3f& corner)
{
	m_sphBoundingBoxMaxCorner = corner;
}

ci::Vec3f ParticleContainer::getSphBoundingBoxMaxCorner() const
{
	return m_sphBoundingBoxMaxCorner;
}

void ParticleContainer::setNormMinCorner(const ci::Vec3f& corner)
{
	m_normMinCorner = corner;
}

ci::Vec3f ParticleContainer::getNormMinCorner() const
{
	return m_normMinCorner;
}

void ParticleContainer::setNormMaxCorner(const ci::Vec3f& corner)
{
	m_normMaxCorner = corner;
}

ci::Vec3f ParticleContainer::getNormMaxCorner() const
{
	return m_normMaxCorner;
}

void ParticleContainer::setMinDensity(const float density)
{
	m_minDensity = density;
}

float ParticleContainer::getMinDensity() const
{
	return m_minDensity;
}

void ParticleContainer::setMaxDensity(const float density)
{
	m_maxDensity = density;
}

float ParticleContainer::getMaxDensity() const
{
	return m_maxDensity;
}

void ParticleContainer::setNormSPHDomainMaxCorner(const ci::Vec3f& corner)
{
	m_normSPHDomainMaxCorner = corner;
}

ci::Vec3f ParticleContainer::getNormSPHDomainMaxCorner() const
{
	return m_normSPHDomainMaxCorner;
}

void ParticleContainer::setNormSPHDomainMinCorner(const ci::Vec3f& corner)
{
	m_normSPHDomainMinCorner = corner;
}

ci::Vec3f ParticleContainer::getNormSPHDomainMinCorner() const
{
	return m_normSPHDomainMinCorner;
}

void ParticleContainer::setAverageDensity(const float density)
{
	m_averageDensity = density;
}

float ParticleContainer::getAverageDensity() const
{
	return m_averageDensity;
}

void ParticleContainer::setStandardDeviationDensity(const float density)
{
	m_standardDeviationDensity = density;
}

float ParticleContainer::getStandardDeviationDensity() const
{
	return m_standardDeviationDensity;
}

std::vector<Particle> ParticleContainer::getParticles()
{
	return m_particles;
}