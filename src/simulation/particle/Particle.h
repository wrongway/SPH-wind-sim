#ifndef PARTICLE_H
#define PARTICLE_H

#include "cinder/Vector.h"

#include "core/Defines.h"

class Particle
{
public:
	Particle();
	Particle(const id id);
	~Particle();

	static Particle createParticle(const ci::Vec3f& position, const float density, const float radius, const float massModifier, const float simulationFactor, const float viscosity);

	id getId() const;

	void setIs3D(const bool is3D);
	bool getIs3D() const;

	void setIsGhost(const bool isGhost);
	bool getIsGhost() const;

	void setPosition(const ci::Vec3f& position);
	ci::Vec3f getPosition() const;

	ci::Vec3f getInterpolationPoint() const;

	void setVelocity(const ci::Vec3f& velocity);
	ci::Vec3f getVelocity() const;

	void setForce(const ci::Vec3f& force);
	ci::Vec3f getForce() const;

	void setDensity(const float density);
	float getDensity() const;

	void setRadius(const float radius);
	float getRadius() const;

	float getDiameter() const;
	float getDiameterSquared() const;

	float getMass() const;

	void setViscosity(const float viscosity);
	float getViscosity() const;

	void setMassModifier(const float massModifier);
	float getMassModifier() const;

	void setTargetRelativeAltitude(const float relativeAltitude);
	float getTargetRelativeAltitude() const;

	void setSPHDensity(const float sphDensity);
	float getSPHDensity() const;

	void setSimulationFactor(const float factor);
	float getSimulationFactor() const;

	float getVolume() const;

	void increaseEvolution();
	void resetEvolution();
	unsigned int getEvolution() const;

	std::vector<unsigned int> getNeighbours() const;
	void pushNeighbour(const unsigned int neighbour);
	void clearNeighbours();

private:
	void updateMass();

	const id m_id;

	bool m_is3D;
	bool m_isGhost;

	ci::Vec3f m_position;
	ci::Vec3f m_velocity;
	ci::Vec3f m_force;

	float m_density;
	float m_radius;
	float m_mass;
	float m_viscosity;
	float m_massModifier;
	float m_targetRelativeAltitude;
	float m_sphDensity;

	float m_simulationFactor;

	unsigned int m_evolution;

	std::vector<unsigned int> m_neighbours;
};

#endif // PARTICLE_H