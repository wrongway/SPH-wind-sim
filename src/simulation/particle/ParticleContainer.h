#ifndef PARTICLE_CONTAINER_H
#define PARTICLE_CONTAINER_H

#include "cinder/Vector.h"

#include "Particle.h"

class ParticleContainer
{
public:
	ParticleContainer();
	~ParticleContainer();

	void push_back(const Particle& particle);
	Particle& getParticle(const unsigned int idx);
	Particle getParticle(const unsigned int idx) const;
	Particle& operator[](const unsigned int idx);
	Particle operator[](const unsigned int idx) const;

	unsigned int size() const;

	void clear();

	void setBoundingBoxMinCorner(const ci::Vec3f& corner);
	ci::Vec3f getBoundingBoxMinCorner() const;

	void setBoundingBoxMaxCorner(const ci::Vec3f& corner);
	ci::Vec3f getBoundingBoxMaxCorner() const;

	void setNormParticlesPerDimension(const unsigned int particlesPerDimension);
	unsigned int getNormParticlesPerDimension() const;

	void setSphBoundingBoxMinCorner(const ci::Vec3f& corner);
	ci::Vec3f getSphBoundingBoxMinCorner() const;

	void setSphBoundingBoxMaxCorner(const ci::Vec3f& corner);
	ci::Vec3f getSphBoundingBoxMaxCorner() const;

	void setNormMinCorner(const ci::Vec3f& corner);
	ci::Vec3f getNormMinCorner() const;

	void setNormMaxCorner(const ci::Vec3f& corner);
	ci::Vec3f getNormMaxCorner() const;

	void setMinDensity(const float density);
	float getMinDensity() const;

	void setMaxDensity(const float density);
	float getMaxDensity() const;

	void setNormSPHDomainMaxCorner(const ci::Vec3f& corner);
	ci::Vec3f getNormSPHDomainMaxCorner() const;

	void setNormSPHDomainMinCorner(const ci::Vec3f& corner);
	ci::Vec3f getNormSPHDomainMinCorner() const;

	void setAverageDensity(const float density);
	float getAverageDensity() const;

	void setStandardDeviationDensity(const float density);
	float getStandardDeviationDensity() const;

	std::vector<Particle> getParticles();

private:
	std::vector<Particle> m_particles;

	ci::Vec3f m_boundingBoxMinCorner;
	ci::Vec3f m_boundingBoxMaxCorner;

	ci::Vec3f m_sphBoundingBoxMinCorner;
	ci::Vec3f m_sphBoundingBoxMaxCorner;

	unsigned int m_normParticlesPerDimension;

	ci::Vec3f m_normMinCorner;
	ci::Vec3f m_normMaxCorner;

	ci::Vec3f m_normSPHDomainMinCorner;
	ci::Vec3f m_normSPHDomainMaxCorner;

	float m_minDensity;
	float m_maxDensity;
	float m_averageDensity;
	float m_standardDeviationDensity;
};

#endif // ParticleContainer