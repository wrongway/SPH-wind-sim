#ifndef VISCOSITY_KERNEL_3D_H
#define VISCOSITY_KERNEL_3D_H

#include "IKernelFunction.h"

class ViscosityKernel3D : public IKernelFunction
{
public:
	ViscosityKernel3D();
	~ViscosityKernel3D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // VISCOSITY_KERNEL_3D_H