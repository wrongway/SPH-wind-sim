#ifndef DENSITY_KERNEL_2_D_FOR_SQUARED_DISTANCE_H
#define DENSITY_KERNEL_2_D_FOR_SQUARED_DISTANCE_H

#include "IKernelFunction.h"

class DensityKernel2DForSquaredDistance : public IKernelFunction
{
public:
	DensityKernel2DForSquaredDistance();
	virtual ~DensityKernel2DForSquaredDistance();

	virtual float kernel(const float squaredDistance, const float supportRadius) const;
};

#endif // DENSITY_KERNEL_2_D_FOR_SQUARED_DISTANCE_H