#include "DensityKernel3D.h"

#include <cmath>

#include "core/Defines.h"

DensityKernel3D::DensityKernel3D()
{

}

DensityKernel3D::~DensityKernel3D()
{

}

float DensityKernel3D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 315.0f / (64.0f * 3.141592654f * std::pow(supportRadius, 9.0f));
	result *= std::pow((h*h - d*d), 3.0f);

	return result;
}