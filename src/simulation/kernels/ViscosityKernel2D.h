#ifndef VISCOSITY_KERNEL_2D_H
#define VISCOSITY_KERNEL_2D_H

#include "IKernelFunction.h"

class ViscosityKernel2D : public IKernelFunction
{
public:
	ViscosityKernel2D();
	~ViscosityKernel2D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // VISCOSITY_KERNEL_2D_H