#include "DensityKernel2DForSquaredDistance.h"

#include <cmath>

#include "core/Defines.h"

DensityKernel2DForSquaredDistance::DensityKernel2DForSquaredDistance()
{

}

DensityKernel2DForSquaredDistance::~DensityKernel2DForSquaredDistance()
{
	
}

float DensityKernel2DForSquaredDistance::kernel(const float squaredDistance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = squaredDistance;
	result = 4.0f / (3.141592654f * std::pow(supportRadius, 8.0f));
	result *= std::pow((h*h - d), 3.0f);

	return result;
}