#ifndef PRESSURE_KERNEL_2D_H
#define PRESSURE_KERNEL_2D_H

#include "IKernelFunction.h"

class PressureKernel2D : public IKernelFunction
{
public:
	PressureKernel2D();
	~PressureKernel2D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // PRESSURE_KERNEL_2D_H