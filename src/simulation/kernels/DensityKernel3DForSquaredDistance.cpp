#include "DensityKernel3DForSquaredDistance.h"

#include <cmath>

#include "core/Defines.h"

DensityKernel3DForSquaredDistance::DensityKernel3DForSquaredDistance()
{

}

DensityKernel3DForSquaredDistance::~DensityKernel3DForSquaredDistance()
{

}

float DensityKernel3DForSquaredDistance::kernel(const float squaredDistance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = squaredDistance;
	result = 315.0f / (64.0f * 3.141592654f * std::pow(supportRadius, 9.0f));
	result *= std::pow((h*h - d), 3.0f);

	return result;
}