#ifndef PRESSURE_KERNEL_3D_H
#define PRESSURE_KERNEL_3D_H

#include "IKernelFunction.h"

class PressureKernel3D : public IKernelFunction
{
public:
	PressureKernel3D();
	~PressureKernel3D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // PRESSURE_KERNEL_3D_H