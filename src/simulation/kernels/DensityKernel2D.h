#ifndef DENSITY_KERNEL_2_D_H
#define DENSITY_KERNEL_2_D_H

#include "IKernelFunction.h"

class DensityKernel2D : public IKernelFunction
{
public:
	DensityKernel2D();
	virtual ~DensityKernel2D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // DENSITY_KERNEL_2_D_H