#ifndef DENSITY_KERNEL_3D_H
#define DENSITY_KERNEL_3D_H

#include "IKernelFunction.h"

class DensityKernel3D : public IKernelFunction
{
public:
	DensityKernel3D();
	~DensityKernel3D();

	virtual float kernel(const float distance, const float supportRadius) const;
};

#endif // DENSITY_KERNEL_3D_H