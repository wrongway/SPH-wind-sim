#include "ViscosityKernel3D.h"

#include <cmath>

#include "core/Defines.h"

ViscosityKernel3D::ViscosityKernel3D()
{

}

ViscosityKernel3D::~ViscosityKernel3D()
{

}

float ViscosityKernel3D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 45.0f / (3.141592654f * std::pow(supportRadius, 6.0f)); // 20.0f / (3.141592654f * std::pow(supportRadius, 5.0f));
	result *= (h - d);

	return result;
}