#include "PressureKernel3D.h"

#include <cmath>

#include "core/Defines.h"

PressureKernel3D::PressureKernel3D()
{

}

PressureKernel3D::~PressureKernel3D()
{

}

float PressureKernel3D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 15.0f / (3.141592654f * std::pow(supportRadius, 6.0f)); //  (45.0f * std::pow(supportRadius, 6.0f)) / (3.141592654f * distance);
	result *= std::pow((h - d), 3.0f);

	return result;
}