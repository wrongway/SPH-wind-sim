#include "PressureKernel2D.h"

#include <cmath>

#include "core/Defines.h"

PressureKernel2D::PressureKernel2D()
{

}

PressureKernel2D::~PressureKernel2D()
{

}

float PressureKernel2D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 30.0f / (3.141592654f * std::pow(h, 5.0f));
	result *= std::pow((h - d), 2.0f);

	return result;
}