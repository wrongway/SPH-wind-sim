#ifndef I_KERNEL_FUNCTION_H
#define I_KERNEL_FUNCTION_H

class IKernelFunction
{
public:
	IKernelFunction() {}
	virtual ~IKernelFunction() {}

	virtual float kernel(const float distance, const float supportRadius) const = 0;
};

#endif // I_KERNEL_FUNCTION_H