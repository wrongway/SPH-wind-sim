#include "DensityKernel2D.h"

#include <cmath>

#include "core/Defines.h"

DensityKernel2D::DensityKernel2D()
{

}

DensityKernel2D::~DensityKernel2D()
{

}

float DensityKernel2D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 4.0f / (3.141592654f * std::pow(supportRadius, 8.0f));
	result *= std::pow((h*h - d*d), 3.0f);

	return result;
}