#ifndef DENSITY_KERNEL_3_D_FOR_SQUARED_DISTANCE_H
#define DENSITY_KERNEL_3_D_FOR_SQUARED_DISTANCE_H

#include "IKernelFunction.h"

class DensityKernel3DForSquaredDistance : public IKernelFunction
{
public:
	DensityKernel3DForSquaredDistance();
	virtual ~DensityKernel3DForSquaredDistance();

	virtual float kernel(const float squaredDistance, const float supportRadius) const;
};

#endif // DENSITY_KERNEL_2_D_FOR_SQUARED_DISTANCE_H