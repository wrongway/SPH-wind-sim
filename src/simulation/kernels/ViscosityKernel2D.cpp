#include "ViscosityKernel2D.h"

#include <cmath>

#include "core/Defines.h"

ViscosityKernel2D::ViscosityKernel2D()
{

}

ViscosityKernel2D::~ViscosityKernel2D()
{

}

float ViscosityKernel2D::kernel(const float distance, const float supportRadius) const
{
	float result = 0.0f;

	float h = supportRadius;
	float d = distance;
	result = 20.0f / (3.141592654f * std::pow(supportRadius, 5.0f));
	result *= (h - d);

	return result;
}