#include "OperationCalculateForces.h"

#include <algorithm>
#include <thread>

#include "core/Defines.h"
#include "core/utility/logging/logging.h"
#include "core/utility/profiling/profiling.h"
#include "core/utility/profiling/StopWatch.h"
#include "core/math/VectorUtility.h"

OperationCalculateForces::OperationCalculateForces()
	: m_is3D(true)
	, m_supportRadius(0.0f)
	, m_supportRadiusSquared(0.0f)
	, m_stiffness(0.0f)
	, m_distanceTolerance(0.05f)
	, m_domainSideLength(1.0f)
	, m_neighbours()
	, m_densities()
	, m_forces()
	, m_densityKernel(NULL)
	, m_pressureKernel(NULL)
	, m_viscosityKernel(NULL)
	, m_densityKernelNear(NULL)
	, m_pressureKernelNear(NULL)
	, m_viscosityKernelNear(NULL)
	, m_domainCenter(0.0f, 0.0f, 0.0f)
{

}

OperationCalculateForces::~OperationCalculateForces()
{

}

void OperationCalculateForces::operate(ParticleContainer& particles, const float deltaTime)
{
	clearVectors();

	unsigned int threadCount = 4;
	std::vector<std::thread> threads;
	unsigned int particlesPerThread = particles.size() / threadCount;
	particlesPerThread++;

	findNeighbours(particles, std::pair<unsigned int, unsigned int>(0, particles.size() - 1));

	/*for (unsigned int i = 0; i < particles.size(); i++)
	{
		particles[i].clearNeighbours();

		for (unsigned int j = 0; j < m_neighbours[i].size(); j++)
		{
			particles[i].pushNeighbour(m_neighbours[i][j]);
		}
	}*/

	initForces(particles);

	calculateDensitySelfInfluences(particles, std::pair<unsigned int, unsigned int>(0, particles.size() - 1));

	StopWatch watch;

	watch.start();
	/*for (unsigned int i = 0; i < threadCount; i++)
	{
		unsigned int start = i*particlesPerThread;
		unsigned int end = (i + 1)*particlesPerThread - 1;
		if (i == (threadCount - 1))
		{
			end = particles.size() - 1;
		}
		threads.push_back(std::thread(&OperationCalculateForces::updateDensities, this, particles, std::pair<unsigned int, unsigned int>(start, end)));
	}
	for (unsigned int i = 0; i < threadCount; i++)
	{
		threads[i].join();
	}
	threads.clear();*/

	updateDensities(particles, std::pair<unsigned int, unsigned int>(0, particles.size()-1));

	watch.stop();
	//LOG_WARNING_STREAM(<< "time: " << watch.getTime());

	watch.start();
	/*for (unsigned int i = 0; i < threadCount; i++)
	{
		unsigned int start = i*particlesPerThread;
		unsigned int end = (i + 1)*particlesPerThread - 1;
		if (i == (threadCount - 1))
		{
			end = particles.size() - 1;
		}
		threads.push_back(std::thread(&OperationCalculateForces::updatePressureForces, this, particles, std::pair<unsigned int, unsigned int>(start, end)));
	}
	for (unsigned int i = 0; i < threadCount; i++)
	{
		threads[i].join();
	}
	threads.clear();*/

	updatePressureForces(particles, std::pair<unsigned int, unsigned int>(0, particles.size()-1));

	watch.stop();
	//LOG_WARNING_STREAM(<< "time: " << watch.getTime());

	watch.start();
	/*for (unsigned int i = 0; i < threadCount; i++)
	{
		unsigned int start = i*particlesPerThread;
		unsigned int end = (i + 1)*particlesPerThread - 1;
		if (i == (threadCount - 1))
		{
			end = particles.size() - 1;
		}
		threads.push_back(std::thread(&OperationCalculateForces::updateViscousForces, this, particles, std::pair<unsigned int, unsigned int>(start, end)));
	}
	for (unsigned int i = 0; i < threadCount; i++)
	{
		threads[i].join();
	}
	threads.clear();*/

	updateViscousForces(particles, std::pair<unsigned int, unsigned int>(0, particles.size()-1));

	watch.stop();
	//LOG_WARNING_STREAM(<< "time: " << watch.getTime());

	applyForces(particles, deltaTime);
}

std::string OperationCalculateForces::getName() const
{
	return "OperationCalculateForces";
}

void OperationCalculateForces::setIs3D(bool is3D)
{
	m_is3D = is3D;
}

bool OperationCalculateForces::getIs3D() const
{
	return m_is3D;
}

void OperationCalculateForces::setSupportRadius(const float supportRadius)
{
	m_supportRadius = supportRadius;
	m_supportRadiusSquared = supportRadius * supportRadius;
}

float OperationCalculateForces::getSupportRadius() const
{
	return m_supportRadius;
}

void OperationCalculateForces::setStiffness(const float stiffness)
{
	m_stiffness = stiffness;
}

float OperationCalculateForces::getStiffness() const
{
	return m_stiffness;
}

void OperationCalculateForces::setDomainSideLength(const float domainSideLength)
{
	m_domainSideLength = domainSideLength;
}

float OperationCalculateForces::getDomainSideLength() const
{
	return m_domainSideLength;
}

void OperationCalculateForces::setDomainCenter(const ci::Vec3f& domainCenter)
{
	m_domainCenter = domainCenter;
}

ci::Vec3f OperationCalculateForces::getDomainCenter() const
{
	return m_domainCenter;
}

void OperationCalculateForces::setDensityKernel(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_densityKernel = kernel;
}

void OperationCalculateForces::setPressureKernel(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_pressureKernel = kernel;
}

void OperationCalculateForces::setViscosityKernel(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_viscosityKernel = kernel;
}

void OperationCalculateForces::setDensityKernelNear(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_densityKernelNear = kernel;
}

void OperationCalculateForces::setPressureKernelNear(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_pressureKernelNear = kernel;
}

void OperationCalculateForces::setViscosityKernelNear(const std::shared_ptr<IKernelFunction>& kernel)
{
	m_viscosityKernelNear = kernel;
}

void OperationCalculateForces::clearVectors()
{
	m_neighbours.clear();
	m_densities.clear();
	m_forces.clear();
}

void OperationCalculateForces::findNeighbours(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range)
{
	for (unsigned int i = range.first; i <= range.second; i++)
	{
		m_neighbours.push_back(std::vector<unsigned int>());
	}

	for(unsigned int i = range.first; i <= range.second; i++)
	{
		for (unsigned int j = i+1; j < particles.size(); j++)
		{
			if ((particles[i].getInterpolationPoint() - particles[j].getInterpolationPoint()).lengthSquared() <= m_supportRadiusSquared)
			{
				m_neighbours[i].push_back(j);
				m_neighbours[j].push_back(i);
			}
		}
	}
}

void OperationCalculateForces::updateDensities(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range)
{
	if (m_densityKernel == NULL)
	{
		LOG_ERROR_STREAM(<< "Density kernel is not set.");
	}

	for(int i = range.first; i <= range.second; i++)
	{
		unsigned int particleAIdx = i;

		for(int j = 0; j < m_neighbours[i].size(); j++)
		{
			unsigned int particleBIdx = m_neighbours[i][j];

			float distanceSquared = (particles[particleAIdx].getInterpolationPoint() - particles[particleBIdx].getInterpolationPoint()).lengthSquared();

			if (distanceSquared < m_supportRadiusSquared)
			{
				float kernel = m_densityKernel->kernel(distanceSquared, m_supportRadius);

				if (m_densityKernelNear != NULL && distanceSquared < particles[particleAIdx].getDiameterSquared())
				{
					float diameter = particles[particleAIdx].getDiameter();
					kernel += m_densityKernelNear->kernel(distanceSquared, diameter); // * diameter;
				}

				m_densities[particleAIdx] += kernel * particles[particleBIdx].getMass();
				m_densities[particleBIdx] += kernel * particles[particleAIdx].getMass();
			}
		}
	}
}

void OperationCalculateForces::updatePressureForces(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range)
{
	if (m_pressureKernel == NULL)
	{
		LOG_ERROR_STREAM(<< "Pressure kernel is not set.");
	}

	float y = 7.0f;

	for(int i = range.first; i <= range.second; i++)
	{
		unsigned int particleAIdx = i;

		for(int j = 0; j < m_neighbours[i].size(); j++)
		{
			unsigned int particleBIdx = m_neighbours[i][j];

			ci::Vec3f seperation = (particles[particleAIdx].getInterpolationPoint() - particles[particleBIdx].getInterpolationPoint());
			float distanceSquared = seperation.lengthSquared();

			if (distanceSquared < m_supportRadiusSquared)
			{
				float distance = seperation.length();
				float kernel = m_pressureKernel->kernel(distance, m_supportRadius);

				if (m_pressureKernelNear != NULL && distance < particles[particleAIdx].getDiameter())
				{
					float diameter = particles[particleAIdx].getDiameter();
					kernel += m_pressureKernelNear->kernel(distance, diameter); // *diameter; // *diameter * diameter;
				}

				float massA = particles[particleAIdx].getMass();
				float massB = particles[particleBIdx].getMass();

				float densityA = m_densities[particleAIdx];
				float densityB = m_densities[particleBIdx];

				
				float particleARestDensity = particles[particleAIdx].getDensity();
				float particleBRestDensity = particles[particleBIdx].getDensity();
				float pressureA = (m_stiffness * particleARestDensity / y) * (std::pow(densityA / particleARestDensity, y) - 1.0f);
				float pressureB = (m_stiffness * particleBRestDensity / y) * (std::pow(densityB / particleBRestDensity, y) - 1.0f);

				densityA /= massA;
				densityB /= massB;

				float pressureForce = ((pressureA / (densityA * densityA)) + (pressureB / (densityB * densityB))) * kernel;

				ci::Vec3f dir = seperation.normalized();

				m_forces[particleAIdx] += (dir * pressureForce);
				m_forces[particleBIdx] -= (dir * pressureForce);
			}
		}
	}
}

void OperationCalculateForces::updateViscousForces(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range)
{
	if (m_viscosityKernel == NULL)
	{
		LOG_ERROR_STREAM(<< "Viscosity kernel is not set.");
	}

	for(int i = range.first; i <= range.second; i++)
	{
		unsigned int particleAIdx = i;

		for(int j = 0; j < m_neighbours[i].size(); j++)
		{
			unsigned int particleBIdx = m_neighbours[i][j];

			ci::Vec3f seperation = (particles[particleAIdx].getInterpolationPoint() - particles[particleBIdx].getInterpolationPoint());
			float distanceSquared = seperation.lengthSquared();

			float viscosityA = particles[particleAIdx].getViscosity();
			float viscosityB = particles[particleBIdx].getViscosity();

			if (viscosityA + viscosityB <= 0.0f)
			{
				continue;
			}

			if (distanceSquared < m_supportRadiusSquared)
			{
				float massA = particles[particleAIdx].getMass();
				float massB = particles[particleBIdx].getMass();

				float densityA = m_densities[particleAIdx];
				float densityB = m_densities[particleBIdx];

				densityA /= massA;
				densityB /= massB;

				ci::Vec3f velocityA = particles[particleAIdx].getVelocity();
				ci::Vec3f velocityB = particles[particleBIdx].getVelocity();
				ci::Vec3f velDifference = velocityB - velocityA;

				float distance = seperation.length();
				float kernel = m_viscosityKernel->kernel(distance, m_supportRadius);

				float viscosity = (1.0f / densityA) * (1.0f / densityB) * ((viscosityA + viscosityB) / 2.0f) * kernel;

				m_forces[particleAIdx] += velDifference * viscosity;
			}
		}
	}
}

void OperationCalculateForces::calculateDensitySelfInfluences(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range)
{
	for(unsigned int i = range.first; i <= range.second; i++)
	{
		float kernel = m_densityKernel->kernel(0.0f, m_supportRadius);
		m_densities.push_back(kernel * particles[i].getMass());
	}
}

void OperationCalculateForces::initForces(const ParticleContainer& particles)
{
	for(unsigned int i = 0; i < particles.size(); i++)
	{
		m_forces.push_back(ci::Vec3f(0.0f, 0.0f, 0.0f));
	}
}

void OperationCalculateForces::applyForces(ParticleContainer& particles, const float deltaTime)
{
	float speedLimit = 2.0f * m_stiffness;
	float speedLimitSquared = speedLimit * speedLimit;

	for(unsigned int i = 0; i < particles.size(); i++)
	{
		ci::Vec3f velocity = particles[i].getVelocity();
		velocity += (m_forces[i] * deltaTime);
		if (velocity.lengthSquared() > speedLimitSquared)
		{
			velocity = velocity.normalized() * speedLimit;
		}

		particles[i].setVelocity(velocity);
		particles[i].setForce(m_forces[i]);
		particles[i].setSPHDensity(m_densities[i]);
	}
}