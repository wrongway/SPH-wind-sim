#include "OperationGhostParticles.h"

#include "core/utility/logging/logging.h"

OperationGhostParticles::OperationGhostParticles()
	: m_is3D(true)
	, m_initialized(false)
	, m_massModifier(1.0f)
	, m_restDensity(1.0f)
	, m_viscosity(0.0f)
	, m_sphCenter(0.0f, 0.0f, 0.0f)
	, m_minCorner(0.0f, 0.0f, 0.0f)
	, m_maxCorner(0.0f, 0.0f, 0.0f)
	, m_displacement(0.0f, 0.0f, 0.0f)
	, m_ghostIndices()
	, m_terrain(NULL)
{

}

OperationGhostParticles::~OperationGhostParticles()
{

}

void OperationGhostParticles::operate(ParticleContainer& particles, const float deltaTime)
{
	m_sphCenter += m_displacement;

	if (m_initialized == false)
	{
		init(particles, particles[0].getRadius());
		m_initialized = true;
	}

	if (m_terrain != NULL && m_ghostIndices.size() > 0 && m_displacement.lengthSquared() > 0.0f)
	{
		for (unsigned int i = 0; i < m_ghostIndices.size(); i++)
		{
			unsigned int idx = m_ghostIndices[i];
			ci::Vec3f position = particles[idx].getPosition();

			if (particles[idx].getIsGhost())
			{
				position += m_displacement;
				// particles[idx].setPosition(position);

				float altitude = 0.0f;

				if (m_terrain->getAltitudeAtPosition(position, altitude))
				{
					float newY = altitude + particles[idx].getTargetRelativeAltitude();
					position.y = newY;
				}

				particles[idx].setPosition(position);

				//else
				//{
				//	 LOG_ERROR_STREAM(<< "well fuck...: " << "[" << m_ghostIndices[i] << "(" << i << ")]");

				//	 m_terrain->getAltitudeAtPosition(position, altitude);
				//}
			}
		}
	}

	m_displacement = ci::Vec3f(0.0f, 0.0f, 0.0f);
}

void OperationGhostParticles::moveDomain(const ci::Vec3f& displacement)
{
	m_displacement += displacement;
}

std::string OperationGhostParticles::getName() const
{
	return "OperationGhostParticles";
}

bool OperationGhostParticles::getIs3D() const
{
	return m_is3D;
}

void OperationGhostParticles::setIs3D(const bool is3D)
{
	m_is3D = is3D;
}

float OperationGhostParticles::getMassModifier() const
{
	return m_massModifier;
}

void OperationGhostParticles::setMassModifier(const float massModifier)
{
	m_massModifier = massModifier;
}

float OperationGhostParticles::getRestDensity() const
{
	return m_restDensity;
}

void OperationGhostParticles::setRestDensity(const float restDensity)
{
	m_restDensity = restDensity;
}

float OperationGhostParticles::getVescosity() const
{
	return m_viscosity;
}

void OperationGhostParticles::setViscosity(const float viscosity)
{
	m_viscosity = viscosity;
}

ci::Vec3f OperationGhostParticles::getSPHCenter() const
{
	return m_sphCenter;
}

void OperationGhostParticles::setSPHCenter(const ci::Vec3f& sphCenter)
{
	m_sphCenter = sphCenter;
}

ci::Vec3f OperationGhostParticles::getMinCorner() const
{
	return m_minCorner;
}

void OperationGhostParticles::setMinCorner(const ci::Vec3f& minCorner)
{
	m_minCorner = minCorner;
}

ci::Vec3f OperationGhostParticles::getMaxCorner() const
{
	return m_maxCorner;
}

void OperationGhostParticles::setMaxCorner(const ci::Vec3f& maxCorner)
{
	m_maxCorner = maxCorner;
}

std::shared_ptr<IPolygonBody> OperationGhostParticles::getTerrain() const
{
	return m_terrain;
}

void OperationGhostParticles::setTerrain(const std::shared_ptr<IPolygonBody>& terrain)
{
	m_terrain = terrain;
}

void OperationGhostParticles::init(ParticleContainer& particles, const float particleRadius)
{
	float particleDiameter = particleRadius * 2.0f;

	ci::Vec3f size = m_maxCorner - m_minCorner;
	ci::Vec3f minCorner = m_maxCorner; // m_minCorner;

	unsigned int ppdX = size.x / particleDiameter;
	unsigned int ppdY = size.y / particleDiameter;
	unsigned int ppdZ = size.z / particleDiameter;
	if (m_is3D == false)
	{
		ppdZ = 1;
		minCorner.z = m_sphCenter.z;
	}

	for (unsigned int i = 0; i < ppdX; i++)
	{
		for (unsigned int j = 0; j < ppdZ; j++)
		{
			for (unsigned int k = 0; k < ppdY; k++)
			{
				float x = (i + 1) * particleDiameter; // +particleRadius;
				float y = k * particleDiameter; // +particleRadius;
				float z = minCorner.z - m_sphCenter.z;
				if (m_is3D)
				{
					z = j * particleDiameter + particleRadius + particleRadius;
				}

				Particle particle = Particle::createParticle(minCorner - ci::Vec3f(x, y, z), m_restDensity, particleRadius, m_massModifier, 0.0f, m_viscosity);
				particle.setIs3D(m_is3D);
				particle.setIsGhost(true);
				particle.setTargetRelativeAltitude(-(y + particle.getRadius()));

				if (m_terrain != NULL)
				{
					float altitude = 0.0f;
					ci::Vec3f position = particle.getPosition();
					if (m_terrain->getAltitudeAtPosition(position, altitude))
					{
						float newY = altitude + particle.getTargetRelativeAltitude();
						position.y = newY;
						particle.setPosition(position);
					}
				}

				particles.push_back(particle);

				m_ghostIndices.push_back(particles.size() - 1);
			}
		}
	}
}