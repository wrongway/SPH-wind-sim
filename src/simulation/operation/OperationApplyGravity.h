#ifndef OPERATION_APPLY_GRAVITY_H
#define OPERATION_APPLY_GRAVITY_H

#include "cinder/Vector.h"

#include "IOperation.h"

class OperationApplyGravity : public IOperation
{
public:
	OperationApplyGravity();
	virtual ~OperationApplyGravity();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	void setGravityForce(const ci::Vec3f& gravityForce);
	ci::Vec3f getGravityForce() const;

private:
	ci::Vec3f m_gravityForce;
};

#endif // OPERATION_APPLY_GRAVITY_H