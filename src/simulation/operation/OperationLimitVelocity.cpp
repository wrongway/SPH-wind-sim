#include "OperationLimitVelocity.h"

OperationLimitVelocity::OperationLimitVelocity()
	: m_speedLimit(0.0f)
{

}

OperationLimitVelocity::~OperationLimitVelocity()
{

}

void OperationLimitVelocity::operate(ParticleContainer& particles, const float deltaTime)
{
	if (m_windSpeed <= 0.0f)
	{
		return;
	}

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float simulationFactor = particles[i].getSimulationFactor();
		if (simulationFactor > 0.0f && particles[i].getIsGhost() == false)
		{
			ci::Vec3f velocity = particles[i].getVelocity();

			float oldY = velocity.y;
			velocity = particles[i].getVelocity();
			float speed = velocity.length();
			if (speed > m_windSpeed)
			{
				float overspeedLimit = m_speedLimit - m_windSpeed;
				float overspeed = speed - m_windSpeed;
				if (overspeed > overspeedLimit)
				{
					overspeed = overspeedLimit;
				}

				float x = overspeed / overspeedLimit;
				// 1 - (-2(1-x)^2 + 3(1-x)^2)
				x = 1.0f - (-2.0f * std::pow(1.0f - x, 2.0f) + 3.0f * std::pow(1.0f - x, 2.0f));
				x *= overspeedLimit;
				x += m_windSpeed;

				velocity.normalize();
				velocity *= x;
				velocity.y = oldY;
			}

			particles[i].setVelocity(velocity);
		}
	}
}

void OperationLimitVelocity::moveDomain(const ci::Vec3f& displacement)
{

}

std::string OperationLimitVelocity::getName() const
{
	return "OperationLimitVelocity";
}

void OperationLimitVelocity::setWindSpeed(const float windSpeed)
{
	m_windSpeed = windSpeed;
}

float OperationLimitVelocity::getWindSpeed() const
{
	return m_windSpeed;
}

void OperationLimitVelocity::setSpeedLimit(const float speedLimit)
{
	m_speedLimit = speedLimit;
}

float OperationLimitVelocity::getSpeedLimit() const
{
	return m_speedLimit;
}