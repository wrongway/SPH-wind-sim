#ifndef OPERATION_CALCULATE_FORCES_H
#define OPERATION_CALCULATE_FORCES_H

#include "IOperation.h"

#include "core/algorithm/RegularGrid3D.h"

#include "simulation/kernels/IKernelFunction.h"

class OperationCalculateForces : public IOperation
{
public:
	OperationCalculateForces();
	virtual ~OperationCalculateForces();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	void setIs3D(bool is3D);
	bool getIs3D() const;

	void setSupportRadius(const float supportRadius);
	float getSupportRadius() const;

	void setStiffness(const float stiffness);
	float getStiffness() const;

	void setDomainSideLength(const float domainSideLength);
	float getDomainSideLength() const;

	void setDomainCenter(const ci::Vec3f& domainCenter);
	ci::Vec3f getDomainCenter() const;

	void setDensityKernel(const std::shared_ptr<IKernelFunction>& kernel);
	void setPressureKernel(const std::shared_ptr<IKernelFunction>& kernel);
	void setViscosityKernel(const std::shared_ptr<IKernelFunction>& kernel);

	void setDensityKernelNear(const std::shared_ptr<IKernelFunction>& kernel);
	void setPressureKernelNear(const std::shared_ptr<IKernelFunction>& kernel);
	void setViscosityKernelNear(const std::shared_ptr<IKernelFunction>& kernel);

private:
	void clearVectors();

	void findNeighbours(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range);
	void updateDensities(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range);
	void updatePressureForces(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range);
	void updateViscousForces(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range);

	void calculateDensitySelfInfluences(const ParticleContainer& particles, const std::pair<unsigned int, unsigned int>& range);
	void initForces(const ParticleContainer& particles);

	void applyForces(ParticleContainer& particles, const float deltaTime);

	bool m_is3D;

	float m_supportRadius;
	float m_supportRadiusSquared;
	float m_stiffness;

	float m_distanceTolerance;

	float m_domainSideLength;

	std::vector<std::vector<unsigned int>> m_neighbours;

	std::vector<float> m_densities;
	std::vector<ci::Vec3f> m_forces;

	std::shared_ptr<IKernelFunction> m_densityKernel;
	std::shared_ptr<IKernelFunction> m_pressureKernel;
	std::shared_ptr<IKernelFunction> m_viscosityKernel;

	std::shared_ptr<IKernelFunction> m_densityKernelNear;
	std::shared_ptr<IKernelFunction> m_pressureKernelNear;
	std::shared_ptr<IKernelFunction> m_viscosityKernelNear;

	ci::Vec3f m_domainCenter;
	RegularGrid3D<unsigned int> m_neighbourGrid;
};

#endif // OPERATION_CALCULATE_FORCES_H