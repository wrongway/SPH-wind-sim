#ifndef I_OPERATION_H
#define I_OPERATION_H

#include <vector>

#include "simulation/particle/Particle.h"
#include "simulation/particle/ParticleContainer.h"

class IOperation
{
public:
	IOperation() {}
	virtual ~IOperation() {}

	virtual void operate(ParticleContainer& particles, const float deltaTime) = 0;

	virtual void moveDomain(const ci::Vec3f& displacement){}

	virtual std::string getName() const = 0;
};

#endif // I_OPERATION_H