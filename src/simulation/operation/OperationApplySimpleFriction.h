#ifndef OPERATION_APPLY_SIMPLE_FRICTION_H
#define OPERATION_APPLY_SIMPLE_FRICTION_H

#include "IOperation.h"

class OperationApplySimpleFriction : public IOperation
{
public:
	OperationApplySimpleFriction();
	virtual ~OperationApplySimpleFriction();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	void setFrictionFactor(const float friction);
	float getFrictionFactor() const;

private:
	float m_frictionFactor;
};

#endif // OPERATION_APPLY_SIMPLE_FRICTION_H