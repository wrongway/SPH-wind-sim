#include "OperationEvolveInflowParticles.h"

OperationEvolveInflowParticles::OperationEvolveInflowParticles()
	: m_wind(0.0f, 0.0f, 0.0f)
{

}

OperationEvolveInflowParticles::~OperationEvolveInflowParticles()
{

}

void OperationEvolveInflowParticles::operate(ParticleContainer& particles, const float deltaTime)
{
	ci::Vec3f minCorner = particles.getBoundingBoxMinCorner();
	ci::Vec3f maxCorner = particles.getBoundingBoxMaxCorner();

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float simulationFactor = particles[i].getSimulationFactor();
		if (simulationFactor < 1.0f && particles[i].getIsGhost() == false)
		{
			float inflowFactor = 1.0f - simulationFactor;
			ci::Vec3f velocity = particles[i].getVelocity();
			particles[i].setPosition(particles[i].getPosition() + (m_wind * deltaTime * inflowFactor) + (velocity * deltaTime * simulationFactor));

			ci::Vec3f newPos = particles[i].getPosition();

			// update min/max corner
			if (minCorner.x > newPos.x)
			{
				minCorner.x = newPos.x;
			}
			if (minCorner.y > newPos.y)
			{
				minCorner.y = newPos.y;
			}
			if (minCorner.z > newPos.z)
			{
				minCorner.z = newPos.z;
			}

			if (maxCorner.x < newPos.x)
			{
				maxCorner.x = newPos.x;
			}
			if (maxCorner.y < newPos.y)
			{
				maxCorner.y = newPos.y;
			}
			if (maxCorner.z < newPos.z)
			{
				maxCorner.z = newPos.z;
			}
		}
	}

	particles.setBoundingBoxMinCorner(minCorner);
	particles.setBoundingBoxMaxCorner(maxCorner);
}

std::string OperationEvolveInflowParticles::getName() const
{
	return "OperationEvolveInflowParticles";
}

ci::Vec3f OperationEvolveInflowParticles::getWind() const
{
	return m_wind;
}

void OperationEvolveInflowParticles::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}