#ifndef OPERATION_APPLY_WIND_H
#define OPERATION_APPLY_WIND_H

#include "IOperation.h"

class OperationApplyWind : public IOperation
{
public:
	OperationApplyWind();
	virtual ~OperationApplyWind();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	ci::Vec3f calculateWindCorrection(const ci::Vec3f& wind, const ci::Vec3f& particleVelocity);

	ci::Vec3f m_wind;
};

#endif // OPERATION_APPLY_WIND_H