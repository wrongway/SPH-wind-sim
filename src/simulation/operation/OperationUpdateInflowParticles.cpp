#include "OperationUpdateInflowParticles.h"

#include <algorithm>
#include <cfloat>

OperationUpdateInflowParticles::OperationUpdateInflowParticles()
	: m_inflowDomainMinCorner(FLT_MIN, FLT_MIN, FLT_MIN)
	, m_inflowDomainMaxCorner(FLT_MAX, FLT_MAX, FLT_MAX)
	, m_terrain(NULL)
{

}

OperationUpdateInflowParticles::~OperationUpdateInflowParticles()
{

}

void OperationUpdateInflowParticles::operate(ParticleContainer& particles, const float deltaTime)
{
	ci::Vec3f size = m_inflowDomainMaxCorner - m_inflowDomainMinCorner;

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getSimulationFactor() > 0.0f || particles[i].getIsGhost())
		{
			continue;
		}

		ci::Vec3f position = particles[i].getPosition();
		float radius = particles[i].getRadius();

		ci::Vec3f posPlusRadius = position; // +ci::Vec3f(radius, radius, radius);
		ci::Vec3f posMinusRadius = position; // -ci::Vec3f(radius, radius, radius);

		float minXDist = posPlusRadius.x - m_inflowDomainMinCorner.x;
		float minYDist = posPlusRadius.y - m_inflowDomainMinCorner.y;
		float minZDist = posPlusRadius.z - m_inflowDomainMinCorner.z;

		float maxXDist = m_inflowDomainMaxCorner.x - posPlusRadius.x;
		float maxYDist = m_inflowDomainMaxCorner.y - posPlusRadius.y;
		float maxZDist = m_inflowDomainMaxCorner.z - posPlusRadius.z;

		bool teleport = false;
		ci::Vec3f newPosition = position;

		if (minXDist < 0.0f)
		{
			teleport = true;
			newPosition.x = position.x + size.x;
		}
		/*if (minYDist < 0.0f)
		{
			teleport = true;
			newPosition.y = m_inflowDomainMaxCorner.y;
		}*/
		if (minZDist < 0.0f)
		{
			teleport = true;
			newPosition.z = position.z + size.z;
		}

		if (maxXDist < 0.0f)
		{
			teleport = true;
			newPosition.x = position.x - size.x;
		}
		/*if (maxYDist < 0.0f)
		{
			teleport = true;
			newPosition.y = m_inflowDomainMinCorner.y;
		}*/
		if (maxZDist < 0.0f)
		{
			teleport = true;
			newPosition.z = position.z - size.z;
		}

		if (teleport)
		{
			particles[i].setPosition(newPosition);
			particles[i].increaseEvolution();
		}


		if (m_terrain != NULL)
		{
			ci::Vec3f position = particles[i].getPosition();
			float altitude = 0.0f;

			if (m_terrain->getAltitudeAtPosition(position, altitude))
			{
				float newY = altitude + particles[i].getTargetRelativeAltitude();
				position.y = newY;
				particles[i].setPosition(position);
			}
		}
	}
}

void OperationUpdateInflowParticles::moveDomain(const ci::Vec3f& displacement)
{
	m_inflowDomainMinCorner += displacement;
	m_inflowDomainMaxCorner += displacement;
}

std::string OperationUpdateInflowParticles::getName() const
{
	return "OperationUpdateInflowParticles";
}

ci::Vec3f OperationUpdateInflowParticles::getInflowDomainMinCorner() const
{
	return m_inflowDomainMinCorner;
}

void OperationUpdateInflowParticles::setInflowDomainMinCorner(const ci::Vec3f& minCorner)
{
	m_inflowDomainMinCorner = minCorner;
}

ci::Vec3f OperationUpdateInflowParticles::getInflowDomainMaxCorner() const
{
	return m_inflowDomainMaxCorner;
}

void OperationUpdateInflowParticles::setInflowDomainMaxCorner(const ci::Vec3f& maxCorner)
{
	m_inflowDomainMaxCorner = maxCorner;
}

std::shared_ptr<IPolygonBody> OperationUpdateInflowParticles::getTerrain() const
{
	return m_terrain;
}

void OperationUpdateInflowParticles::setTerrain(const std::shared_ptr<IPolygonBody>& terrain)
{
	m_terrain = terrain;
}