#include "OperationApplyWind.h"

OperationApplyWind::OperationApplyWind()
{

}

OperationApplyWind::~OperationApplyWind()
{

}

void OperationApplyWind::operate(ParticleContainer& particles, const float deltaTime)
{
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float simulationFactor = particles[i].getSimulationFactor();
		if (simulationFactor > 0.0f)
		{
			/*float xCorrection = 0.0f;
			float yCorrection = 0.0f;
			float zCorrection = 0.0f;

			

			if (particles[i].getVelocity().x < m_wind.x)
			{
				xCorrection = m_wind.x - particles[i].getVelocity().x;
			}
			if (particles[i].getVelocity().y < m_wind.y)
			{
				yCorrection = m_wind.y - particles[i].getVelocity().y;
			}
			if (particles[i].getVelocity().z < m_wind.z)
			{
				zCorrection = m_wind.z - particles[i].getVelocity().z;
			}*/

			ci::Vec3f windCorrection = calculateWindCorrection(m_wind, particles[i].getVelocity());

			// particles[i].setForce(particles[i].getForce() + m_wind);
			particles[i].setVelocity(particles[i].getVelocity() + windCorrection);

			/*particles[i].setForce(m_wind);
			particles[i].setVelocity((m_wind));*/

			//ci::Vec3f particleForce = particles[i].getForce();
			//
			//float foo = particleForce.dot(m_wind.normalized());

			//if (foo*foo < m_wind.lengthSquared())
			//{
			//	// ci::Vec3f forceDiff = m_wind - particleForce;

			//	particles[i].setForce(particles[i].getForce() + m_wind.normalized() * foo);
			//	particles[i].setVelocity(particles[i].getVelocity() + (m_wind.normalized() * foo * deltaTime));
			//}
		}
	}
}

std::string OperationApplyWind::getName() const
{
	return "OperationApplyWind";
}

ci::Vec3f OperationApplyWind::getWind() const
{
	return m_wind;
}

void OperationApplyWind::setWind(const ci::Vec3f& wind)
{
	m_wind = wind;
}

ci::Vec3f OperationApplyWind::calculateWindCorrection(const ci::Vec3f& wind, const ci::Vec3f& particleVelocity)
{
	float xCorrection = 0.0f;
	float yCorrection = 0.0f;
	float zCorrection = 0.0f;

	/*if (std::abs(particleVelocity.x) < std::abs(wind.x))
	{
		xCorrection = wind.x - particleVelocity.x;
	}

	if (std::abs(particleVelocity.y) < std::abs(wind.y))
	{
		yCorrection = wind.y - particleVelocity.y;
	}

	if (std::abs(particleVelocity.z) < std::abs(wind.z))
	{
		zCorrection = wind.z - particleVelocity.z;
	}*/

	if (wind.x < 0.0f)
	{
		if (particleVelocity.x > wind.x)
		{
			xCorrection = wind.x - particleVelocity.x;
		}
	}
	else if (wind.x > 0.0f)
	{
		if (particleVelocity.x < wind.x)
		{
			xCorrection = wind.x - particleVelocity.x;
		}
	}

	if (wind.y < 0.0f)
	{
		if (particleVelocity.y > wind.y)
		{
			yCorrection = wind.y - particleVelocity.y;
		}
	}
	else if (wind.y > 0.0f)
	{
		if (particleVelocity.y < wind.y)
		{
			yCorrection = wind.y - particleVelocity.y;
		}
	}

	if (wind.z < 0.0f)
	{
		if (particleVelocity.z > wind.z)
		{
			zCorrection = wind.z - particleVelocity.z;
		}
	}
	else if (wind.z > 0.0f)
	{
		if (particleVelocity.z < wind.z)
		{
			zCorrection = wind.z - particleVelocity.z;
		}
	}


	/*if (particles[i].getVelocity().x < m_wind.x)
	{
		xCorrection = m_wind.x - particles[i].getVelocity().x;
	}
	if (particles[i].getVelocity().y < m_wind.y)
	{
		yCorrection = m_wind.y - particles[i].getVelocity().y;
	}
	if (particles[i].getVelocity().z < m_wind.z)
	{
		zCorrection = m_wind.z - particles[i].getVelocity().z;
	}*/

	return ci::Vec3f(xCorrection, yCorrection, zCorrection);
}