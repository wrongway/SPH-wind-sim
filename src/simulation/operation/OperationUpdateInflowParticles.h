#ifndef OPERATION_UPDATE_INFLOW_PARTICLES_H
#define OPERATION_UPDATE_INFLOW_PARTICLES_H

#include "IOperation.h"

#include "simulation/bodies/IPolygonBody.h"

class OperationUpdateInflowParticles : public IOperation
{
public:
	OperationUpdateInflowParticles();
	virtual ~OperationUpdateInflowParticles();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual void moveDomain(const ci::Vec3f& displacement);

	virtual std::string getName() const;

	ci::Vec3f getInflowDomainMinCorner() const;
	void setInflowDomainMinCorner(const ci::Vec3f& minCorner);

	ci::Vec3f getInflowDomainMaxCorner() const;
	void setInflowDomainMaxCorner(const ci::Vec3f& maxCorner);

	std::shared_ptr<IPolygonBody> getTerrain() const;
	void setTerrain(const std::shared_ptr<IPolygonBody>& terrain);

private:
	ci::Vec3f m_inflowDomainMinCorner;
	ci::Vec3f m_inflowDomainMaxCorner;

	std::shared_ptr<IPolygonBody> m_terrain;
};

#endif // OPERATION_UPDATE_INFLOW_PARTICLES_H