#ifndef OPERATION_LIMIT_VELOCITY_H
#define OPERATION_LIMIT_VELOCITY_H

#include "IOperation.h"

class OperationLimitVelocity : public IOperation
{
public:
	OperationLimitVelocity();
	virtual ~OperationLimitVelocity();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual void moveDomain(const ci::Vec3f& displacement);

	virtual std::string getName() const;

	void setWindSpeed(const float windSpeed);
	float getWindSpeed() const;

	void setSpeedLimit(const float speedLimit);
	float getSpeedLimit() const;

private:
	float m_windSpeed;
	float m_speedLimit;
};

#endif // OPERATION_LIMIT_VELOCITY_H