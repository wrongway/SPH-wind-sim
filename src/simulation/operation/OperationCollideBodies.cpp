#include "OperationCollideBodies.h"

#include "core/utility/logging/logging.h"

OperationCollideBodies::OperationCollideBodies()
	: m_is3d(true)
{

}

OperationCollideBodies::~OperationCollideBodies()
{

}

void OperationCollideBodies::operate(ParticleContainer& particles, const float deltaTime)
{
	for (unsigned int i = 0; i < m_bodies.size(); i++)
	{
		collideBody(particles, m_bodies[i], deltaTime);
	}
}

std::string OperationCollideBodies::getName() const
{
	return "OperationCollideBodies";
}

void OperationCollideBodies::addBody(const std::shared_ptr<IPolygonBody>& body)
{
	m_bodies.push_back(body);
}

void OperationCollideBodies::setIs3d(const bool is3d)
{
	m_is3d = is3d;
}

bool OperationCollideBodies::getIs3d() const
{
	return m_is3d;
}

void OperationCollideBodies::collideBody(ParticleContainer& particles, std::shared_ptr<IPolygonBody> body, const float deltaTime) const
{
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float simulationFactor = particles[i].getSimulationFactor();
		if (simulationFactor > 0.0f && particles[i].getIsGhost() == false /*simulationFactor > 0.0f && particles[i].getIsGhost() == false*/)
		{
			std::vector<unsigned int> triangleCandidates = body->getTriangleCandidates(particles[i].getPosition(), particles[i].getRadius());

			if (triangleCandidates.size() > 0)
			{
				ci::Vec3f position = particles[i].getPosition();
				float radius = particles[i].getRadius();
				unsigned int closestTriangle;
				float distance = body->getContactDistancePoint(particles[i].getPosition(), triangleCandidates, closestTriangle);
				float altitude = 0.0f;

				if (distance <= radius)
				{
					// LOG_WARNING_STREAM(<< "particle " << i << " below");

					ci::Vec3f contactPoint = body->getContactPointPoint(particles[i].getPosition(), closestTriangle, distance);
					ci::Vec3f contactNormal = body->getTriangleNormal(closestTriangle);

					if (m_is3d == false)
					{
						contactNormal.z = 0.0f;
						contactNormal.normalize();

						contactPoint.z = position.z;
					}

					ci::Vec3f V = contactPoint - position;
					float vDotC = V.dot(contactNormal);
					ci::Vec3f reflection = -2.0f * vDotC * contactNormal + V;

					/*reflection = reflection.normalized() * particles[i].getVelocity().length();
					*/

					vDotC = std::abs(reflection.dot(contactNormal));
					
					if (vDotC < radius || vDotC > radius)
					{
						float length = reflection.length();
						float cos = vDotC / length;
						reflection.normalize();
						vDotC = radius;
						reflection *= (vDotC / cos);
					}
					
					// ci::Vec3f displacement = (contactPoint - (reflection)); // .normalized() * particles[i].getRadius();

					// LOG_WARNING_STREAM(<< "cP: " << contactPoint);

					ci::Vec3f newPos = contactPoint + reflection;
					ci::Vec3f newVel = particles[i].getVelocity(); // newPos - position; // newPos - contactPoint;

					// newVel.y = 0.0f;

					float deflectionMag = contactNormal.dot(newVel);
					if (deflectionMag < 0.0f)
					{
						ci::Vec3f deflection = contactNormal * (-1.0f * deflectionMag);

						newVel += deflection;

						// newVel.y = 0.0f;

						// newVel *= 0.9f;
					}
					

					//if (newVel.length() <= 0.0000001f)
					//{
					//	newVel = particles[i].getVelocity();
					//}
					//else
					//{
					//	// newVel = newVel.normalized() * particles[i].getVelocity().length();
					//	newVel.y = 0.0f;
					//}
					
					particles[i].setPosition(newPos);
					particles[i].setVelocity(newVel /*particles[i].getVelocity()*/); // +(reflection)); // / deltaTime));

					/*particles[i].setPosition(contactPoint + contactNormal * particles[i].getRadius());
					ci::Vec3f vel = particles[i].getPosition() - position;
					particles[i].setVelocity(particles[i].getVelocity() + (vel / deltaTime));*/
				}
				else if (body->getAltitudeAtPosition(position, altitude)) // catch particles falling through the cracks/tunneling
				{
					if (position.y < altitude + radius)
					{
						position.y = altitude + radius;
						particles[i].setPosition(position);
					}
				}
			}
		}
	}
}