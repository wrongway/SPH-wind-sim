#ifndef OPERATION_EVOLVE_INFLOW_PARTICLES_H
#define OPERATION_EVOLVE_INFLOW_PARTICLES_H

#include "IOperation.h"

class OperationEvolveInflowParticles : public IOperation
{
public:
	OperationEvolveInflowParticles();
	virtual ~OperationEvolveInflowParticles();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	ci::Vec3f getWind() const;
	void setWind(const ci::Vec3f& wind);

private:
	ci::Vec3f m_wind;
};

#endif // OPERATION_EVOLVE_INFLOW_PARTICLES_H