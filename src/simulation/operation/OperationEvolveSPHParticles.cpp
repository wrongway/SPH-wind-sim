#include "OperationEvolveSPHParticles.h"

#include <limits>

#include "core/utility/logging/logging.h"

OperationEvolveSPHParticles::OperationEvolveSPHParticles()
	: m_terrain(NULL)
{

}

OperationEvolveSPHParticles::~OperationEvolveSPHParticles()
{

}

void OperationEvolveSPHParticles::operate(ParticleContainer& particles, const float deltaTime)
{
	ci::Vec3f minCorner(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	ci::Vec3f maxCorner(-999999.9f, -999999.9f, -999999.9f); // = minCorner * -1.0f; // (std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()); // 

	for(unsigned int i = 0; i < particles.size(); i++)
	{
		// particles[i].setPosition(particles[i].getPosition() + ci::Vec3f(0.05f, 0.0f, 0.0f) * deltaTime);

		float simulationFactor = particles[i].getSimulationFactor();
		if (simulationFactor > 0.0f && particles[i].getIsGhost() == false)
		{
			ci::Vec3f newPos = particles[i].getPosition() + (particles[i].getVelocity() * deltaTime * simulationFactor);
			ci::Vec3f oldPos = particles[i].getPosition();

			particles[i].setPosition(newPos);

			if (m_terrain != NULL)
			{
				float altitude = 0.0f;

				if (m_terrain->getAltitudeAtPosition(newPos, altitude))
				{
					particles[i].setTargetRelativeAltitude(newPos.y - altitude);
				}
			}

			// update min/max corner
			if (minCorner.x > newPos.x)
			{
				minCorner.x = newPos.x;
			}
			if (minCorner.y > newPos.y)
			{
				minCorner.y = newPos.y;
			}
			if (minCorner.z > newPos.z)
			{
				minCorner.z = newPos.z;
			}

			if (maxCorner.x < newPos.x)
			{
				maxCorner.x = newPos.x;
			}
			if (maxCorner.y < newPos.y)
			{
				maxCorner.y = newPos.y;
			}
			if (maxCorner.z < newPos.z)
			{
				maxCorner.z = newPos.z;
			}
		}
	}

	particles.setBoundingBoxMinCorner(minCorner);
	particles.setBoundingBoxMaxCorner(maxCorner);

	float radius = particles[0].getRadius();

	particles.setSphBoundingBoxMinCorner(minCorner - ci::Vec3f(radius, radius, radius));
	particles.setSphBoundingBoxMaxCorner(maxCorner + ci::Vec3f(radius, radius, radius));
}

std::string OperationEvolveSPHParticles::getName() const
{
	return "OperationEvolveSPHParticles";
}

std::shared_ptr<IPolygonBody> OperationEvolveSPHParticles::getTerrain() const
{
	return m_terrain;
}

void OperationEvolveSPHParticles::setTerrain(const std::shared_ptr<IPolygonBody>& terrain)
{
	m_terrain = terrain;
}