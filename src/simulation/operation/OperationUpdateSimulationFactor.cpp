#include "OperationUpdateSimulationFactor.h"

#include <algorithm>
#include <cfloat>

OperationUpdateSimulationFactor::OperationUpdateSimulationFactor()
	: m_sphDomainMinCorner(FLT_MIN, FLT_MIN, FLT_MIN)
	, m_sphDomainMaxCorner(FLT_MAX, FLT_MAX, FLT_MAX)
{

}

OperationUpdateSimulationFactor::~OperationUpdateSimulationFactor()
{

}

void OperationUpdateSimulationFactor::operate(ParticleContainer& particles, const float deltaTime)
{
	float minDensity = FLT_MAX;
	float maxDensity = FLT_MIN;
	float averageDensity = 0.0f;

	for (unsigned int i = 0; i < particles.size(); i++)
	{
		if (particles[i].getIsGhost())
		{
			continue;
		}

		float currentDensity = particles[i].getSPHDensity();

		if (currentDensity < minDensity)
		{
			minDensity = currentDensity;
		}
		if (currentDensity > maxDensity)
		{
			maxDensity = currentDensity;
		}

		averageDensity += (currentDensity / (float)particles.size());


		ci::Vec3f position = particles[i].getPosition();
		float radius = particles[i].getRadius();

		ci::Vec3f vecRadius(radius, radius, radius);
		ci::Vec3f posPlusRadius = position + vecRadius;
		ci::Vec3f posMinusRadius = position - vecRadius;

		float minXDist = posMinusRadius.x - m_sphDomainMinCorner.x;
		float minYDist = posMinusRadius.y - m_sphDomainMinCorner.y;
		float minZDist = posMinusRadius.z - m_sphDomainMinCorner.z;

		float maxXDist = m_sphDomainMaxCorner.x - posPlusRadius.x;
		float maxYDist = m_sphDomainMaxCorner.y - posPlusRadius.y;
		float maxZDist = m_sphDomainMaxCorner.z - posPlusRadius.z;

		float simFactor = 0.0f;

		if (minXDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(minXDist) / radius), 1.0f), simFactor);
		}
		/*if (minYDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(minYDist) / radius), 1.0f), simFactor);
		}*/
		if (minZDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(minZDist) / radius), 1.0f), simFactor);
		}

		if (maxXDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(maxXDist) / radius), 1.0f), simFactor);
		}
		/*if (maxYDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(maxYDist) / radius), 1.0f), simFactor);
		}*/
		if (maxZDist < 0.0f)
		{
			simFactor = std::max(std::min((std::abs(maxZDist) / radius), 1.0f), simFactor);
		}

		particles[i].setSimulationFactor(1.0f - simFactor);
	}

	particles.setMinDensity(minDensity);
	particles.setMaxDensity(maxDensity);
	particles.setAverageDensity(averageDensity);

	float standardDeviation = 0.0f;
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		float currentDensity = particles[i].getSPHDensity();

		standardDeviation += std::pow(currentDensity - averageDensity, 2.0f) / particles.size();
	}

	particles.setStandardDeviationDensity(std::sqrt(standardDeviation));
}

void OperationUpdateSimulationFactor::moveDomain(const ci::Vec3f& displacement)
{
	m_sphDomainMinCorner += displacement;
	m_sphDomainMaxCorner += displacement;
}

std::string OperationUpdateSimulationFactor::getName() const
{
	return "OperationUpdateSimulationFactor";
}

ci::Vec3f OperationUpdateSimulationFactor::getSPHDomainMinCorner() const
{
	return m_sphDomainMinCorner;
}

void OperationUpdateSimulationFactor::setSPHDomainMinCorner(const ci::Vec3f& minCorner)
{
	m_sphDomainMinCorner = minCorner;
}

ci::Vec3f OperationUpdateSimulationFactor::getSPHDomainMaxCorner() const
{
	return m_sphDomainMaxCorner;
}

void OperationUpdateSimulationFactor::setSPHDomainMaxCorner(const ci::Vec3f& maxCorner)
{
	m_sphDomainMaxCorner = maxCorner;
}