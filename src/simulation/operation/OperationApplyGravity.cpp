#include "OperationApplyGravity.h"

OperationApplyGravity::OperationApplyGravity()
	: m_gravityForce(0.0f, 0.0f, 0.0f)
{

}

OperationApplyGravity::~OperationApplyGravity()
{

}

void OperationApplyGravity::operate(ParticleContainer& particles, const float deltaTime)
{
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		particles[i].setForce(particles[i].getForce() + m_gravityForce);
		particles[i].setVelocity(particles[i].getVelocity() + m_gravityForce*deltaTime);
	}
}

std::string OperationApplyGravity::getName() const
{
	return "OperationApplyGravity";
}

void OperationApplyGravity::setGravityForce(const ci::Vec3f& gravityForce)
{
	m_gravityForce = gravityForce;
}

ci::Vec3f OperationApplyGravity::getGravityForce() const
{
	return m_gravityForce;
}