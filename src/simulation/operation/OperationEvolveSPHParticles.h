#ifndef OPERATION_EVOLVE_SPH_PARTICLES_H
#define OPERATION_EVOLVE_SPH_PARTICLES_H

#include "IOperation.h"

#include "simulation/bodies/IPolygonBody.h"

class OperationEvolveSPHParticles : public IOperation
{
public:
	OperationEvolveSPHParticles();
	virtual ~OperationEvolveSPHParticles();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	std::shared_ptr<IPolygonBody> getTerrain() const;
	void setTerrain(const std::shared_ptr<IPolygonBody>& terrain);

private:
	std::shared_ptr<IPolygonBody> m_terrain;
};

#endif // OPERATION_EVOLVE_SPH_PARTICLES_H