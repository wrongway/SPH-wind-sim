#ifndef OPERATION_GHOST_PARTICLES_H
#define OPERATION_GHOST_PARTICLES_H

#include "IOperation.h"

#include "simulation/bodies/IPolygonBody.h"

class OperationGhostParticles : public IOperation
{
public:
	OperationGhostParticles();
	virtual ~OperationGhostParticles();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual void moveDomain(const ci::Vec3f& displacement);

	virtual std::string getName() const;

	bool getIs3D() const;
	void setIs3D(const bool is3D);

	float getMassModifier() const;
	void setMassModifier(const float massModifier);

	float getRestDensity() const;
	void setRestDensity(const float restDensity);

	float getVescosity() const;
	void setViscosity(const float viscosity);

	ci::Vec3f getSPHCenter() const;
	void setSPHCenter(const ci::Vec3f& sphCenter);

	ci::Vec3f getMinCorner() const;
	void setMinCorner(const ci::Vec3f& minCorner);

	ci::Vec3f getMaxCorner() const;
	void setMaxCorner(const ci::Vec3f& maxCorner);

	std::shared_ptr<IPolygonBody> getTerrain() const;
	void setTerrain(const std::shared_ptr<IPolygonBody>& terrain);

private:
	void init(ParticleContainer& particles, const float particleRadius);

	bool m_is3D;
	bool m_initialized;

	float m_massModifier;
	float m_restDensity;
	float m_viscosity;

	ci::Vec3f m_sphCenter;
	ci::Vec3f m_minCorner;
	ci::Vec3f m_maxCorner;
	ci::Vec3f m_displacement;

	std::vector<unsigned int> m_ghostIndices;

	std::shared_ptr<IPolygonBody> m_terrain;
};

#endif // OPERATION_GHOST_PARTICLES_H