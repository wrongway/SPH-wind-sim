#ifndef OPERATION_UPDATE_SIMULATION_FACTOR_H
#define OPERATION_UPDATE_SIMULATION_FACTOR_H

#include "IOperation.h"

class OperationUpdateSimulationFactor : public IOperation
{
public:
	OperationUpdateSimulationFactor();
	virtual ~OperationUpdateSimulationFactor();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual void moveDomain(const ci::Vec3f& displacement);

	virtual std::string getName() const;

	ci::Vec3f getSPHDomainMinCorner() const;
	void setSPHDomainMinCorner(const ci::Vec3f& minCorner);

	ci::Vec3f getSPHDomainMaxCorner() const;
	void setSPHDomainMaxCorner(const ci::Vec3f& maxCorner);

private:
	ci::Vec3f m_sphDomainMinCorner;
	ci::Vec3f m_sphDomainMaxCorner;
};

#endif // OPERATION_UPDATE_SIMULATION_FACTOR_H