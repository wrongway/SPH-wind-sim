#include "OperationApplySimpleFriction.h"

OperationApplySimpleFriction::OperationApplySimpleFriction()
	: m_frictionFactor(1.0f)
{

}

OperationApplySimpleFriction::~OperationApplySimpleFriction()
{

}

void OperationApplySimpleFriction::operate(ParticleContainer& particles, const float deltaTime)
{
	for (unsigned int i = 0; i < particles.size(); i++)
	{
		particles[i].setVelocity(particles[i].getVelocity() * m_frictionFactor);
	}
}

std::string OperationApplySimpleFriction::getName() const
{
	return "OperationApplySimpleFriction";
}

void OperationApplySimpleFriction::setFrictionFactor(const float friction)
{
	m_frictionFactor = friction;
}

float OperationApplySimpleFriction::getFrictionFactor() const
{
	return m_frictionFactor;
}