#ifndef OPERATION_COLLIDE_BODIES_H
#define OPERATION_COLLIDE_BODIES_H

#include "IOperation.h"

#include "simulation/bodies/IPolygonBody.h"

class OperationCollideBodies : public IOperation
{
public:
	OperationCollideBodies();
	virtual ~OperationCollideBodies();

	virtual void operate(ParticleContainer& particles, const float deltaTime);

	virtual std::string getName() const;

	void addBody(const std::shared_ptr<IPolygonBody>& body);

	void setIs3d(const bool is3d);
	bool getIs3d() const;

private:
	void collideBody(ParticleContainer& particles, std::shared_ptr<IPolygonBody> body, const float deltaTime) const;

	std::vector<std::shared_ptr<IPolygonBody>> m_bodies;

	bool m_is3d;
};

#endif // OPERATION_COLLIDE_BODIES_H