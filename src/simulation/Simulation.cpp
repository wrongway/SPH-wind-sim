#include "simulation.h"

#include "cinder/Vector.h"

#include "core/UnitUtility.h"

#include "core/utility/logging/logging.h"

#include "terrain/terrain/Terrain.h"

#include "bodies/ConcaveBody.h"

#include "kernels/DensityKernel2D.h"
#include "kernels/DensityKernel2DForSquaredDistance.h"
#include "kernels/DensityKernel3D.h"
#include "kernels/DensityKernel3DForSquaredDistance.h"
#include "kernels/PressureKernel2D.h"
#include "kernels/PressureKernel3D.h"
#include "kernels/PressureKernel3D.h"
#include "kernels/ViscosityKernel2D.h"
#include "kernels/ViscosityKernel3D.h"

#include "operation/IOperation.h"
#include "operation/OperationApplyGravity.h"
#include "operation/OperationApplySimpleFriction.h"
#include "operation/OperationApplyWind.h"
#include "operation/OperationCalculateForces.h"
#include "operation/OperationCollideBodies.h"
#include "operation/OperationEvolveInflowParticles.h"
#include "operation/OperationEvolveSPHParticles.h"
#include "operation/OperationGhostParticles.h"
#include "operation/OperationLimitVelocity.h"
#include "operation/OperationUpdateInflowParticles.h"
#include "operation/OperationUpdateSimulationFactor.h"

#include "particle/particle.h"

Simulation::Simulation()
	: m_is3D(true)
	, m_useNearPressureKernels(true)
	, m_createInflow(true)
	, m_createGhosts(true)
	, m_particlesPerDimension(10)
	, m_simulationDomainSideLength(1.0f)
	, m_restDensity(1.0f)
	, m_supportRadius(0.0f)
	, m_stiffness(1.0f)
	, m_frictionFactor(1.0f)
	, m_viscosity(1.0f)
	, m_massModifier(1.0f)
	, m_gravity(0.0f, 0.0f, 0.0f)
	, m_center(0.0f, 0.0f, 0.0f)
	, m_wind(0.0f, 0.0f, 0.0f)
	, m_sphMinCorner(0.0f, 0.0f, 0.0f)
	, m_sphMaxCorner(0.0f, 0.0f, 0.0f)
	, m_inflowMinCorner(0.0f, 0.0f, 0.0f)
	, m_inflowMaxCorner(0.0f, 0.0f, 0.0f)
	, m_particles()
	, m_operations()
	, m_operationStopWatches()
	, m_terrain(NULL)
	, m_physicalTerrain(NULL)
	, m_csvLogger("performance")
{

}

Simulation::~Simulation()
{

}

void Simulation::setup(const SimulationParameters& parameters)
{
	setParameters(parameters);

	m_particles.clear();
	m_operations.clear();

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	float simulationDomainHalfSideLength = m_simulationDomainSideLength * 0.5f;

	float particleDiameter = m_simulationDomainSideLength / (float)m_particlesPerDimension;
	float particleRadius = particleDiameter * 0.5f;
	m_massModifier = parameters.massModifier; // calculateParticleMassModifier();

	ci::Vec3f startPosition(-simulationDomainHalfSideLength + particleRadius,
		-simulationDomainHalfSideLength + particleRadius,
		-simulationDomainHalfSideLength + particleRadius);

	m_supportRadius *= particleDiameter;

	m_center = screenToMeter(m_center);
	m_sphMinCorner = startPosition + m_center;
	m_sphMaxCorner = startPosition + ci::Vec3f(m_simulationDomainSideLength, m_simulationDomainSideLength, m_simulationDomainSideLength) + m_center;
	m_inflowMinCorner = m_sphMinCorner;
	m_inflowMaxCorner = m_sphMaxCorner;

	int count = 0;
	float mass = 0.0f;
	float volume = 0.0f;

	unsigned int inflowPerSide = 0;
	if (m_createInflow)
	{
		inflowPerSide = m_supportRadius / particleDiameter + 1;
		startPosition -= ci::Vec3f(m_supportRadius + particleDiameter, 0.0f, m_supportRadius + particleDiameter);

		m_inflowMinCorner = m_sphMinCorner - ci::Vec3f(m_supportRadius + particleDiameter, 0.0f, m_supportRadius + particleDiameter);
		m_inflowMaxCorner = m_sphMaxCorner + ci::Vec3f(m_supportRadius + particleDiameter, 0.0f, m_supportRadius + particleDiameter);
	}
	
	m_particles.setNormMinCorner(m_inflowMinCorner);
	m_particles.setNormMaxCorner(m_inflowMaxCorner);
	m_particles.setBoundingBoxMinCorner(m_inflowMinCorner);
	m_particles.setBoundingBoxMaxCorner(m_inflowMaxCorner);
	m_particles.setNormSPHDomainMinCorner(m_sphMinCorner);
	m_particles.setNormSPHDomainMaxCorner(m_sphMaxCorner);

	if (m_is3D)
	{
		for (unsigned int yI = 0; yI < m_particlesPerDimension; yI++)
		{
			for (unsigned int xI = 0; xI < m_particlesPerDimension + (inflowPerSide*2); xI++)
			{
				for (unsigned int zI = 0; zI < m_particlesPerDimension + (inflowPerSide * 2); zI++)
				{
					count++;

					x = startPosition.x + (xI * particleDiameter);
					y = startPosition.y + (yI * (particleDiameter * 1.0f));
					z = startPosition.z + (zI * particleDiameter);

					Particle particle = Particle::createParticle(ci::Vec3f(x, y, z) + m_center, m_restDensity, particleRadius, m_massModifier, 0.0f, m_viscosity);
					particle.setIs3D(m_is3D);
					particle.setTargetRelativeAltitude((y + particleRadius) - startPosition.y);

					mass += particle.getMass();
					volume += particle.getVolume();

					m_particles.push_back(particle);
				}
			}
		}
	}
	else
	{
		for (unsigned int yI = 0; yI < m_particlesPerDimension; yI++)
		{
			for (unsigned int xI = 0; xI < m_particlesPerDimension + (inflowPerSide * 2); xI++)
			{
				count++;

				x = startPosition.x + (xI * particleDiameter);
				y = startPosition.y + (yI * (particleDiameter * 1.0f));

				Particle particle = Particle::createParticle(ci::Vec3f(x, y, z) + m_center, m_restDensity, particleRadius, m_massModifier, 0.0f, m_viscosity);
				particle.setIs3D(m_is3D);
				particle.setTargetRelativeAltitude((y + particleRadius) - startPosition.y);

				mass += particle.getMass();
				volume += particle.getVolume();

				m_particles.push_back(particle);
			}
		}
	}

	m_particles.setNormParticlesPerDimension(m_particlesPerDimension);

	LOG_INFO_STREAM(<< "count: " << count << " / mass: " << mass << " / volume: " << volume << " (" << m_simulationDomainSideLength * m_simulationDomainSideLength * m_simulationDomainSideLength << ")");

	setupOperations();
	m_csvLogger.nextLine();

	for (unsigned int i = 0; i < m_particles.size(); i++)
	{
		ci::Vec3f position = m_particles[i].getPosition();
		float altitude = 0.0f;

		if (m_physicalTerrain->getAltitudeAtPosition(position, altitude))
		{
			float newY = altitude + m_particles[i].getTargetRelativeAltitude();
			position.y = newY;
			m_particles[i].setPosition(position);
		}
	}
}

void Simulation::update(const float deltaTime)
{
	// LOG_WARNING_STREAM(<< m_particles.size());

	int sphCount = 0;
	int inflowCount = 0;
	for (unsigned int i = 0; i < m_particles.size(); i++)
	{
		if (m_particles[i].getIsGhost() == false)
		{
			if (m_particles[i].getSimulationFactor() < 1.0f)
			{
				inflowCount++;
			}
			else
			{
				sphCount++;
			}
		}
	}

	for (unsigned int i = 0; i < m_operations.size(); i++)
	{
		m_operationStopWatches[i].start();
		m_operations[i]->operate(m_particles, deltaTime);
		m_operationStopWatches[i].stop();
		m_csvLogger.logValue(m_operationStopWatches[i].getTime());
	}
	m_csvLogger.nextLine();
}

void Simulation::moveDomain(const ci::Vec3f& displacement)
{
	ci::Vec3f disp = displacement;

	if (m_is3D == false)
	{
		disp.z = 0.0f;
	}

	m_center += disp;

	for (unsigned int i = 0; i < m_operations.size(); i++)
	{
		m_operations[i]->moveDomain(disp);
	}
}

bool Simulation::getIs3D() const
{
	return m_is3D;
}

unsigned int Simulation::getParticlesPerDimension() const
{
	return m_particlesPerDimension;
}

float Simulation::getSimulationDomainSideLength() const
{
	return m_simulationDomainSideLength;
}

float Simulation::getRestDensity() const
{
	return m_restDensity;
}

ParticleContainer Simulation::getParticles() const
{
	return m_particles;
}

std::shared_ptr<IPolygonBody> Simulation::getTerrain()
{
	return m_physicalTerrain;
}

void Simulation::setParameters(const SimulationParameters& parameters)
{
	m_is3D = parameters.is3D;
	m_useNearPressureKernels = parameters.useNearPressureKernels;
	m_createInflow = parameters.createInflow;
	m_createGhosts = parameters.createGhosts;
	m_particlesPerDimension = parameters.particlesPerDimension;
	m_simulationDomainSideLength = parameters.simulationDomainSideLength;
	m_restDensity = parameters.restDensity;
	m_stiffness = parameters.stiffness;
	m_frictionFactor = parameters.frictionFactor;
	m_viscosity = parameters.viscosity;
	m_supportRadius = parameters.supportRadiusScale;
	m_gravity = parameters.gravityForce;
	m_center = parameters.center;
	m_wind = parameters.wind;
	m_terrain = parameters.terrain;
}

void Simulation::setupOperations()
{
	std::shared_ptr<OperationGhostParticles> ghostParticles;
	if (m_createGhosts)
	{
		ghostParticles = std::make_shared<OperationGhostParticles>();
		ghostParticles->setIs3D(m_is3D);
		ghostParticles->setRestDensity(m_restDensity);
		ghostParticles->setMassModifier(m_massModifier);
		ghostParticles->setSPHCenter(m_center);
		ghostParticles->setMinCorner(m_inflowMinCorner - ci::Vec3f(0.0f, m_supportRadius, 0.0f));
		ghostParticles->setMaxCorner(m_inflowMaxCorner - ci::Vec3f(0.0f, m_simulationDomainSideLength, 0.0f));
		ghostParticles->setViscosity(m_viscosity);
		addOperation(ghostParticles);
	}

	std::shared_ptr<IKernelFunction> densityKernel;
	std::shared_ptr<IKernelFunction> pressureKernel;
	std::shared_ptr<IKernelFunction> viscosityKernel;

	if (m_is3D)
	{
		densityKernel = std::make_shared<DensityKernel3DForSquaredDistance>();
		pressureKernel = std::make_shared<PressureKernel3D>();
		viscosityKernel = std::make_shared<ViscosityKernel3D>();
	}
	else
	{
		// densityKernel = std::make_shared<DensityKernel2D>();
		densityKernel = std::make_shared<DensityKernel2DForSquaredDistance>();
		pressureKernel = std::make_shared<PressureKernel2D>();
		viscosityKernel = std::make_shared<ViscosityKernel2D>();
	}

	std::shared_ptr<OperationCalculateForces> calculateForces = std::make_shared<OperationCalculateForces>();
	calculateForces->setIs3D(m_is3D);
	calculateForces->setSupportRadius(m_supportRadius);
	calculateForces->setStiffness(m_stiffness);
	calculateForces->setDensityKernel(densityKernel);
	calculateForces->setPressureKernel(pressureKernel);
	calculateForces->setViscosityKernel(viscosityKernel);
	if (m_useNearPressureKernels)
	{
		calculateForces->setDensityKernelNear(densityKernel);
		calculateForces->setPressureKernelNear(pressureKernel);
		//calculateForces->setViscosityKernelNear(viscosityKernel);
	}
	calculateForces->setDomainCenter(m_center);
	calculateForces->setDomainSideLength(m_inflowMaxCorner.x - m_inflowMinCorner.x);
	addOperation(calculateForces);

	// std::shared_ptr<Oper

	std::shared_ptr<OperationApplyWind> applyWind = std::make_shared<OperationApplyWind>();
	applyWind->setWind(m_wind);
	addOperation(applyWind);

	std::shared_ptr<OperationApplySimpleFriction> applySimpleFriction = std::make_shared<OperationApplySimpleFriction>();
	applySimpleFriction->setFrictionFactor(m_frictionFactor);
	addOperation(applySimpleFriction);

	std::shared_ptr<OperationLimitVelocity> limitVelocity = std::make_shared<OperationLimitVelocity>();
	limitVelocity->setWindSpeed(m_wind.length());
	limitVelocity->setSpeedLimit(m_wind.length() * 1.5f);
	addOperation(limitVelocity);

	std::shared_ptr<OperationApplyGravity> applyGravity = std::make_shared<OperationApplyGravity>();
	applyGravity->setGravityForce(m_gravity);
	addOperation(applyGravity);

	std::shared_ptr<OperationEvolveSPHParticles> evolveParticles = std::make_shared<OperationEvolveSPHParticles>();
	// evolveParticles->setWin
	addOperation(evolveParticles);

	std::shared_ptr<OperationEvolveInflowParticles> evolveInflow = std::make_shared<OperationEvolveInflowParticles>();
	evolveInflow->setWind(m_wind);
	addOperation(evolveInflow);

	if (m_terrain != NULL)
	{
		std::shared_ptr<OperationCollideBodies> collideBodies = std::make_shared<OperationCollideBodies>();

		std::vector<Triangle> triangles;

		std::vector<unsigned int> indices = m_terrain->getIndices();
		std::vector<ci::Vec3f> vertices = m_terrain->getVertices();
		for(unsigned int i = 0; i < indices.size() / 3; i++)
		{
			unsigned int indexIdx = i * 3;

			Triangle triangle(screenToMeter(vertices[indices[indexIdx]]), screenToMeter(vertices[indices[indexIdx + 1]]), screenToMeter(vertices[indices[indexIdx + 2]]));
			triangles.push_back(triangle);
		}

		std::shared_ptr<ConcaveBody> physicalTerrain = std::make_shared<ConcaveBody>();
		physicalTerrain->setTriangles(triangles, m_terrain->getCubesPerDimension());
		m_physicalTerrain = physicalTerrain;

		collideBodies->addBody(m_physicalTerrain);
		collideBodies->setIs3d(m_is3D);
		addOperation(collideBodies);
	}

	if (ghostParticles != NULL)
	{
		ghostParticles->setTerrain(m_physicalTerrain);
	}
	evolveParticles->setTerrain(m_physicalTerrain);

	ci::Vec3f simDomainMinCorner = m_center - (ci::Vec3f(m_simulationDomainSideLength, m_simulationDomainSideLength, m_simulationDomainSideLength) * 0.5f);
	ci::Vec3f simDomainMaxCorner = m_center + (ci::Vec3f(m_simulationDomainSideLength, m_simulationDomainSideLength, m_simulationDomainSideLength) * 0.5f);

	std::shared_ptr<OperationUpdateSimulationFactor> updateSimulationFactor = std::make_shared<OperationUpdateSimulationFactor>();
	updateSimulationFactor->setSPHDomainMinCorner(simDomainMinCorner);
	updateSimulationFactor->setSPHDomainMaxCorner(simDomainMaxCorner);
	addOperation(updateSimulationFactor);

	std::shared_ptr<OperationUpdateInflowParticles> updateInflowParticles = std::make_shared<OperationUpdateInflowParticles>();
	/*updateInflowParticles->setInflowDomainMinCorner(simDomainMinCorner - ci::Vec3f(m_supportRadius, 0.0f, m_supportRadius));
	updateInflowParticles->setInflowDomainMaxCorner(simDomainMaxCorner + ci::Vec3f(m_supportRadius, 0.0f, m_supportRadius));*/
	updateInflowParticles->setInflowDomainMinCorner(m_inflowMinCorner);
	updateInflowParticles->setInflowDomainMaxCorner(m_inflowMaxCorner);
	updateInflowParticles->setTerrain(m_physicalTerrain);
	addOperation(updateInflowParticles);
}

void Simulation::addOperation(const std::shared_ptr<IOperation>& operation)
{
	m_operations.push_back(operation);
	m_operationStopWatches.push_back(StopWatch());
	m_csvLogger.logValue(operation->getName());
}

float Simulation::calculateParticleMassModifier() const
{
	std::shared_ptr<IKernelFunction> densityKernel;

	if (m_is3D)
	{
		densityKernel = std::make_shared<DensityKernel3D>();
	}
	else
	{
		densityKernel = std::make_shared<DensityKernel2D>();
	}

	float modifier = densityKernel->kernel(0.0f, m_supportRadius);

	return modifier;
}