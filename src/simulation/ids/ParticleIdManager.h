#ifndef PARTICLE_ID_MANAGER_H
#define PARTICLE_ID_MANAGER_H

#include <memory>

#include "core/utility/IdManager.h"

class ParticleIdManager : public IdManager
{
public:
	~ParticleIdManager();

	static std::shared_ptr<ParticleIdManager> getInstance();

private:
	ParticleIdManager();

	static std::shared_ptr<ParticleIdManager> m_instance;
};

#endif // PARTICLE_ID_MANAGER_H