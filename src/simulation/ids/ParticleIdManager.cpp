#include "ParticleIdManager.h"

std::shared_ptr<ParticleIdManager> ParticleIdManager::m_instance = NULL;

std::shared_ptr<ParticleIdManager> ParticleIdManager::getInstance()
{
	if (m_instance == NULL)
	{
		m_instance = std::shared_ptr<ParticleIdManager>(new ParticleIdManager());
	}

	return m_instance;
}

ParticleIdManager::ParticleIdManager()
{

}

ParticleIdManager::~ParticleIdManager()
{

}