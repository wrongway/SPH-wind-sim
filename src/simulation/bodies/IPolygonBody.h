#ifndef I_BODY_H
#define I_BODY_H

#include "cinder/Vector.h"

class IPolygonBody
{
public:
	IPolygonBody(){}
	virtual ~IPolygonBody(){}

	virtual float getContactDistancePoint(const ci::Vec3f& point, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle) = 0;
	virtual float getContactDistanceSphere(const ci::Vec3f& point, const float radius, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle) = 0;

	virtual ci::Vec3f getContactPointPoint(const ci::Vec3f& point, const unsigned int triangle, const float distance) = 0;

	virtual bool getAltitudeAtPosition(const ci::Vec3f& position, float& altitude) = 0;
	virtual std::vector<unsigned int> getTriangleCandidates(const ci::Vec3f& point) const = 0;
	virtual std::vector<unsigned int> getTriangleCandidates(const ci::Vec3f& point, const float radius) const = 0;
	virtual std::vector<unsigned int> getTriangleCandidatesXZProjected(const ci::Vec3f& point) const = 0;
	virtual ci::Vec3f getTriangleNormal(const unsigned int triangleIndex) const = 0;
};

#endif // I_BODY_H