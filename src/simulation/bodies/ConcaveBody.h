#ifndef CONCAVE_BODY_H
#define CONCAVE_BODY_H

#include "IPolygonBody.h"

#include <mutex>

#include "core/algorithm/RegularGrid.h"
#include "core/algorithm/RegularGrid2D.h"

#include "core/math/Triangle.h"

#include "core/utility/logging/CSVLogger.h"

class ConcaveBody : public IPolygonBody
{
public:
	ConcaveBody();
	virtual ~ConcaveBody();

	virtual float getContactDistancePoint(const ci::Vec3f& point, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle);
	virtual float getContactDistanceSphere(const ci::Vec3f& point, const float radius, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle);

	virtual ci::Vec3f getContactPointPoint(const ci::Vec3f& point, const unsigned int triangle, const float distance);
	
	virtual bool getAltitudeAtPosition(const ci::Vec3f& position, float& altitude);

	virtual std::vector<unsigned int> getTriangleCandidates(const ci::Vec3f& point) const;
	virtual std::vector<unsigned int> getTriangleCandidates(const ci::Vec3f& point, const float radius) const;
	virtual std::vector<unsigned int> getTriangleCandidatesXZProjected(const ci::Vec3f& point) const;

	virtual ci::Vec3f getTriangleNormal(const unsigned int triangleIndex) const;

	void setTriangles(std::vector<Triangle>& triangles, const int cubesPerDimension);

private:
	std::vector<unsigned int> getPreCandidates(const ci::Vec3f& point) const;

	void parallelAddCandidates(std::pair<std::vector<unsigned int>::iterator, std::vector<unsigned int>::iterator> startEnd, std::vector<unsigned int>* candidates, const ci::Vec3f& point) const;

	std::vector<Triangle> m_triangles;

	// RegularGrid<unsigned int, 2> m_triangleGrid;
	RegularGrid2D<unsigned int> m_triangleGrid;

	float m_maxY;

	mutable std::mutex m_mutexCandidates;

	CSVLogger m_csvLogger;
};

#endif // CONCAVE_BODY_H