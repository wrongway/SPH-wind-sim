#include "ConcaveBody.h"

#include <algorithm>
#include <cfloat>
#include <thread>

#include "core/math/MathUtility.h"
#include "core/math/VectorUtility.h"
#include "core/utility/logging/logging.h"
#include "core/utility/profiling/StopWatch.h"

ConcaveBody::ConcaveBody()
	: m_mutexCandidates()
	, m_csvLogger("triangleCandidates")
{

}

ConcaveBody::~ConcaveBody()
{

}

float ConcaveBody::getContactDistancePoint(const ci::Vec3f& point, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle)
{
	float smallestDistance = FLT_MAX;
	const size_t num_triangles = m_triangles.size();

	if (triangleCandidates.size() > 0)
	{
		for (unsigned int i = 0; i < triangleCandidates.size(); i++)
		{
			const float distToTriangle = m_triangles[triangleCandidates[i]].getShortestDistance(point);
			if (distToTriangle < smallestDistance)
			{
				smallestDistance = distToTriangle;
				closestTriangle = triangleCandidates[i];
			}
		}
	}
	else
	{
		for (unsigned iTriangle = 0; iTriangle < num_triangles; ++iTriangle)
		{
			const float distToTriangle = m_triangles[iTriangle].getShortestDistance(point);
			if (distToTriangle < smallestDistance)
			{
				smallestDistance = distToTriangle;
				closestTriangle = iTriangle;
			}
		}
	}

	return smallestDistance;
}

float ConcaveBody::getContactDistanceSphere(const ci::Vec3f& point, const float radius, const std::vector<unsigned int>& triangleCandidates, unsigned int& closestTriangle)
{
	float smallestDistance = FLT_MAX;

	if (triangleCandidates.size() > 0)
	{
		for (unsigned i = 0; i < triangleCandidates.size(); ++i)
		{
			const float distToTriangle = m_triangles[triangleCandidates[i]].getShortestDistance(point) - radius;
			if (distToTriangle > 0.0f)
			{
				return distToTriangle;
			}
			if (distToTriangle < smallestDistance)
			{
				smallestDistance = distToTriangle;
				closestTriangle = triangleCandidates[i];
			}
		}
	}
	else
	{
		const size_t num_triangles = m_triangles.size();
		for (unsigned iTriangle = 0; iTriangle < num_triangles; ++iTriangle)
		{
			const float distToTriangle = m_triangles[iTriangle].getShortestDistance(point) - radius;
			if (distToTriangle > 0.0f)
			{
				closestTriangle = iTriangle;
				return distToTriangle;
			}
			if (distToTriangle < smallestDistance)
			{
				smallestDistance = distToTriangle;
				closestTriangle = iTriangle;
			}
		}
	}
	return smallestDistance;
}

ci::Vec3f ConcaveBody::getContactPointPoint(const ci::Vec3f& point, const unsigned int triangle, const float distance)
{
	const ci::Vec3f contactNormal = m_triangles[triangle].GetNormal();
	ci::Vec3f contactPoint = point - (contactNormal * distance);
	return contactPoint;
}

bool ConcaveBody::getAltitudeAtPosition(const ci::Vec3f& position, float& altitude)
{
	altitude = 0.0f;

	std::vector<unsigned int> candidates = getTriangleCandidatesXZProjected(position);

	for (unsigned int i = 0; i < candidates.size(); i++)
	{
		altitude = m_triangles[candidates[i]].getAltitudeAtPoint(position);
		return true;
	}

	return false;
}

std::vector<unsigned int> ConcaveBody::getTriangleCandidates(const ci::Vec3f& point) const
{
	std::vector<unsigned int> candidates;

	if (point.y > m_maxY)
	{
		return candidates;
	}

	std::vector<unsigned int> preCandidates = getPreCandidates(point);

	for (unsigned int i = 0; i < preCandidates.size(); i++)
	{
		if (point.y > m_triangles[preCandidates[i]].getMaxY())
		{
			continue;
		}

		if (m_triangles[preCandidates[i]].testCircumsphere(point))
		{
			candidates.push_back(preCandidates[i]);
		}
	}

	return candidates;
}

std::vector<unsigned int> ConcaveBody::getTriangleCandidates(const ci::Vec3f& point, const float radius) const
{
	std::vector<unsigned int> candidates;

	if (point.y - radius > m_maxY)
	{
		return candidates;
	}

	std::vector<unsigned int> preCandidates = getPreCandidates(point);

	for (unsigned int i = 0; i < preCandidates.size(); i++)
	{
		if (point.y - radius > m_triangles[preCandidates[i]].getMaxY())
		{
			continue;
		}

		if (m_triangles[preCandidates[i]].testCircumsphere(point, radius))
		{
			candidates.push_back(preCandidates[i]);
		}
	}

	return candidates;
}

std::vector<unsigned int> ConcaveBody::getTriangleCandidatesXZProjected(const ci::Vec3f& point) const
{
	std::vector<unsigned int> candidates;

	std::vector<unsigned int> preCandidates = getPreCandidates(point);

	/*if (preCandidates.size() <= 0)
	{
		LOG_WARNING_STREAM(<< "no pre candidates at " << point);
	}*/

	if (preCandidates.size() <= 0)
	{
		return candidates;
	}

	int threadCount = min(4, preCandidates.size());
	int candidatesPerThread = preCandidates.size() / threadCount;

	if (candidatesPerThread <= 0)
	{
		candidatesPerThread = 1;
	}

	/*std::vector<std::thread> threads;
	for (unsigned int i = 0; i < threadCount; i++)
	{
		std::pair<std::vector<unsigned int>::iterator, std::vector<unsigned int>::iterator> startEnd;
		startEnd.first = preCandidates.begin() + (i*candidatesPerThread);
		if (i == threadCount - 1)
		{
			startEnd.second = preCandidates.end();
		}
		else
		{
			startEnd.second = preCandidates.begin() + ((i + 1)*candidatesPerThread);
		}
		
		threads.push_back(std::thread(&ConcaveBody::parallelAddCandidates, this, startEnd, &candidates, point));
	}

	for (unsigned int i = 0; i < threadCount; i++)
	{
		threads[i].join();
	}*/

	std::pair<std::vector<unsigned int>::iterator, std::vector<unsigned int>::iterator> startEnd;
	startEnd.first = preCandidates.begin();
	startEnd.second = preCandidates.end();
	parallelAddCandidates(startEnd, &candidates, point);

	/*if (candidates.size() <= 0)
	{
		LOG_WARNING_STREAM(<< "no candidates at " << point);
	}*/

	/*std::thread t(&ConcaveBody::parallelAddCandidates, this, startEnd, candidates, point);
	t.join();*/

	/*for (unsigned int i = 0; i < preCandidates.size(); i++)
	{
		if (m_triangles[preCandidates[i]].checkAbove(point))
		{
			candidates.push_back(preCandidates[i]);
		}
	}*/

	return candidates;
}

ci::Vec3f ConcaveBody::getTriangleNormal(const unsigned int triangleIndex) const
{
	return m_triangles[triangleIndex].GetNormal();
}

void ConcaveBody::setTriangles(std::vector<Triangle>& triangles, const int cubesPerDimension)
{
	m_triangles = triangles;

	float minX = FLT_MAX; // std::numeric_limits<float>::max();
	float maxX = FLT_MIN; // std::numeric_limits<float>::min();

	float minZ = FLT_MAX; // std::numeric_limits<float>::max();
	float maxZ = FLT_MIN; // std::numeric_limits<float>::min();

	float maxTriangleXSize = FLT_MIN; // std::numeric_limits<float>::min();
	float maxTriangleZSize = FLT_MIN; // std::numeric_limits<float>::min();

	m_maxY = FLT_MIN;

	for (unsigned int i = 0; i < m_triangles.size(); i++)
	{
		float xSize = FLT_MIN; // std::numeric_limits<float>::min();
		float zSize = FLT_MIN; // std::numeric_limits<float>::min();

		std::vector<float> x;

		std::vector<float> z;

		for (unsigned int j = 0; j < 3; j++)
		{
			ci::Vec3f vertex = m_triangles[i].getVertex(j);

			if (vertex.x > maxX)
			{
				maxX = vertex.x;
			}
			if (vertex.x < minX)
			{
				minX = vertex.x;
			}

			if (vertex.y > m_maxY)
			{
				m_maxY = vertex.y;
			}

			if (vertex.z > maxZ)
			{
				maxZ = vertex.z;
			}
			if (vertex.z < minZ)
			{
				minZ = vertex.z;
			}

			x.push_back(vertex.x);
			z.push_back(vertex.z);
		}

		if (std::abs(x[0] - x[1]) > maxTriangleXSize)
		{
			maxTriangleXSize = std::abs(x[0] - x[1]);
		}
		if (std::abs(x[0] - x[2]) > maxTriangleXSize)
		{
			maxTriangleXSize = std::abs(x[0] - x[2]);
		}

		if (std::abs(z[0] - z[1]) > maxTriangleZSize)
		{
			maxTriangleZSize = std::abs(z[0] - z[1]);
		}
		if (std::abs(z[0] - z[2]) > maxTriangleZSize)
		{
			maxTriangleZSize = std::abs(z[0] - z[2]);
		}
	}

	float sideLength = MathUtility::maximum(std::abs(maxX - minX), std::abs(maxZ - minZ));
	
	float cellSideLength = MathUtility::maximum(maxTriangleXSize, maxTriangleZSize);
	unsigned int cellsPerDim = cubesPerDimension; // unsigned int(sideLength / cellSideLength);

	ci::Vec2f center;
	center.x = (minX + maxX) * 0.5f;
	center.y = (minZ + maxZ) * 0.5f;

	m_triangleGrid.setGridSideLength(sideLength);
	m_triangleGrid.setCellsPerDimension(cellsPerDim);
	m_triangleGrid.setCenter(center);
	m_triangleGrid.clearCells();

	LOG_WARNING_STREAM(<< "triangle grid:" << sideLength << ", " << cellsPerDim);

	for (unsigned int i = 0; i < m_triangles.size(); i++)
	{
		m_triangleGrid.addValue(VectorUtility::projectXZ(m_triangles[i].getCenter()), i);
	}
}

std::vector<unsigned int> ConcaveBody::getPreCandidates(const ci::Vec3f& point) const
{
	std::vector<std::vector<unsigned int>> cells = m_triangleGrid.getCells(VectorUtility::projectXZ(point), 1);

	//float cellCount = float(cells.size());
	//float particleCount = 0.0f;

	//for (unsigned int i = 0; i < cells.size(); i++)
	//{
	//	particleCount += float(cells[i].size());

	//	const_cast<CSVLogger&>(m_csvLogger).logValue(cells[i].size());
	//}

	//particleCount /= cellCount;

	//// const_cast<CSVLogger&>(m_csvLogger).logValue(particleCount);
	//const_cast<CSVLogger&>(m_csvLogger).logValue(float(m_triangleGrid.getMaxTrianglePerCell()));
	//const_cast<CSVLogger&>(m_csvLogger).logValue(float(m_triangleGrid.getMinTrianglePerCell()));
	//const_cast<CSVLogger&>(m_csvLogger).nextLine();

	std::vector<unsigned int> preCandidates;
	for (unsigned int i = 0; i < cells.size(); i++)
	{
		for (unsigned int j = 0; j < cells[i].size(); j++)
		{
			if (m_triangles[cells[i][j]].testCircumsphere2D(point))
			{
				preCandidates.push_back(cells[i][j]);
			}
		}
	}

	return preCandidates;
}

void ConcaveBody::parallelAddCandidates(std::pair<std::vector<unsigned int>::iterator, std::vector<unsigned int>::iterator> startEnd, std::vector<unsigned int>* candidates, const ci::Vec3f& point) const
{
	int i = 0;

	for (startEnd.first; startEnd.first != startEnd.second; startEnd.first++)
	{
		i++;

		// LOG_WARNING_STREAM(<< "i: " << i);

		if (m_triangles[*startEnd.first].checkAbove(point))
		{
			m_mutexCandidates.lock();
				candidates->push_back(*startEnd.first);
			m_mutexCandidates.unlock();
		}
	}
}