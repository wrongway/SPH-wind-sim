#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>

#include "core/Defines.h"

#include "cinder/Vector.h"

#include "core/utility/logging/CSVLogger.h"
#include "core/utility/profiling/StopWatch.h"

#include "particle/ParticleContainer.h"

class IOperation;
class IPolygonBody;
class Particle;
class Terrain;

struct SimulationParameters
{
public:
	SimulationParameters()
		: is3D(true)
		, useNearPressureKernels(true)
		, createInflow(true)
		, createGhosts(true)
		, particlesPerDimension(10)
		, simulationDomainSideLength(1.0f)
		, restDensity(1.0f)
		, supportRadiusScale(1.0f)
		, stiffness(1.0f)
		, frictionFactor(1.0f)
		, massModifier(1.0f)
		, viscosity(0.0f)
		, gravityForce(0.0f, 0.0f, 0.0f)
		, center(0.0f, 0.0f, 0.0f)
		, wind(0.0f, 0.0f, 0.0f)
		, terrain(NULL)
	{}

	bool is3D;
	bool useNearPressureKernels;
	bool createInflow;
	bool createGhosts;
	unsigned int particlesPerDimension;
	float simulationDomainSideLength;
	float restDensity;
	float supportRadiusScale;
	float stiffness;
	float frictionFactor;
	float massModifier;
	float viscosity;
	ci::Vec3f gravityForce;
	ci::Vec3f center;
	ci::Vec3f wind;
	const Terrain* terrain;
};

class Simulation
{
public:
	Simulation();
	~Simulation();

	void setup(const SimulationParameters& parameters);

	void update(const float deltaTime);

	void moveDomain(const ci::Vec3f& displacement);

	bool getIs3D() const;
	unsigned int getParticlesPerDimension() const;
	float getSimulationDomainSideLength() const;
	float getRestDensity() const;

	ParticleContainer getParticles() const;

	std::shared_ptr<IPolygonBody> getTerrain();

private:
	void setParameters(const SimulationParameters& parameters);
	void setupOperations();
	void addOperation(const std::shared_ptr<IOperation>& operation);

	float calculateParticleMassModifier() const;

	bool m_is3D;
	bool m_useNearPressureKernels;
	bool m_createInflow;
	bool m_createGhosts;

	unsigned int m_particlesPerDimension;

	float m_simulationDomainSideLength;
	float m_restDensity;
	float m_supportRadius;
	float m_stiffness;
	float m_frictionFactor;
	float m_viscosity;
	float m_massModifier;

	ci::Vec3f m_gravity;
	ci::Vec3f m_center;
	ci::Vec3f m_wind;

	ci::Vec3f m_sphMinCorner;
	ci::Vec3f m_sphMaxCorner;
	ci::Vec3f m_inflowMinCorner;
	ci::Vec3f m_inflowMaxCorner;

	ParticleContainer m_particles;
	std::vector<std::shared_ptr<IOperation>> m_operations;
	std::vector<StopWatch> m_operationStopWatches;

	const Terrain* m_terrain;
	std::shared_ptr<IPolygonBody> m_physicalTerrain;

	CSVLogger m_csvLogger;
};

#endif // SIMULATION_H