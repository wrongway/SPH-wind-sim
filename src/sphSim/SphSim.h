#ifndef SPH_SIM_H
#define SPH_SIM_H

#include "cinder/app/AppBasic.h"

#include "cinder/MayaCamUI.h"
#include "cinder/Camera.h"
#include "cinder/params/Params.h"

#include "core/graphics/MeshBufferObject.h"
#include "core/graphics/Shader.h"

#include "core/utility/logging/CSVLogger.h"

#include "rendering/IRenderer.h"
#include "rendering/TerrainRenderer.h"

#include "simulation/simulation.h"
#include "status/StatusView.h"

#include "validation/IValidator.h"

#include "validation/SPHDroneValidator.h" // for debug, think of something better once it works
#include "validation/SPHLiftRecorder.h"
#include "validation/SPHDroneWindRecorder.h"

#include "SphSimSettings.h"

/*
ffmpeg arguments:
ffmpeg -framerate 30 -start_number 150 -pattern_type sequence -i frame%d.png -c:v libx264 -pix_fmt yuv420p out.mp4
*/

class SphSim : public ci::app::AppBasic
{
public:
	void prepareSettings(ci::app::AppBasic::Settings* settings);
	void setup();

	void mouseDown(ci::app::MouseEvent event);
	void mouseDrag(ci::app::MouseEvent event);
	void keyDown(ci::app::KeyEvent event);

	void update();

	void resize();
	void draw();

	void quit();

private:
	void setupSimulation();
	void setupParameters();
	void setupCamera(const ci::Vec3f& position, const::ci::Vec3f& centerOfInterest);
	void setupRenderer();
	void pushRenderer(const std::shared_ptr<IRenderer>& renderer);
	void setupValidators();
	void pushValidator(const std::shared_ptr<IValidator>& validator);
	void setupTerrain();
	void setupParams();
	void setupStatusEntries();

	void postSetupValidators();

	void cameraTraceDrone();

	void reset();
	void resetSmokeRenderers();

	void executeRenderers();
	void drawValidators();

	void saveSettings();

	std::string getTimeString();
	void createFilePath(const std::string& filePath);
	void setupFramePath();
	void safeFrame();

	Simulation m_simulation;

	float m_lastTime;

	std::vector<std::shared_ptr<IRenderer>> m_renderers;
	TerrainRenderer m_terrainRenderer;

	std::shared_ptr<ci::MayaCamUI> m_mayaCam;
	ci::CameraPersp m_camera;

	bool m_paused;
	bool m_updateOneStep;

	std::vector<bool*> m_renderersEnabled;

	bool m_renderTerrain;
	bool m_renderTerrainWireframe;

	Terrain* m_terrain;
	MeshBufferObject m_terrainMesh;
	Shader m_phongShader;

	ci::Vec3f m_simulationCenter;
	float m_simulationDomainSize;

	ci::params::InterfaceGlRef m_params;
	SimulationParameters m_parameters;

	StatusView m_statusView;
	std::shared_ptr<StatusEntry<bool>> m_pausedStatus;
	std::shared_ptr<StatusEntry<float>> m_fpsStatus;
	std::shared_ptr<StatusEntry<int>> m_framesElapsedStatus;

	CSVLogger m_csvLogger;

	std::string m_terrainMapPath;
	SphSimSettings m_settings;

	std::string m_baseFrameFilePath;
	std::string m_currentFrameFilePath;
	unsigned int m_frameCount;
	bool m_safeFrames;
	bool m_framePathCreated;

	std::vector<std::shared_ptr<IValidator>> m_validators;
	std::vector<bool*> m_validatorsEnabled;

	std::shared_ptr<SPHDroneValidator> m_drone;
	bool m_cameraTraceDrone;

	int m_framesElapsed;
};

#endif // SPH_SIM_H