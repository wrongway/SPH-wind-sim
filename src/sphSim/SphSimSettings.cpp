#include "SphSimSettings.h"

SphSimSettings::SphSimSettings()
	: m_rootTag("simulation/")
{

}

SphSimSettings::~SphSimSettings()
{

}

void SphSimSettings::setIs3D(const bool is3D)
{
	setValue<bool>(m_rootTag + "is3D", is3D);
}

bool SphSimSettings::getIs3D()
{
	return getValue<bool>(m_rootTag + "is3D", false);
}

void SphSimSettings::setUseNearPressureKernels(const bool useNearPressureKernel)
{
	setValue<bool>(m_rootTag + "useNearPressureKernel", useNearPressureKernel);
}

bool SphSimSettings::getUseNearPressureKernels()
{
	return getValue<bool>(m_rootTag + "useNearPressureKernel", true);
}

void SphSimSettings::setCreateInflow(const bool createInflow)
{
	setValue<bool>(m_rootTag + "createInflow", createInflow);
}

bool SphSimSettings::getCreateInflow()
{
	return getValue<bool>(m_rootTag + "createInflow", true);
}

void SphSimSettings::setCreateGhosts(const bool createGhosts)
{
	setValue<bool>(m_rootTag + "createGhosts", createGhosts);
}

bool SphSimSettings::getCreateGhosts()
{
	return getValue<bool>(m_rootTag + "createGhosts", true);
}

void SphSimSettings::setParticlesPerDimension(const unsigned int particlesPerDimension)
{
	setValue<unsigned int>(m_rootTag + "particlesPerDimension", particlesPerDimension);
}

unsigned int SphSimSettings::getParticlesPerDimension()
{
	return getValue<unsigned int>(m_rootTag + "particlesPerDimension", 10);
}

void SphSimSettings::setSimulationDomainSideLength(const float sideLength)
{
	setValue<float>(m_rootTag + "simulationDomainSideLength", sideLength);
}

float SphSimSettings::getSimulationDomainSideLength()
{
	return getValue<float>(m_rootTag + "simulationDomainSideLength", 1.0f);
}

void SphSimSettings::setRestDensity(const float restDensity)
{
	setValue<float>(m_rootTag + "restDensity", restDensity);
}

float SphSimSettings::getRestDensity()
{
	return getValue<float>(m_rootTag + "restDensity", 1.0f);
}

void SphSimSettings::setSupportRadiusScale(const float supportRadiusScale)
{
	setValue<float>(m_rootTag + "supportRadiusScale", supportRadiusScale);
}

float SphSimSettings::getSupportRadiusScale()
{
	return getValue<float>(m_rootTag + "supportRadiusScale", 4.0f);
}

void SphSimSettings::setStiffness(const float stiffness)
{
	setValue<float>(m_rootTag + "stiffness", stiffness);
}

float SphSimSettings::getStiffness()
{
	return getValue<float>(m_rootTag + "stiffness", 1.0f);
}

void SphSimSettings::setFrictionFactor(const float frictionFactor)
{
	setValue<float>(m_rootTag + "frictionFactor", frictionFactor);
}

float SphSimSettings::getFrictionFactor()
{
	return getValue<float>(m_rootTag + "frictionFactor", 0.96f);
}

void SphSimSettings::setMassModifier(const float massModifier)
{
	setValue<float>(m_rootTag + "massModifier", massModifier);
}

float SphSimSettings::getMassModifier()
{
	return getValue<float>(m_rootTag + "massModifier", 1.0f);
}

void SphSimSettings::setViscosity(const float viscosity)
{
	setValue<float>(m_rootTag + "viscosity", viscosity);
}

float SphSimSettings::getViscosity()
{
	return getValue<float>(m_rootTag + "viscosity", 0.0f);
}

void SphSimSettings::setGravityForce(const ci::Vec3f& gravityForce)
{
	setValue<float>(m_rootTag + "gravityForce/x", gravityForce.x);
	setValue<float>(m_rootTag + "gravityForce/y", gravityForce.y);
	setValue<float>(m_rootTag + "gravityForce/z", gravityForce.z);
}

ci::Vec3f SphSimSettings::getGravityForce()
{
	float x = getValue<float>(m_rootTag + "gravityForce/x", 0.0f);
	float y = getValue<float>(m_rootTag + "gravityForce/y", 0.0f);
	float z = getValue<float>(m_rootTag + "gravityForce/z", 0.0f);
	return ci::Vec3f(x, y, z);
}

void SphSimSettings::setCenter(const ci::Vec3f& center)
{
	setValue<float>(m_rootTag + "center/x", center.x);
	setValue<float>(m_rootTag + "center/y", center.y);
	setValue<float>(m_rootTag + "center/z", center.z);
}

ci::Vec3f SphSimSettings::getCenter()
{
	float x = getValue<float>(m_rootTag + "center/x", 0.0f);
	float y = getValue<float>(m_rootTag + "center/y", 0.0f);
	float z = getValue<float>(m_rootTag + "center/z", 0.0f);
	return ci::Vec3f(x, y, z);
}

void SphSimSettings::setWind(const ci::Vec3f& wind)
{
	setValue<float>(m_rootTag + "wind/x", wind.x);
	setValue<float>(m_rootTag + "wind/y", wind.y);
	setValue<float>(m_rootTag + "wind/z", wind.z);
}

ci::Vec3f SphSimSettings::getWind()
{
	float x = getValue<float>(m_rootTag + "wind/x", 0.0f);
	float y = getValue<float>(m_rootTag + "wind/y", 0.0f);
	float z = getValue<float>(m_rootTag + "wind/z", 0.0f);
	return ci::Vec3f(x, y, z);
}

void SphSimSettings::setTerrainMap(const std::string& path)
{
	setValue<std::string>(m_rootTag + "terrainMapPath", path);
}

std::string SphSimSettings::getTerrainMap()
{
	return getValue<std::string>(m_rootTag + "terrainMapPath", "");
}

void SphSimSettings::setRenderTerrain(const bool renderTerrain)
{
	setValue<bool>(m_rootTag + "renderTerrain", renderTerrain);
}

bool SphSimSettings::getRenderTerrain()
{
	return getValue<bool>(m_rootTag + "renderTerrain", true);
}

void SphSimSettings::setRenderTerrainWireframe(const bool renderTerrainWireframe)
{
	setValue<bool>(m_rootTag + "renderTerrainWireframe", renderTerrainWireframe);
}

bool SphSimSettings::getRenderTerrainWireframe()
{
	return getValue<bool>(m_rootTag + "renderTerrainWireframe", false);
}

void SphSimSettings::setBoolean(const std::string& name, const bool value)
{
	setValue<bool>(m_rootTag + name, value);
}

bool SphSimSettings::getBoolean(const std::string& name)
{
	return getValue<bool>(m_rootTag + name, false);
}