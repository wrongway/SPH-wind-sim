#include "sphSim.h"

#include "core/UnitUtility.h"

#include "core/utility/logging/logging.h"
#include "core/utility/logging/ConsoleLogger.h"
#include "core/utility/logging/FileLogger.h"

#include "rendering/BorderParticleRenderer.h"
#include "rendering/InflowParticleRenderer.h"
#include "rendering/ModularParticleRenderer.h"
#include "rendering/RelativeDensityParticleRenderer.h"
#include "rendering/RestDensityParticleRenderer.h"
#include "rendering/SimpleParticleRenderer.h"
#include "rendering/SmokeRenderer2d.h"
#include "rendering/SmokeRenderer3d.h"
#include "rendering/SPHParticleRenderer.h"
#include "rendering/StreamlineRenderer.h"
#include "rendering/SurfaceRenderer.h"

#include "validation/SimpleDroneValidator.h"
#include "validation/SPHDroneValidator.h"
#include "validation/SimpleLiftDrone.h"
#include "validation/SimpleLiftRecorder.h"
#include "validation/SimpleTurbulenceDrone.h"
#include "validation/SimpleWindValidator.h"
#include "validation/SPHLiftRecorder.h"
#include "validation/SPHWindValidator.h"
#include "validation/StraightPathValidator.h"
#include "validation/VerticalPressureRecorder.h"

#include "terrain/terrain/TerrainGenerator.h"
#include "terrain/densityFunction/DensityFunctionSinCos.h"
#include "terrain/densityFunction/DensityFunctionHeightmap.h"

// ffmpeg -framerate 30 -start_number 1 -pattern_type sequence -i frame%d.png -c:v libx264 -pix_fmt yuv420p out.mp4

void SphSim::prepareSettings(ci::app::AppBasic::Settings* settings)
{
	FILE* f;
	AllocConsole();
	freopen_s(&f, "CON", "w", stdout);
}

void SphSim::setup()
{
	m_settings.setFilePath("data", "settings.ini");

	m_terrainMapPath = m_settings.getTerrainMap();

	m_simulationCenter = m_settings.getCenter();
	m_simulationDomainSize = m_settings.getSimulationDomainSideLength();

	m_terrain = NULL;

	std::shared_ptr<ConsoleLogger> consoleLogger = std::make_shared<ConsoleLogger>();
	std::shared_ptr<FileLogger> fileLogger = std::make_shared<FileLogger>();
	consoleLogger->setLogLevel(Logger::LOG_WARNINGS | Logger::LOG_ERRORS);
	LogManager::getInstance()->addLogger(consoleLogger);
	LogManager::getInstance()->addLogger(fileLogger);

	m_csvLogger.logValue("fps");
	m_csvLogger.logValue("volume");
	m_csvLogger.logValue("averageDensity");
	m_csvLogger.logValue("minDensity");
	m_csvLogger.logValue("maxDensity");
	m_csvLogger.logValue("stdDeviation");
	m_csvLogger.nextLine();

	setupRenderer();
	setupValidators();
	setupTerrain();
	setupParameters();
	setupSimulation();
	for (unsigned int i = 0; i < m_renderers.size(); i++)
	{
		if (m_renderers[i]->getName() == "SmokeRenderer2d")
		{
			IRenderer* renderer = m_renderers[i].get();
			SmokeRenderer2d* smokeRenderer = reinterpret_cast<SmokeRenderer2d*>(renderer);
			smokeRenderer->setParticleXCount(m_parameters.particlesPerDimension + (2 * m_parameters.supportRadiusScale * (int)m_parameters.createInflow));
			smokeRenderer->setParticleYCount(m_parameters.particlesPerDimension);
			smokeRenderer->setWind(meterToScreen(m_parameters.wind));
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_renderers[i]->getName() == "StreamlineRenderer")
		{
			IRenderer* renderer = m_renderers[i].get();
			StreamlineRenderer* smokeRenderer = reinterpret_cast<StreamlineRenderer*>(renderer);
			smokeRenderer->setWind(meterToScreen(m_parameters.wind));
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_renderers[i]->getName() == "SmokeRenderer3d")
		{
			IRenderer* renderer = m_renderers[i].get();
			SmokeRenderer3d* smokeRenderer = reinterpret_cast<SmokeRenderer3d*>(renderer);
			smokeRenderer->setParticleXCount(m_parameters.particlesPerDimension + (2 * m_parameters.supportRadiusScale * (int)m_parameters.createInflow));
			smokeRenderer->setParticleYCount(m_parameters.particlesPerDimension);
			smokeRenderer->setWind(meterToScreen(m_parameters.wind));
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
	}

	postSetupValidators();

	setupCamera(ci::Vec3f(0.0f, m_simulationCenter.y, 4.2f), m_simulationCenter);
	setupParams();
	setupStatusEntries();

	m_lastTime = 0.0f;

	m_paused = true;
	m_updateOneStep = false;

	m_terrainRenderer.setup("data/shader/phongShader.vert", "data/shader/phongShader.frag");

	m_baseFrameFilePath = ci::app::getAppPath().string() + "data/frames/";
	m_currentFrameFilePath = "";
	m_frameCount = 0;
	m_safeFrames = false;
	m_framePathCreated = false;

	m_framesElapsed = 0;
}

void SphSim::mouseDown(ci::app::MouseEvent event)
{
	if (m_cameraTraceDrone == false)
	{
		m_mayaCam->mouseDown(event.getPos());
	}
}

void SphSim::mouseDrag(ci::app::MouseEvent event)
{
	if (m_cameraTraceDrone == false)
	{
		m_mayaCam->mouseDrag(event.getPos(), event.isLeftDown(), event.isMiddleDown(), event.isRightDown());
	}
}

void SphSim::keyDown(ci::app::KeyEvent event)
{
	bool moveDomain = false;
	ci::Vec3f displacement(0.0f, 0.0f, 0.0f);
	float speed = 0.1f;

	if (event.getChar() == 'p')
	{
		m_paused = !m_paused;
		m_pausedStatus->setValue(m_paused);
	}
	else if (event.getChar() == '�')
	{
		if (m_paused == true)
		{
			m_updateOneStep = true;
		}
	}
	else if (event.getChar() == 'n')
	{
		reset();
	}
	else if (event.getCode() == ci::app::KeyEvent::KEY_LEFT)
	{
		displacement += ci::Vec3f(-1.0f, 0.0f, 0.0f) * speed;
		moveDomain = true;
	}
	else if (event.getCode() == ci::app::KeyEvent::KEY_RIGHT)
	{
		displacement += ci::Vec3f(1.0f, 0.0f, 0.0f) * speed;
		moveDomain = true;
	}
	else if (event.getCode() == ci::app::KeyEvent::KEY_UP)
	{
		displacement += ci::Vec3f(0.0f, 0.0f, 1.0f) * speed;
		moveDomain = true;
	}
	else if (event.getCode() == ci::app::KeyEvent::KEY_DOWN)
	{
		displacement += ci::Vec3f(0.0f, 0.0f, -1.0f) * speed;
		moveDomain = true;
	}

	if (moveDomain)
	{
		displacement.normalize();
		displacement *= speed;
		m_simulation.moveDomain(screenToMeter(displacement));
		m_simulationCenter += displacement;

		for (unsigned int i = 0; i < m_renderers.size(); i++)
		{
			m_renderers[i]->move(displacement);
		}

		for (unsigned int i = 0; i < m_validators.size(); i++)
		{
			m_validators[i]->move(screenToMeter(displacement));
		}
	}
}

void SphSim::update()
{
	float currentTime = ci::app::getElapsedSeconds();
	float deltaTime = currentTime - m_lastTime;
	m_lastTime = currentTime;

	if ((m_paused == false || m_updateOneStep == true))
	{
		m_framesElapsed++;

		m_simulation.update(1.0f / 30.0f);
		
		if (m_updateOneStep == true)
		{
			m_updateOneStep = false;
		}

		if (m_framePathCreated == false)
		{
			setupFramePath();
			m_framePathCreated = true;
		}

		if (m_safeFrames)
		{
			m_frameCount++;
			safeFrame();
		}

		for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
		{
			if (*m_renderersEnabled[i] == true)
			{
				m_renderers[i]->update(m_simulation.getParticles(), 1.0f / 30.0f);
			}
		}

		if (m_framesElapsed > 0)
		{
			for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
			{
				if (*m_validatorsEnabled[i] == true)
				{
					m_validators[i]->update(m_simulation.getParticles(), 1.0f / 30.0f);
				}
			}



			if (m_drone != NULL)
			{
				ci::Vec3f displacement = m_drone->getDisplacement();

				if (displacement.length() > 0.00001f)
				{
					m_simulation.moveDomain(displacement);
					m_simulationCenter += meterToScreen(displacement);

					for (unsigned int i = 0; i < m_renderers.size(); i++)
					{
						m_renderers[i]->move(meterToScreen(displacement));
					}
				}
			}
		}
	}

	if (m_cameraTraceDrone)
	{
		cameraTraceDrone();
	}

	float fps = ci::app::App::getAverageFps();
	m_fpsStatus->setValue(fps);

	m_framesElapsedStatus->setValue(m_framesElapsed);

	ci::Vec3f min = meterToScreen(m_simulation.getParticles().getSphBoundingBoxMinCorner());
	ci::Vec3f max = meterToScreen(m_simulation.getParticles().getSphBoundingBoxMaxCorner());
	ci::Vec3f size = max - min;

	float volume = size.x * size.y * size.z;

	m_csvLogger.logValue(size.x);
	m_csvLogger.logValue(size.y);
	m_csvLogger.logValue(size.z);
	m_csvLogger.logValue(volume);
	m_csvLogger.logValue(fps);
	m_csvLogger.logValue(m_simulation.getParticles().getAverageDensity());
	m_csvLogger.logValue(m_simulation.getParticles().getMinDensity());
	m_csvLogger.logValue(m_simulation.getParticles().getMaxDensity());
	m_csvLogger.logValue(m_simulation.getParticles().getStandardDeviationDensity());
	m_csvLogger.nextLine();
}

void SphSim::resize()
{
	ci::CameraPersp cam = m_mayaCam->getCamera();
	cam.setAspectRatio(ci::app::getWindowAspectRatio());
	m_mayaCam->setCurrentCam(cam);
}

void SphSim::draw()
{
	ci::Matrix44f mat_modelview = m_mayaCam->getCamera().getModelViewMatrix();
	ci::Matrix44f mat_model_view_proj = m_mayaCam->getCamera().getProjectionMatrix() * mat_modelview;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	ci::gl::clear(ci::ColorAf(0.3f, 0.6f, 0.8f, 1.0f));

	ci::gl::pushMatrices();
		ci::gl::setMatrices(m_mayaCam->getCamera());

		ci::gl::setViewport(getWindowBounds());

		if (m_renderTerrain)
		{
			m_terrainRenderer.render(m_terrain, m_mayaCam->getCamera(), m_renderTerrainWireframe);
		}
		
		executeRenderers();
		drawValidators();

		ci::gl::color(ci::ColorAf(1.0f, 1.0f, 1.0f, 1.0f));
		ci::gl::drawStrokedCube(m_simulationCenter, meterToScreen(ci::Vec3f(m_simulationDomainSize, m_simulationDomainSize, m_simulationDomainSize)));

		ci::Vec3f min = meterToScreen(m_simulation.getParticles().getSphBoundingBoxMinCorner());
		ci::Vec3f max = meterToScreen(m_simulation.getParticles().getSphBoundingBoxMaxCorner());

		ci::Vec3f size = max - min;
		ci::Vec3f center = (min + max) * 0.5f;

		ci::gl::color(ci::ColorAf(0.0f, 1.0f, 0.0f, 1.0f));
		ci::gl::drawStrokedCube(center, size);
	ci::gl::popMatrices();

	m_params->draw();

	m_statusView.draw();
}

void SphSim::quit()
{
	for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
	{
		delete m_renderersEnabled[i];
	}

	for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
	{
		delete m_validatorsEnabled[i];
	}

	if (m_terrain != NULL)
	{
		delete m_terrain;
	}
}

void SphSim::setupSimulation()
{
	m_framePathCreated = false;
	m_frameCount = 0;
	m_simulationCenter = m_parameters.center;
	m_simulation.setup(m_parameters);
}

void SphSim::setupParameters()
{
	m_parameters.is3D = m_settings.getIs3D();
	m_parameters.useNearPressureKernels = m_settings.getUseNearPressureKernels();
	m_parameters.createInflow = m_settings.getCreateInflow();
	m_parameters.createGhosts = m_settings.getCreateGhosts();
	m_parameters.particlesPerDimension = m_settings.getParticlesPerDimension();
	m_parameters.simulationDomainSideLength = m_simulationDomainSize;
	m_parameters.restDensity = m_settings.getRestDensity();
	m_parameters.supportRadiusScale = m_settings.getSupportRadiusScale();
	m_parameters.stiffness = m_settings.getStiffness();
	m_parameters.frictionFactor = m_settings.getFrictionFactor();
	m_parameters.massModifier = m_settings.getMassModifier();
	m_parameters.viscosity = m_settings.getViscosity();
	m_parameters.gravityForce = m_settings.getGravityForce();
	m_parameters.center = m_simulationCenter;
	m_parameters.wind = m_settings.getWind();
	m_parameters.terrain = m_terrain;
	
	for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
	{
		*m_renderersEnabled[i] = m_settings.getBoolean(m_renderers[i]->getName());
	}

	for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
	{
		*m_validatorsEnabled[i] = m_settings.getBoolean(m_validators[i]->getName());
	}

	m_renderTerrain = m_settings.getRenderTerrain();
	m_renderTerrainWireframe = m_settings.getRenderTerrainWireframe();

	m_cameraTraceDrone = m_settings.getBoolean("cameraTraceDrone");
}

void SphSim::setupCamera(const ci::Vec3f& position, const::ci::Vec3f& centerOfInterest)
{
	m_camera.setEyePoint(position);
	m_camera.setCenterOfInterestPoint(centerOfInterest);
	m_camera.setPerspective(45.0f, ci::app::getWindowAspectRatio(), 0.1f, 10000.0f);
	m_mayaCam = std::make_shared<ci::MayaCamUI>(),
	m_mayaCam->setCurrentCam(m_camera);
}

void SphSim::setupRenderer()
{
	std::shared_ptr<SurfaceRenderer> surfaceRenderer = std::make_shared<SurfaceRenderer>();
	surfaceRenderer->loadShader("data/shader/phongShader.vert", "data/shader/phongShader.frag");
	surfaceRenderer->setDomainSize(1.0f);
	surfaceRenderer->setCellSize(1.0f / 10.0f);
	surfaceRenderer->setKernelSupportRadius(1.0f / 12.0f * 8.0f);
	surfaceRenderer->setRestDensity(1.0f);

	std::shared_ptr<IRenderer> particleRenderer = std::make_shared<SPHParticleRenderer>();
	std::shared_ptr<IRenderer> inflowRenderer = std::make_shared<InflowParticleRenderer>();
	std::shared_ptr<IRenderer> borderRenderer = std::make_shared<BorderParticleRenderer>();
	std::shared_ptr<SmokeRenderer2d> smokeRenderer = std::make_shared<SmokeRenderer2d>();
	smokeRenderer->setShader("data/shader/smokeShader.vert", "data/shader/smokeShader.frag");
	smokeRenderer->setStreakWidth(3);
	smokeRenderer->setStreakSpacing(2);
	smokeRenderer->setParticleYCount(m_parameters.particlesPerDimension);
	smokeRenderer->setParticleXCount(m_parameters.particlesPerDimension);
	std::shared_ptr<SmokeRenderer3d> smokeRenderer3d = std::make_shared<SmokeRenderer3d>();
	smokeRenderer3d->setShader("data/shader/smokeShader.vert", "data/shader/smokeShader.frag");
	smokeRenderer3d->setStreakSpacing(2);
	smokeRenderer3d->setParticleYCount(m_parameters.particlesPerDimension);
	smokeRenderer3d->setParticleXCount(m_parameters.particlesPerDimension);

	std::shared_ptr<StreamlineRenderer> streamlineRenderer = std::make_shared<StreamlineRenderer>();

	pushRenderer(particleRenderer);
	pushRenderer(inflowRenderer);
	pushRenderer(borderRenderer);
	pushRenderer(smokeRenderer);
	pushRenderer(smokeRenderer3d);
	pushRenderer(surfaceRenderer);
	pushRenderer(streamlineRenderer);
}

void SphSim::pushRenderer(const std::shared_ptr<IRenderer>& renderer)
{
	m_renderers.push_back(renderer);
	m_renderersEnabled.push_back(new bool(false));
}

void SphSim::setupValidators()
{
	int validatorId = 0;
	float speed = 40.0f;
	ci::Vec3f velocity = ci::Vec3f(0.0f, 0.0f, 1.0f);
	velocity.normalize();
	velocity *= speed;

	std::shared_ptr<SimpleWindValidator> simpleWindValidator = std::make_shared<SimpleWindValidator>(validatorId++);
	simpleWindValidator->setVelocity(velocity);

	std::shared_ptr<SPHWindValidator> sphWindValidator = std::make_shared<SPHWindValidator>(validatorId++);
	sphWindValidator->setVelocity(velocity);

	std::shared_ptr<StraightPathValidator> straightPathValidator = std::make_shared<StraightPathValidator>(validatorId++);
	straightPathValidator->setVelocity(velocity);

	for (unsigned int i = 0; i < 5; i++)
	{
		LOG_WARNING_STREAM(<< "drone created");
		
		std::shared_ptr<SPHDroneValidator> sphDroneValidator = std::make_shared<SPHDroneValidator>(validatorId++);
		sphDroneValidator->setVelocity(velocity);
		pushValidator(sphDroneValidator);

		m_drone = sphDroneValidator;

		
		if (i == 2)
		{
			 m_drone = sphDroneValidator;
		}
	}

	std::shared_ptr<SimpleDroneValidator> simpleDroneValidator = std::make_shared<SimpleDroneValidator>(validatorId++);
	simpleDroneValidator->setVelocity(velocity);

	for (unsigned int i = 0; i < 5; i++)
	{
		std::shared_ptr<SimpleLiftDrone> simpleLiftDrone = std::make_shared<SimpleLiftDrone>(validatorId++);
		simpleLiftDrone->setVelocity(velocity);
		pushValidator(simpleLiftDrone);
	}

	std::shared_ptr<SimpleTurbulenceDrone> simpleTurbulenceDrone = std::make_shared<SimpleTurbulenceDrone>(validatorId++);
	simpleTurbulenceDrone->setVelocity(velocity);

	std::shared_ptr<SimpleLiftRecorder> simpleLiftRecorder = std::make_shared<SimpleLiftRecorder>(validatorId++);
	simpleLiftRecorder->setVelocity(velocity);

	std::shared_ptr<SPHLiftRecorder> sphLiftRecorder = std::make_shared<SPHLiftRecorder>(validatorId++);
	sphLiftRecorder->setVelocity(velocity);

	std::shared_ptr<VerticalPressureRecorder> pressureRecorder = std::make_shared<VerticalPressureRecorder>(validatorId++);

	pushValidator(simpleLiftRecorder);

	pushValidator(sphLiftRecorder);

	pushValidator(straightPathValidator);
	pushValidator(sphWindValidator);
	pushValidator(simpleWindValidator);
	
	pushValidator(simpleDroneValidator);
	
	pushValidator(simpleTurbulenceDrone);

	pushValidator(pressureRecorder);
}

void SphSim::pushValidator(const std::shared_ptr<IValidator>& validator)
{
	m_validators.push_back(validator);
	m_validatorsEnabled.push_back(new bool(false));
}

void SphSim::setupTerrain()
{
	if (m_terrainMapPath.length() <= 0)
	{
		m_terrainMapPath = "data/heightmaps/flat_01.png";
	}

	float resolutionMultiplier = 4.0f;

#ifdef _DEBUG
	resolutionMultiplier = 1.0f;
#endif

	float terrainSize = 54.0f;
	float cellCount = 54.0f * resolutionMultiplier;

	LOG_WARNING_STREAM(<< "cube count: " << cellCount);

	DensityFunctionSinCos densitySinCos;
	DensityFunctionHeightMap densityHeightMap;
	densityHeightMap.setTerrainSize(terrainSize);
	densityHeightMap.loadHeightMap(m_terrainMapPath);
	densityHeightMap.setAltitudeScale(6.0f);

	TerrainGenerator generator;
	m_terrain = generator.generateTerrain(densityHeightMap, terrainSize, terrainSize / cellCount);
	m_terrain->setCubesPerDimension(cellCount);

	m_terrainMesh.generateBuffers();
	m_terrainMesh.bindVertices(m_terrain->getVertices());
	m_terrainMesh.bindIndices(m_terrain->getIndices());
	m_terrainMesh.bindNormals(m_terrain->getNormals());
	m_terrainMesh.bindTexCoords(m_terrain->getTextureCoordinates());

	LOG_INFO_STREAM(<< "done generating terrain");
}

void SphSim::setupParams()
{
	m_params = ci::params::InterfaceGl::create(ci::app::getWindow(), "Global", ci::app::toPixels(ci::Vec2i(300, 400)));
	m_params->addText("Simulation Parameters");
	m_params->addParam("3D", &m_parameters.is3D);
	m_params->addParam("Near Pressure Kernels", &m_parameters.useNearPressureKernels);
	m_params->addParam("Inflow", &m_parameters.createInflow);
	m_params->addParam("Ghosts", &m_parameters.createGhosts);
	m_params->addParam("Particles per dim", &m_parameters.particlesPerDimension);
	m_params->addParam("Side Length", &m_parameters.simulationDomainSideLength);
	m_params->addParam("Rest Density", &m_parameters.restDensity);
	m_params->addParam("Support Radius", &m_parameters.supportRadiusScale);
	m_params->addParam("Stiffness", &m_parameters.stiffness);
	m_params->addParam("Friction Coefficient", &m_parameters.frictionFactor);
	m_params->addParam("Mass modifier", &m_parameters.massModifier);
	m_params->addParam("Viscosity", &m_parameters.viscosity);
	m_params->addParam("Gravity", &m_parameters.gravityForce);
	m_params->addParam("Center", &m_parameters.center);
	m_params->addParam("Wind", &m_parameters.wind);
	m_params->addSeparator();
	m_params->addParam("Terrain Map", &m_terrainMapPath);
	m_params->addSeparator();
	
	for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
	{
		m_params->addParam(m_renderers[i]->getName(), m_renderersEnabled[i]);
	}

	m_params->addParam("Render Terrain", &m_renderTerrain);
	m_params->addParam("Wireframe", &m_renderTerrainWireframe);
	m_params->addSeparator();

	for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
	{
		m_params->addParam(m_validators[i]->getName(), m_validatorsEnabled[i]);
	}

	m_params->addSeparator();
	m_params->addParam("Camera Trace Drone", &m_cameraTraceDrone);
	m_params->addSeparator();
	m_params->addParam("Safe Frames", &m_safeFrames);
	m_params->addSeparator();
	m_params->addButton("Save Settings", std::bind(&SphSim::saveSettings, this));
}

void SphSim::setupStatusEntries()
{
	m_pausedStatus = std::make_shared < StatusEntry<bool>>();
	m_pausedStatus->setName("Paused");
	m_pausedStatus->setValue(true);
	m_statusView.pushEntry(m_pausedStatus);


	m_fpsStatus = std::make_shared<StatusEntry<float>>();
	m_fpsStatus->setName("FPS");
	m_fpsStatus->setValue(0.0f);
	m_statusView.pushEntry(m_fpsStatus);


	m_framesElapsedStatus = std::make_shared<StatusEntry<int>>();
	m_framesElapsedStatus->setName("Elapsed Frames");
	m_framesElapsedStatus->setValue(0);
	m_statusView.pushEntry(m_framesElapsedStatus);
}

void SphSim::postSetupValidators()
{
	ci::Vec3f validatorPosition = (m_simulation.getParticles().getNormMinCorner() + m_simulation.getParticles().getNormMaxCorner()) * 0.5f;
	ci::Vec3f dronePosition = validatorPosition;
	validatorPosition.z = m_simulation.getParticles().getNormMinCorner().z;

	float targetAltitude = 0.0f;
	float sphTargetAlt = 0.0f;
	float simpleLiftAlt = 0.0f;

	std::shared_ptr<IPolygonBody> terrain = m_simulation.getTerrain();
	if (terrain != NULL)
	{
		float altitude = 0.0f;
		if (terrain->getAltitudeAtPosition(validatorPosition, altitude))
		{
			targetAltitude = validatorPosition.y - m_simulation.getParticles().getNormMinCorner().y;

			float y = targetAltitude + altitude;
			sphTargetAlt = y - 000;
			simpleLiftAlt = y - 000;

			validatorPosition.y = y;
			dronePosition.y = y;
		}
	}

	for (unsigned int i = 0; i < m_validators.size(); i++)
	{
		if (m_validators[i]->getName() == "StraightPathValidator")
		{
			IValidator* validator = m_validators[i].get();
			StraightPathValidator* straightPathValidator = reinterpret_cast<StraightPathValidator*>(validator);
			straightPathValidator->setPosition(validatorPosition);
			straightPathValidator->setTargetAltitude(targetAltitude);
			straightPathValidator->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SimpleWindValidator")
		{
			IValidator* validator = m_validators[i].get();
			SimpleWindValidator* simpleWindValidator = reinterpret_cast<SimpleWindValidator*>(validator);
			simpleWindValidator->setPosition(validatorPosition);
			simpleWindValidator->setWind(m_parameters.wind);
			simpleWindValidator->setTargetAltitude(targetAltitude);
			simpleWindValidator->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SPHWindValidator")
		{
			IValidator* validator = m_validators[i].get();
			SPHWindValidator* sphWindValidator = reinterpret_cast<SPHWindValidator*>(validator);
			sphWindValidator->setPosition(validatorPosition);
			sphWindValidator->setWind(m_parameters.wind);
			sphWindValidator->setTargetAltitude(targetAltitude);
			sphWindValidator->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SPHDroneValidator")
		{
			IValidator* validator = m_validators[i].get();
			SPHDroneValidator* sphDroneValidator = reinterpret_cast<SPHDroneValidator*>(validator);
			sphDroneValidator->setPosition(ci::Vec3f(dronePosition.x, sphTargetAlt, dronePosition.z));
			sphDroneValidator->setWind(m_parameters.wind);
			sphDroneValidator->setTargetAltitude(sphTargetAlt);
			sphTargetAlt += 100.0f;
			sphDroneValidator->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SimpleDroneValidator")
		{
			IValidator* validator = m_validators[i].get();
			SimpleDroneValidator* simpleDroneValidator = reinterpret_cast<SimpleDroneValidator*>(validator);
			simpleDroneValidator->setPosition(dronePosition);
			simpleDroneValidator->setWind(m_parameters.wind);
			simpleDroneValidator->setTargetAltitude(targetAltitude);
			simpleDroneValidator->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SimpleLiftDrone")
		{
			IValidator* validator = m_validators[i].get();
			SimpleLiftDrone* simpleLiftDrone = reinterpret_cast<SimpleLiftDrone*>(validator);
			simpleLiftDrone->setPosition(ci::Vec3f(dronePosition.x, simpleLiftAlt, dronePosition.z));
			simpleLiftDrone->setWind(m_parameters.wind);
			simpleLiftDrone->setTargetAltitude(simpleLiftAlt);
			simpleLiftAlt += 100.0f;
			simpleLiftDrone->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SimpleTurbulenceDrone")
		{
			IValidator* validator = m_validators[i].get();
			SimpleLiftDrone* simpleLiftDrone = reinterpret_cast<SimpleLiftDrone*>(validator);
			simpleLiftDrone->setPosition(dronePosition);
			simpleLiftDrone->setWind(m_parameters.wind);
			simpleLiftDrone->setTargetAltitude(targetAltitude);
			simpleLiftDrone->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SPHLiftRecorder")
		{
			IValidator* validator = m_validators[i].get();
			SPHLiftRecorder* sphLiftRecorder = reinterpret_cast<SPHLiftRecorder*>(validator);
			sphLiftRecorder->setPosition(dronePosition);
			sphLiftRecorder->setWind(m_parameters.wind);
			sphLiftRecorder->setTargetAltitude(targetAltitude);
			sphLiftRecorder->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SimpleLiftRecorder")
		{
			IValidator* validator = m_validators[i].get();
			SimpleLiftRecorder* simpleLiftRecorder = reinterpret_cast<SimpleLiftRecorder*>(validator);
			simpleLiftRecorder->setPosition(dronePosition);
			simpleLiftRecorder->setWind(m_parameters.wind);
			simpleLiftRecorder->setTargetAltitude(targetAltitude);
			simpleLiftRecorder->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_validators[i]->getName() == "SPHDroneWindRecorder")
		{
			IValidator* validator = m_validators[i].get();
			SPHDroneWindRecorder* windRecorder = reinterpret_cast<SPHDroneWindRecorder*>(validator);
			windRecorder->setPosition(dronePosition);
			windRecorder->setWind(m_parameters.wind);
			windRecorder->setTargetAltitude(targetAltitude);
			windRecorder->setTerrain(m_simulation.getTerrain().get());
		}
	}
}

void SphSim::cameraTraceDrone()
{
	ci::CameraPersp cam = m_mayaCam->getCamera();

	ci::Vec3f dronePos = meterToScreen(m_drone->getPosition());
	ci::Vec3f droneVel = meterToScreen(m_drone->getVelocity());

	ci::Vec3f toCamera = droneVel.cross(ci::Vec3f(0.0f, 1.0f, 0.0f));

	cam.lookAt(dronePos);

	ci::Vec3f newEyePoint = dronePos + ci::Vec3f(0.0f, 3.0f, -1.0f);

	cam.setEyePoint(newEyePoint);

	m_mayaCam->setCurrentCam(cam);
}

void SphSim::reset()
{
	setupSimulation();

	resetSmokeRenderers();
}

void SphSim::resetSmokeRenderers()
{
	for (unsigned int i = 0; i < m_renderers.size(); i++)
	{
		if (m_renderers[i]->getName() == "SmokeRenderer2d")
		{
			m_renderers[i] = std::make_shared<SmokeRenderer2d>();
			IRenderer* renderer = m_renderers[i].get();
			SmokeRenderer2d* smokeRenderer = reinterpret_cast<SmokeRenderer2d*>(renderer);
			smokeRenderer->setShader("data/shader/smokeShader.vert", "data/shader/smokeShader.frag");
			smokeRenderer->setStreakWidth(3);
			smokeRenderer->setStreakSpacing(2);
			smokeRenderer->setParticleXCount(m_parameters.particlesPerDimension + (2 * m_parameters.supportRadiusScale * (int)m_parameters.createInflow));
			smokeRenderer->setParticleYCount(m_parameters.particlesPerDimension);
			smokeRenderer->setWind(m_parameters.wind);
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_renderers[i]->getName() == "StreamlineRenderer")
		{
			m_renderers[i] = std::make_shared<StreamlineRenderer>();
			IRenderer* renderer = m_renderers[i].get();
			StreamlineRenderer* smokeRenderer = reinterpret_cast<StreamlineRenderer*>(renderer);
			smokeRenderer->setWind(m_parameters.wind);
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
		else if (m_renderers[i]->getName() == "SmokeRenderer3d")
		{
			m_renderers[i] = std::make_shared<SmokeRenderer3d>();
			IRenderer* renderer = m_renderers[i].get();
			SmokeRenderer3d* smokeRenderer = reinterpret_cast<SmokeRenderer3d*>(renderer);
			smokeRenderer->setShader("data/shader/smokeShader.vert", "data/shader/smokeShader.frag");
			smokeRenderer->setStreakSpacing(2);
			smokeRenderer->setParticleXCount(m_parameters.particlesPerDimension + (2 * m_parameters.supportRadiusScale * (int)m_parameters.createInflow));
			smokeRenderer->setParticleYCount(m_parameters.particlesPerDimension);
			smokeRenderer->setWind(m_parameters.wind);
			smokeRenderer->setTerrain(m_simulation.getTerrain().get());
		}
	}
}

void SphSim::executeRenderers()
{
	for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
	{
		bool render = *m_renderersEnabled[i];

		if (render)
		{
			m_renderers[i]->render(m_simulation.getParticles(), m_mayaCam->getCamera());
		}
	}
}

void SphSim::drawValidators()
{
	for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
	{
		bool validate = *m_validatorsEnabled[i];

		if (validate)
		{
			m_validators[i]->render(m_simulation.getParticles(), m_mayaCam->getCamera());
		}
	}
}

void SphSim::saveSettings()
{
	m_settings.setIs3D(m_parameters.is3D);
	m_settings.setUseNearPressureKernels(m_parameters.useNearPressureKernels);
	m_settings.setCreateInflow(m_parameters.createInflow);
	m_settings.setCreateGhosts(m_parameters.createGhosts);
	m_settings.setParticlesPerDimension(m_parameters.particlesPerDimension);
	m_settings.setSimulationDomainSideLength(m_parameters.simulationDomainSideLength);
	m_settings.setRestDensity(m_parameters.restDensity);
	m_settings.setSupportRadiusScale(m_parameters.supportRadiusScale);
	m_settings.setStiffness(m_parameters.stiffness);
	m_settings.setFrictionFactor(m_parameters.frictionFactor);
	m_settings.setMassModifier(m_parameters.massModifier);
	m_settings.setViscosity(m_parameters.viscosity);
	m_settings.setGravityForce(m_parameters.gravityForce);
	m_settings.setCenter(m_parameters.center);
	m_settings.setWind(m_parameters.wind);
	m_settings.setTerrainMap(m_terrainMapPath);
	
	for (unsigned int i = 0; i < m_renderersEnabled.size(); i++)
	{
		bool enabled = *m_renderersEnabled[i];
		m_settings.setBoolean(m_renderers[i]->getName(), enabled);
	}

	m_settings.setRenderTerrain(m_renderTerrain);
	m_settings.setRenderTerrainWireframe(m_renderTerrainWireframe);

	for (unsigned int i = 0; i < m_validatorsEnabled.size(); i++)
	{
		bool enabled = *m_validatorsEnabled[i];
		m_settings.setBoolean(m_validators[i]->getName(), enabled);
	}

	m_settings.setBoolean("cameraTraceDrone", m_cameraTraceDrone);

	m_settings.saveSettings();
}

std::string SphSim::getTimeString()
{
	time_t time;
	std::time(&time);
	tm localTime = *std::localtime(&time);

	std::stringstream timeString;
	timeString << localTime.tm_mon << "-" << localTime.tm_mday << "_";
	timeString << localTime.tm_hour << "-" << localTime.tm_min << "-" << localTime.tm_sec;

	return timeString.str();
}

void SphSim::createFilePath(const std::string& filePath)
{
	ci::createDirectories(filePath, true);
}

void SphSim::setupFramePath()
{
	m_currentFrameFilePath = m_baseFrameFilePath + getTimeString();
	createFilePath(m_currentFrameFilePath);
}

void SphSim::safeFrame()
{
	std::stringstream fileName;
	fileName << m_currentFrameFilePath << "/frame" << m_frameCount << ".png";
	ci::writeImage(fileName.str(), ci::app::copyWindowSurface());
}

CINDER_APP_BASIC(SphSim, ci::app::RendererGl)