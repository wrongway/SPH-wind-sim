#include "StatusView.h"

#include "cinder/gl/gl.h"

StatusView::StatusView()
	: m_stringEntries()
	, m_floatEntries()
	, m_intEntries()
	, m_drawPosition(0.0f, 0.0f)
	, m_textColor(0.0f, 0.0f, 0.0f)
{

}

StatusView::~StatusView()
{

}

void StatusView::draw()
{
	ci::Vec2f currentPosition = m_drawPosition;
	float offset = 12.0f;

	for (unsigned int i = 0; i < m_stringEntries.size(); i++)
	{
		ci::gl::drawString(m_stringEntries[i]->getEntryString(), currentPosition, m_textColor);
		currentPosition.y += offset;
	}

	for (unsigned int i = 0; i < m_floatEntries.size(); i++)
	{
		ci::gl::drawString(m_floatEntries[i]->getEntryString(), currentPosition, m_textColor);
		currentPosition.y += offset;
	}

	for (unsigned int i = 0; i < m_intEntries.size(); i++)
	{
		ci::gl::drawString(m_intEntries[i]->getEntryString(), currentPosition, m_textColor);
		currentPosition.y += offset;
	}

	for (unsigned int i = 0; i < m_boolEntries.size(); i++)
	{
		ci::gl::drawString(m_boolEntries[i]->getEntryString(), currentPosition, m_textColor);
		currentPosition.y += offset;
	}
}

void StatusView::pushEntry(const std::shared_ptr<StatusEntry<std::string>>& entry)
{
	m_stringEntries.push_back(entry);
}

void StatusView::pushEntry(const std::shared_ptr<StatusEntry<float>>& entry)
{
	m_floatEntries.push_back(entry);
}

void StatusView::pushEntry(const std::shared_ptr<StatusEntry<int>>& entry)
{
	m_intEntries.push_back(entry);
}

void StatusView::pushEntry(const std::shared_ptr<StatusEntry<bool>>& entry)
{
	m_boolEntries.push_back(entry);
}

void StatusView::clearEntries()
{
	m_stringEntries.clear();
	m_floatEntries.clear();
	m_intEntries.clear();
	m_boolEntries.clear();
}

void StatusView::setDrawPosition(const ci::Vec2f& drawPosition)
{
	m_drawPosition = drawPosition;
}

ci::Vec2f StatusView::getDrawPosition() const
{
	return m_drawPosition;
}

void StatusView::setTextColor(const ci::Color& color)
{
	m_textColor = color;
}

ci::Color StatusView::getTextColor() const
{
	return m_textColor;
}