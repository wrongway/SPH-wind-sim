#ifndef STATUS_ENTRY_H
#define STATUS_ENTRY_H

#include <sstream>
#include <string>

template<class T>
class StatusEntry
{
public:
	StatusEntry();
	~StatusEntry();

	void setName(const std::string& name);
	std::string getName() const;

	void setValue(const T& value);
	T getValue() const;

	std::string getEntryString() const;

private:
	std::string m_name;
	T m_value;
};

template<class T>
StatusEntry<T>::StatusEntry()
{

}

template<class T>
StatusEntry<T>::~StatusEntry()
{

}

template<class T>
void StatusEntry<T>::setName(const std::string& name)
{
	m_name = name;
}

template<class T>
std::string StatusEntry<T>::getName() const
{
	return m_name;
}

template<class T>
void StatusEntry<T>::setValue(const T& value)
{
	m_value = value;
}

template<class T>
T StatusEntry<T>::getValue() const
{
	return m_value;
}

template<class T>
std::string StatusEntry<T>::getEntryString() const
{
	std::stringstream message;
	message << m_name << ": " << m_value;
	return message.str();
}

#endif // STATUS_ENTRY_H