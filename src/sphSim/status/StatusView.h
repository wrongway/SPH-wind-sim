#ifndef STATUS_VIEW_H
#define STATUS_VIEW_H

#include <vector>
#include <memory>

#include "cinder/Color.h"
#include "cinder/Vector.h"

#include "StatusEntry.h"

class StatusView
{
public:
	StatusView();
	~StatusView();

	void draw();

	void pushEntry(const std::shared_ptr<StatusEntry<std::string>>& entry);
	void pushEntry(const std::shared_ptr<StatusEntry<float>>& entry);
	void pushEntry(const std::shared_ptr<StatusEntry<int>>& entry);
	void pushEntry(const std::shared_ptr<StatusEntry<bool>>& entry);

	void clearEntries();

	void setDrawPosition(const ci::Vec2f& drawPosition);
	ci::Vec2f getDrawPosition() const;

	void setTextColor(const ci::Color& color);
	ci::Color getTextColor() const;

private:
	std::vector<std::shared_ptr<StatusEntry<std::string>>> m_stringEntries;
	std::vector<std::shared_ptr<StatusEntry<float>>> m_floatEntries;
	std::vector<std::shared_ptr<StatusEntry<int>>> m_intEntries;
	std::vector<std::shared_ptr<StatusEntry<bool>>> m_boolEntries;

	ci::Vec2f m_drawPosition;

	ci::Color m_textColor;
};

#endif // STATUS_VIEW_H