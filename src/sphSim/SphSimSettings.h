#ifndef SPH_SIM_SETTINGS_H
#define SPH_SIM_SETTINGS_H

#include "cinder/Vector.h"

#include "core/utility/settings/BaseSettings.h"

class SphSimSettings : public BaseSettings
{
public:
	SphSimSettings();
	virtual ~SphSimSettings();

	void setIs3D(const bool is3D);
	bool getIs3D();

	void setUseNearPressureKernels(const bool useNearPressureKernel);
	bool getUseNearPressureKernels();

	void setCreateInflow(const bool createInflow);
	bool getCreateInflow();

	void setCreateGhosts(const bool createGhosts);
	bool getCreateGhosts();

	void setParticlesPerDimension(const unsigned int particlesPerDimension);
	unsigned int getParticlesPerDimension();

	void setSimulationDomainSideLength(const float sideLength);
	float getSimulationDomainSideLength();

	void setRestDensity(const float restDensity);
	float getRestDensity();

	void setSupportRadiusScale(const float supportRadiusScale);
	float getSupportRadiusScale();

	void setStiffness(const float stiffness);
	float getStiffness();

	void setFrictionFactor(const float frictionFactor);
	float getFrictionFactor();

	void setMassModifier(const float massModifier);
	float getMassModifier();

	void setViscosity(const float viscosity);
	float getViscosity();

	void setGravityForce(const ci::Vec3f& gravityForce);
	ci::Vec3f getGravityForce();

	void setCenter(const ci::Vec3f& center);
	ci::Vec3f getCenter();

	void setWind(const ci::Vec3f& wind);
	ci::Vec3f getWind();

	void setTerrainMap(const std::string& path);
	std::string getTerrainMap();

	void setRenderTerrain(const bool renderTerrain);
	bool getRenderTerrain();

	void setRenderTerrainWireframe(const bool renderTerrainWireframe);
	bool getRenderTerrainWireframe();

	void setBoolean(const std::string& name, const bool value);
	bool getBoolean(const std::string& name);

private:
	const std::string m_rootTag;
};

#endif // SPH_SIM_SETTINGS_H