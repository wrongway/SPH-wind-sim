#version 150

uniform mat4 modelviewMatrix;
uniform mat4 modelviewProjectionMatrix;

in vec3 vPosition;							//vertex position
in vec3 vNormal;							//vertex normal
in vec2 vTexcoord;							//uv vertex textcoord
in float vArea;
in float vFormFactor;

uniform vec3 vLightPosition;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexcoord;
out float fArea;
out float fFormFactor;
out vec3 fLightPosition;

void main()
{
	fTexcoord = vTexcoord;
	fPosition = vec3(modelviewMatrix * vec4(vPosition, 1.0));
	//fNormal = vNormal;
	fNormal = normalize(vec3(modelviewMatrix * vec4(vNormal, 0.0f)));
	fLightPosition = vLightPosition;
	
	fArea = vArea;
	fFormFactor = vFormFactor;
	
	gl_Position = modelviewProjectionMatrix * vec4(vPosition, 1.0);
}
