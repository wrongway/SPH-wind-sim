#version 150

//uniform sampler2D tex0;

uniform vec4 fLightAmbient;
uniform vec4 fLightSpecular;
uniform vec4 fLightDiffuse;

uniform float fK;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexcoord;
in float fArea;
in float fFormFactor;
in vec3 fLightPosition;

in vec3 wsNormal;

uniform float fShinyness;

uniform vec4 fMaterialSpecular;

void main()
{
	vec3 smokeColor = vec3(0.4f, 0.5f, 0.62f);

	vec3 E = normalize(-fPosition);
	vec3 nNormal = normalize(fNormal);
	vec3 lightDirection = normalize(fLightPosition - fPosition);
	
	float lightIntensity  = dot(lightDirection, nNormal);
	
	smokeColor = smokeColor * lightIntensity;
	
	float viewAngleFactor = clamp(dot(E, nNormal), 0.01f, 1.0f);
	viewAngleFactor = viewAngleFactor * 1.6f;
	
	float blub = fArea;
	float alphaDensity = fK / (blub * viewAngleFactor);
	alphaDensity *= fFormFactor;
	alphaDensity = clamp(alphaDensity, 0.0001f, 1.0f);
	
	// alphaDensity = 0.5f;
 
	gl_FragColor = vec4(smokeColor, alphaDensity);
	// gl_FragColor = vec4(blub * 1000.0f, 0.0f, 0.0f, 1.0f);
	// gl_FragColor = vec4(alphaDensity, blub, viewAngleFactor, 1.0f);
}