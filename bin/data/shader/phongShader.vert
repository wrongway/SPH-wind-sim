#version 150

uniform mat4 modelviewMatrix;
uniform mat4 modelviewProjectionMatrix;

in vec3 vPosition;							//vertex position
in vec3 vNormal;							//vertex normal
in vec2 vTexcoord;							//uv vertex textcoord

uniform vec3 vLightPosition;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexcoord;

out vec3 wsNormal;

out vec3 fLightPosition;

void main()
{
	wsNormal = vNormal;

	fTexcoord = vTexcoord;
	fPosition = vec3(modelviewMatrix * vec4(vPosition, 1.0));
	//fNormal = vNormal;
	fNormal = normalize(vec3(modelviewMatrix * vec4(vNormal, 0.0f)));
	
	//fLightPosition = vec3(modelviewMatrix * vec4(vLightPosition, 1.0));
	fLightPosition = vLightPosition;
	
	gl_Position = modelviewProjectionMatrix * vec4(vPosition, 1.0);
}
