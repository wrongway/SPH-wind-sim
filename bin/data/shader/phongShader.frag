#version 150

//uniform sampler2D tex0;

uniform vec4 fLightAmbient;
uniform vec4 fLightSpecular;
uniform vec4 fLightDiffuse;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexcoord;

in vec3 fLightPosition;

in vec3 wsNormal;

uniform float fShinyness;

uniform vec4 fMaterialSpecular;

void main()
{
    //vec4 DiffuseColor = texture2D(tex0, fTexcoord);
	
	vec3 nNormal = normalize(fNormal);
	vec3 lightDirection = normalize(fLightPosition - fPosition);
	
	vec3 color = wsNormal * 0.5;
	vec4 DiffuseColor = vec4(color, 1.0); // vec4(0.05, 0.6, 0.18, 1.0);
	
	//light fPosition array
	//lightDirection = normalize(fLightPosition - fPosition);
	
	vec3 R;
	R = normalize(-reflect(lightDirection,nNormal));
	
	vec3 E = normalize(-fPosition);
	
	float lightIntensity  = dot(lightDirection,nNormal);

	//diffuse Color
	vec4 diffuse = vec4(0.0, 0.0, 0.0, 1.0);
	diffuse = vec4(DiffuseColor * fLightDiffuse) * lightIntensity;
	
	//specular highlights
	vec4 specular = vec4(0.0,0.0,0.0,1.0);
	specular = fLightSpecular * pow(max(dot(R,E),0.0), fShinyness);
	specular = clamp(specular, 0.0, 1.0);
	specular = specular * fMaterialSpecular;
	specular = clamp(specular, 0.0, 1.0);
	
	vec4 ambient = vec4(0.0, 0.1, 0.0, 1.0);
	
	//final assembly
	gl_FragColor = clamp(diffuse + specular + ambient, 0.0, 1.0);
	gl_FragColor.a = 1.0f;
	//fLightAmbient + specular + 
	//gl_FragColor = vec4(normalize(wsNormal), 1.0);
}