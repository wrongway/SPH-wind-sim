#version 150

//uniform sampler2D tex0;

uniform vec4 fLightAmbient;
uniform vec4 fLightSpecular;
uniform vec4 fLightDiffuse;

uniform float fK;

in vec3 fPosition;
in vec3 fNormal;
in vec3 fLightPosition;

in vec3 wsNormal;

uniform float fShinyness;

uniform vec4 fMaterialSpecular;

void main()
{
	gl_FragColor = vec4(0.8f, 0.3f, 0.9f, 0.1f);
}